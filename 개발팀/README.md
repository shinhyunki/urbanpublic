### 

```
git status  
파일의 상태 확인  
git add [파일명]  
파일을 새로 추적 및 Modified 상태의 파일을 Stage  
git commit  
변경사항 커밋(staged 된 파일만 커밋됨)  
git rm  
파일 삭제 - (주의) Tracked 상태의 파일을 삭제한 후에(정확하게는 Staging Area에서 삭제하는 것) 커밋해야 함  
git log  
커밋 히스토리 조회  
git reset  
파일 상태를 Unstage로 변경  
git checkout  
Modified 파일을 원복(수정한 파일을 되돌리는 방법) - (주의) 중앙집중식 버전 관리(CVCS) 툴인 svn에 checkout과 다른 명령어이므로 사용에 주의 필요  
git remote  
리모트 저장소 확인  
git fetch [remote-name]  
리모트 저장소를 Fetch(리모트 저장소에는 있는 데이터를 모두 가져옴)  
git merge  
브랜치를 합침  
git pull  
Fetch와 merge 명령을 모두 실행  
git push  
리모트 저장소에 Push  
git remote show [리모트 저장소 이름]  
리모트 저장소 조회  
git branch  
새 브랜치 생성  

Failed to connect to gitlab.com port 443: Timed out
--
방화벽 끄고 하는것도 좋을듯 
netsh advfirewall set allprofiles state off