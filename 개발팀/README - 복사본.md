###test Development
 http://www.11st.co.kr/product/SellerProductDetail.tmall?method=getSellerProductDetail&prdNo=2441441711&gclid=EAIaIQobChMIyfbW7q-T5AIVhqmWCh1h9Ay5EAkYASABEgKJy_D_BwE&utm_term=&utm_campaign=%B1%B8%B1%DB%BC%EE%C7%CEPC+%C3%DF%B0%A1%C0%DB%BE%F7&utm_source=%B1%B8%B1%DB_PC_S_%BC%EE%C7%CE&utm_medium=%B0%CB%BB%F6
 
 Vue 파헤치기 .   

-1 . 실무 이론 .   
<생명주기>   
- 인스턴스 생성    
- 인스턴스를 화면에 부착   
- 인스턴스 내용 갱신   
- 인스턴스 소멸   

react 에서는 단방향 통신   
	기존의 데이터 변환이 어떻게 이루어졌냐 => callBack 함수로 부모 => 자식 (state 변경) => 부모 => store => inject를 받았으니 당연히 부모 => 자식 이렇게 이루어 졌단 말이지 .   
###디렉티브 => v- 로 시작하는 접두어 의 속성들을 일컷는 개념 .    
Vue 에서는 양방향이라는게 존재. Angular 와 React 의 사이인 셈인데 .   

<v-model>    
 -2 가지 Case 가 존재 한다 .  
	-첫번째  Event Bus 로 바꿔주느냐 ex) this.$EventBus.emit('aaa',data) (자식) -> <abc type="text" @aaa="onAAA" ></abc> (부모)   
	-두번째  Store 값을 변경해서 Computed 로 바꿔주느냐   
component의 개념으로 양방향바인딩을 v-model을 사용해서 적용함.   
URL : https://kamang-it.tistory.com/entry/Vue20%EA%B0%92%EC%9D%98-%EB%8F%99%EC%A0%81-%EB%B3%80%EA%B2%BD%EA%B3%BC-%EC%96%91%EB%B0%A9%ED%96%A5%EB%B0%94%EC%9D%B8%EB%94%A9-%EA%B7%B8%EB%A6%AC%EA%B3%A0-vmodel%EC%8B%A4%EC%9A%A9%ED%8E%B8?category=732314
v-model 에선 굉장히 쓸 말이 많은데 .   
쉽게말해서 app이라는것은 부모 컴포넌트이고 input은 자식 컴포넌트이다.  
input박스에서 값을 바꾼것은 input의 값을 바꾼 것이다.  
하지만 이를 app이라는 부모컴포넌트에 반영하려면 v-model을 사용하면된다.  
이를 통해 양방향 이라는 말이 생성되는건데 . v-model 로 연결되어 있다고 보면된다 .   
react 에서는 event 함수 또는 CallBack 함수를 통해 상태를 변경하였지만   
Vue.js 에선 그럴 필요없이 그냥 v-model 을 사용하면 된다.   
 

--------------------------------------------
@click = @ 골뱅이는 on 의 의미 .  -> onChange == @change   
:item  = 객체를 의미 .   

$emit == 부모한테 보내는 값 .   
$on == 받을께  

vue [ angular 1.x, react ]  

1. angular emit, on  
2. vue emit, on   
 -- eventbus -> 부모 <-> 자식   
 -- component(view) - onChange -> method -> this.$EventBus.emit('aaa',data)  'aaa'게 data 보내는 중~~!!!!   
 -- component를 호출한 부모가 있는데...  'aaa'로 data 받고 싶어   
    ----- <abc type="text" @aaa="onAAA" ></abc>-  
	  
	
 -- abc component(view) - onChange 변경된 값을 넣어주고 싶다고 !!! -> this.emit('value', data)   
 <표기방법>  
 props: ['value']  
 props : {-------------------------------------------------------  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	value : {  
		type : "Array",  
		default : []  
	}  
 }  
   
 -- abc component를 호출한 부모가 있는데...    
	<abc type="text" v-model="abc" @aaa="onAAA" ></abc>  
	
	data(){	  
	 abc = {}  
	}  
,  
method : {  
 onAAA(data){  
  
 }  
}  
  

vuex -store   

Vuex Store를 바인딩하는 4가지 방법  
# 1)    
- Vue 공식 API에서는 명시된 기본 Store 바인딩 방법이다.  

ex)   
	import {mapState} from 'vuex'  
  
	new Vue({  
		computed: mapState({  
			count: state => state.count,  
			countAlias: 'count'  
		})  
	})  
	  
# 2)   
createNamespacedHelpers  

모듈의 이름이 길고 구조가 복잡하다면 createNamespacedHelpers를 사용하여  
바인딩한다면 computed 또는 methods가 간결하고 뛰어난 가독성을 보이는 것을 알 수 있다.  

vuex 중요 함수 들 .   
  
mapState - state를 연결해주는 함수  

mapGetters - getters를 연결해주는 함수  
  
mapMutations - mutations를 연결해주는 함수  
  
mapActions - actions를 연결해주는 함수  
  
if else 문   

< 태크 v-if="value >= 10000000000">   
< 태크 v-else-if" 뭐대강 이런식 > </>  

변수 binding 은 {{}}   
태그 속성에 바인딩 시켜주고 싶다 => v-bind:src 등등 이런식으로 사용함,   

for 문   

<option v-for="(num1,num2) in regions" v-bind 이런식 >  

v-once 디렉티브는 처음 한 번만 렌더링을 수행한다던가. 그렇기 떄문에 vue 인스턴스를 변경하더라도 다시 수행 x   

computed 는 구독 계속 주시하고 있는것 .   
또는 계산형 속성에서 사용할때 . 무슨 말이냐 => Vue 객체를 만들때 , computed 라는 속성과 함께 함수를 등록해두면   
마치 속성처럼 사용할수 있다는것 . 그러나 SDS 에서 적용하고자 할때 , =>  


Mutations 이란 Vuex 의 데이터, 즉 state 값을 변경하는 로직들을 의미   
성격상 안에 정의한 로직들이 순차적으로 일어나야 각 컴포넌트의 반영 여부를 제대로 추적할 수가 있기 때문이다.  
추적이 가능한 문법이고 , 쉽게 풀어서 Setter 에 근접하게 보면 된다. 큰그림은 .   
  

