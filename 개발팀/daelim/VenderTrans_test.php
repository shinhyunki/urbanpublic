<? session_start(); ?>
<html> 
<head> 
<title> :: 대림제지 - 사전정보예약 :: </title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 5px 1px;
    text-align: right;
    border: 1px solid #ddd;
}

tr:nth-child(odd) {
    background-color: #f2f2f2;
}

tr:hover {background-color:#f49d9d;}

table td a {
	width:100%;
	display:block;
	height:100%;
}

.btn {
    background-color: hotpink;
    color: white;
    padding: 10px 10px;
    border: none;
    cursor: pointer;
    width: 60%;
    opacity: 0.9;
	margin : auto;
}

.btn:hover {
    opacity: 1;
}

</style>

<script>

function goURL(URL) {
	var url = URL+'.php';
	location.href = url;
}

</script>
</head>
<body>

<div> <center> <img src = "image/ci.jpg" width="80%" style="max-width:383px;" onClick="goURL('VenderNotice');"> </center> </div>

<?
	include 'db_access.php';
	$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
	$mysqli->query("SET NAMES 'utf8'");

	if($_SESSION["join_id"]) $join_id = $_SESSION["join_id"];
	else echo "<script language='javascript'> alert('로그인 정보를 알 수 없습니다. 다시 로그인 하세요.'); location.replace('Login.php'); </script>";

	$gizunNow = $_GET["gizunNow"];
	$limitValue = $_GET["limitValue"];
	if(!$gizunNow) $gizunNow = date('Y-m-d');
	else $gizunNow = date('Y-m-d', strtotime('-1 days', strtotime($gizunNow))); 

	$baseDay =  date('Y-m-d', strtotime('-3 days'));
	if($limitValue=='') $limitValue = 0;
	else $limitValue += 5;
	

	$sql = "SELECT TDATA.VENDR_, TDATA.VCOD_, TDATA.TRANSDATE_ FROM `TDATA` WHERE 
	VCOD_=(select TUSERINFO.VCOD_ from TUSERINFO where TUSERINFO.USERID_ = '$join_id') and TDATA.STATUS_ = 5 and TDATA.TRANSDATE_ <= '$gizunNow' 
	order by TDATA.TRANSDATE_ desc limit 1";
	
	$result = $mysqli->query($sql);
	$row = $result->fetch_object();	

	//$sqlC = "SELECT * FROM `TDATA` WHERE (TDATA.STATUS_ = 4 or 5)  order by TDATA.IDX_ desc limit $limitValue, 5";	
	$sqlC = "SELECT TDATA.VENDR_, DATE_, WNO_, CARNO_, ITEM_, NET_, SNET_, PERC_, MINUS_ FROM `TDATA` WHERE VCOD_=(select TUSERINFO.VCOD_ from TUSERINFO where TUSERINFO.USERID_ = '$join_id') and (TDATA.STATUS_ = 4 or 5)  order by TDATA.IDX_ desc limit $limitValue, 5";	
	$resultC = $mysqli->query($sqlC);
	echo $sqlC;

 ?>
<h3> 거래명세표 </h3> 
<table>
   <? if($row->TRANSDATE_) { ?>
   <tr onclick="location.href='VenderTransReciept.php?venderCode=<?=$row->VCOD_?>&transDate=<?=$row->TRANSDATE_?>'" style="cursor:hand">
	<td>
		<? echo $row->VENDR_."님께 ".$row->TRANSDATE_."에 발행된 역발행 거래명세표가 있습니다.<br><font size='2'> (클릭시 상세화면, PC 인쇄/저장 가능, 크롬사용권장)</font>"; ?>
	</td>
   </tr> 
   <? } else { ?>
   <tr><td> 확인 가능한 거래명세표가 없습니다. </td></tr>
     <? } ?>
  </table>
  <br ?>
<center>
  <? if($row->TRANSDATE_) { ?> <a href="VenderTrans.php?gizunNow=<?=$row->TRANSDATE_?>"><button type="button" class="btn" style="background-color: #777777;width:20%;"> 이전 </button></a> <?}?>
  <a href="VenderTrans.php?"> <button type="button" class="btn" style="background-color: #777777;width:20%;" > 최근 </button></a> 
</center>

<h3> 거래내역 </h3>

<table width='500'>
   <tr>
     <td>일자</td>
     <td>순번</td>
	 <td>차번</td>
     <td>품목</td>
	 <td>총중량</td>
	 <td>감량</td>
     <td>감률</td>
	 <td>인수량</td>
    </tr>
 <? while($rowC = $resultC->fetch_object()) { ?>
   <tr>
     <td><?=substr($rowC->DATE_,5,5)?></td>
	 <td><?=$rowC->WNO_?></td>
     <td><?=$rowC->CARNO_?></td>
     <td><?=$rowC->ITEM_?></td>
     <td><?=number_format($rowC->NET_)?></td>
	 <td><?=number_format($rowC->MINUS_)?></td>
	 <td><?=substr($rowC->PERC_,0,4)?></td>
	 <td><?=number_format($rowC->SNET_)?></td>
    </tr>
   <?}?>
</table>
<br>
<center>
  <a href="VenderTrans.php?limitValue=<?=$limitValue?>"><button type="button" class="btn" style="background-color: #777777;width:20%;"> 이전 </button></a> 
  <a href="VenderTrans.php?"> <button type="button" class="btn" style="background-color: #777777;width:20%;" > 최근 </button></a> 
</center>

<br /><br />
<center><button type="button" class="btn" onClick="goURL('VenderNotice');"> 이전화면으로 </button> </center>

</html>