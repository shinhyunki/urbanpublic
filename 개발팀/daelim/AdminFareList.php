<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<? include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$startDate=$_GET["startDate"];
$endDate=$_GET["endDate"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 
$start_day = $_REQUEST["start_day"]; 
$end_year = $_REQUEST["end_year"]; 
$end_month = $_REQUEST["end_month"]; 
$end_day = $_REQUEST["end_day"]; 

$startDate = $start_year.'-'.$start_month.'-'.$start_day;
$endDate = $end_year.'-'.$end_month.'-'.$end_day;

$sql = "SELECT `IDX_`, `DATE_`, `WNO_`, `VENDR_`, `CARNO_`, `SNET_`, `FAREUNIT_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료' and `FAREUNIT_`>0 order by `WGUBN_` asc"; 
$result = $mysqli->query($sql);


?>

<!DOCTYPE html>
<html lang="ko">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script src="script/jquery-latest.min.js"></script>
    <script type="text/javascript" src="script/jquery.battatech.excelexport.js"></script>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }
        table{
            width: 650px;
            text-align: center;
            border: 1px solid black;
			font-size:12px;
        }
		th {
			font-size:15px;
		}
		.btn {
		   background-color: hotpink;
		    color: white;
		    padding: 10px 10px;
		    border: none;
		    cursor: pointer;
		    width: 20%;
		    opacity: 0.9;
			margin : auto;			
		}
    </style>
	<script>
		function goURL(URL) {
			var url = URL+'.php';
			location.replace(url);
		}
	</script>
</head>

<body>
 <div id="wrap" align='center'>
  <table id='tblExport' border=1>
   <tbody>
    <tr>
	 <th colspan='7' align='center'> <?=$startDate?> ~ <?=$endDate?> 운임 통계 </font> </td>
	</tr>
	<tr>
     <td> 입고일자 </td>
	 <td> 순번</td>
	 <td> 거래처</td>
	 <td> 차량번호</td>
	 <td> 인수량</td>
	 <td> 운임</td>
	 <td> 금액</td>
    </tr>
	<?while($row=$result->fetch_object()) { ?>
	<tr>
	 <td><?=$row->DATE_?></td>
	 <td><?=$row->WNO_?></td>
	 <td><?=$row->VENDR_?></td>
	 <td><?=$row->CARNO_?></td>
	 <td><?=number_format($row->SNET_)?></td>
	 <td><?=$row->FAREUNIT_?></td>
	 <td><? $fareUnit=$row->FAREUNIT_*$row->SNET_; echo number_format($fareUnit);?></td>
    </tr>
	<? } ?>
   </tbody>
  </table>     
 </div>
 <br /><br />	  
 <center>
  <a id="btnExport" href="#" download="<?='원재료마감_'.$startDate.'_'.$endDate?>.xls"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a>
	<button type="button" class="btn" onclick="window.print()"> 인쇄 </button> 
	
 </center>
 
<script type="text/javascript">
    $(document).ready(function () {
 
        function itoStr($num)
        {
            $num < 10 ? $num = '0'+$num : $num;
            return $num.toString();
        }
         
        var btn = $('#btnExport');
        var tbl = 'tblExport';
 
        btn.on('click', function () {
            var dt = new Date();
            var year =  itoStr( dt.getFullYear() );
            var month = itoStr( dt.getMonth() + 1 );
            var day =   itoStr( dt.getDate() );
            var hour =  itoStr( dt.getHours() );
            var mins =  itoStr( dt.getMinutes() );
 
            var postfix = year + month + day + "_" + hour + mins;
            var fileName = "Daelim_"+ postfix + ".xls";
 
            var uri = $("#"+tbl).excelexportjs({
                containerid: tbl
                , datatype: 'table'
                , returnUri: true
            });
 
            $(this).attr('download', fileName).attr('href', uri).attr('target', '_blank');
        });
    });
</script>
</body> 
</html>