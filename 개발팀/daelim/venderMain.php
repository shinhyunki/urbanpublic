<? session_start(); ?>
<? if($_SESSION["join_id"]) $join_id = $_SESSION["join_id"];
else echo "<script language='javascript'> alert('로그인 정보를 알 수 없습니다. 다시 로그인 하세요.'); location.replace('Login.php'); </script>";

include '../db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$sql = "SELECT `userCompany`, `userName` FROM `userAdmin` WHERE userID = $join_id"; 
$result = $mysqli->query($sql);
$row = $result->fetch_object();
$userCompany = $row->userCompany;
$userName = $row->userName;

?>

<html> 
<head> 
<title> :: 대림제지 - 사전정보예약 :: </title>
<meta http-equiv="Content-Type" content="text/html" charset=utf-8> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

.input-container {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    width: 100%;
    margin-bottom: 5px;
}

.icon {
    padding: 10px;
    background: hotpink;
    color: white;
    min-width: 50px;
    text-align: center;
}

.input-field {
    width: 100%;
    padding: 10px;
    outline: none;
}

.input-field:focus {
    border: 2px solid hotpink;
}

/* Set a style for the submit button */
.btn {
    background-color: hotpink;
    color: white;
    padding: 10px 10px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
	position:relative;
	width:100%;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}

/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 15px;
    width: 15px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 5px;
    top: 2px;
    width: 4px;
    height: 8px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}

</style>

<style>
/*the container must be positioned relative:*/
.custom-select {
  position: relative;
  font-family: Arial;
  margin-bottom: 8px;
}
.custom-select select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: hotpink;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 30px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
  user-select: none;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: hotpink;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}
</style>

<script  src="http://code.jquery.com/jquery-latest.min.js"></script>

<script>

function goRegister() {
	 alert("취소 되었습니다.");
	 location.replace('venderView.php');
}

</script>

</head>

<body>


<link rel="stylesheet" href="../style.css" type="text/css"> 
<form  name=registerContent method=post action=venderMainOK.php style="max-width:500px;margin:auto">

<h2 align='center'> 대림제지 사전정보 예약시스템 </h2>
  <div class="custom-select" style="max-width:500px;" name="destinationLocal">
    <select>
	    <option value="0"> 도착지 : 본사 </option>
		<option value="1"> 제1공장 </option>
	    <option value="2"> 제2공장 </option>
		<option value="3"> 마라도 </option>
	</select> 

  <!-- <i class="fa fa-university icon"></i> -->
 </div>
  <div class="input-container">
	<i class="fa fa-building icon"></i>
    <input class="input-field" type="text" placeholder="회사명 : 로그인정보에서 자동으로 불러옴(입력불가)" name="userCompany" value="<?=$userCompany?>">
  </div>
  <div class="input-container">
	<i class="fa fa-info icon"> </i> 
    <input class="input-field" type="text" placeholder="입력자이름 : 로그인정보에서 자동으로 불러옴(입력불가)" name="userName" value="<?=$userName?>">
  </div>
  <div class="input-container">
	<i class="fa fa-truck icon"></i>
    <input class="input-field" type="number" placeholder="차량번호 : 필수" name="carNo">
  </div>
  <div class="input-container">
	<i class="fa fa-user-circle-o icon"></i>
    <input class="input-field" type="text" placeholder="기사명 : 필수입력사항 아님" name="carDriver">
  </div>
  <div class="input-container">
	<i class="fa fa-question icon"></i>
    <input class="input-field" type="text" placeholder="품목 : 오더정보에서 자동 or 입력가능" name="itemName">
  </div>

  <div class="input-container">
    <i class="fa fa-balance-scale icon"></i>
    <input class="input-field" type="number" placeholder="예상중량(KG) : 필수입력사항 아님" name="expectWeight">
  </div>
 
  <div class="input-container">
    <i class="fa fa-clock-o icon"></i>
    <input class="input-field" type="text" placeholder="도착예정일시" name="destinationTime" value='<?=date("Y-m-d H:00")?>');>
  </div>

  <table border=0 width=100%>
  <tr><td><button type="button" class="btnR" style='background-color: silver;' onclick='goRegister();'> 취소 </button> </td>
  <td><button type="submit" class="btnR" > 등록 </button></td></tr></table>
</form>

 <script>
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script>

</body> 
</html> 
