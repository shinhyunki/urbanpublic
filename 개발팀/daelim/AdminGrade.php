<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<? include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$startDate=$_GET["startDate"];
$endDate=$_GET["endDate"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 
$start_day = $_REQUEST["start_day"]; 
$end_year = $_REQUEST["end_year"]; 
$end_month = $_REQUEST["end_month"]; 
$end_day = $_REQUEST["end_day"]; 

$startDate = $start_year.'-'.$start_month.'-'.$start_day;
$endDate = $end_year.'-'.$end_month.'-'.$end_day;

//$startDate = '2018-01-01';
//$endDate = '2018-12-31';
$venderCode = '12345';
$hap = $venderCode.'_HAP';

$sqlV = "SELECT `VCOD_`, `VENDR_`, `NAME_` FROM `TVCOD`"; 
$resultV = $mysqli->query($sqlV);
$j=0;
while($rowV=$resultV->fetch_object()){
	$venderInfo[$j]['vcod'] = $rowV->VCOD_;
	$venderInfo[$j]['vender'] = $rowV->VENDR_;
	$j++;
}

$sqlD = "SELECT DISTINCT `VCOD_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>=2 and `WGUBN_`='원재료'"; 
$resultD = $mysqli->query($sqlD);
$k=0;
while($rowD=$resultD->fetch_object()){
	$vender[$k]['vcod'] = $rowD->VCOD_;
	for($l=0;$l<$j;$l++) {
		if($vender[$k]['vcod'] == $venderInfo[$l]['vcod']) {
			$vender[$k]['vender'] = $venderInfo[$l]['vender'];
		}
	}
	//echo $k.':'.$vender[$k]['vcod'].'<br>';
	$k++;	
}
$gradeTotal=0;
$gradeAplus=0;
$gradeA=0;
$gradeBplus=0;
$gradeB=0;
$gradeCplus=0;
$gradeC=0;
$gradeE=0;
$gradeFactory=0;
$gradeHouse=0;

$sql = "SELECT `VCOD_`, `SNET_`, `GRADE_`, `ORIENT_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료'"; 
$result = $mysqli->query($sql);

while($row=$result->fetch_object()) { 
	for($i=0;$i<$k;$i++) {		
		if($vender[$i]['vcod'] == $row->VCOD_) {			
			$vender[$i]['snet'] += $row->SNET_;
			$gradeTotal += $row->SNET_;
			if($row->GRADE_ == 'A+') { 
				$vender[$i]['aplus'] += $row->SNET_;
				$gradeAplus += $row->SNET_;
			} else if($row->GRADE_ == 'A') { 
				$vender[$i]['a'] += $row->SNET_;
				$gradeA += $row->SNET_;
			} else if($row->GRADE_ == 'B+') { 
				$vender[$i]['bplus'] += $row->SNET_;
				$gradeBplus += $row->SNET_;
			} else if($row->GRADE_ == 'B') { 
				$vender[$i]['b'] += $row->SNET_;
				$gradeB += $row->SNET_;
			} else if($row->GRADE_ == 'C+') { 
				$vender[$i]['cplus'] += $row->SNET_;
				$gradeCplus += $row->SNET_;
			} else if($row->GRADE_ == 'C') { 
				$vender[$i]['c'] += $row->SNET_;
				$gradeC += $row->SNET_;
			} else if($row->GRADE_ == 'E') { 
				$vender[$i]['e'] += $row->SNET_;
				$gradeE += $row->SNET_;
			}

			if($row->ORIENT_ == '생활') { 
				$vender[$i]['house'] += $row->SNET_;
				$gradeFactory += $row->SNET_;
			} else if($row->ORIENT_ == '공장') { 
				$vender[$i]['factory'] += $row->SNET_;						
				$gradeHouse += $row->SNET_;
			}
		}
	}
}


?>

<!DOCTYPE html>
<html lang="ko">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script src="script/jquery-latest.min.js"></script>
    <script type="text/javascript" src="script/jquery.battatech.excelexport.js"></script>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }
        table{
            width: 650px;
            text-align: center;
            border: 1px solid black;
			font-size:12px;
        }
		th {
			font-size:15px;
		}
		.btn {
		   background-color: hotpink;
		    color: white;
		    padding: 10px 10px;
		    border: none;
		    cursor: pointer;
		    width: 20%;
		    opacity: 0.9;
			margin : auto;			
		}
    </style>
	<script>
		function goURL(URL) {
			var url = URL+'.php';
			location.replace(url);
		}
	</script>
</head>

<body>
 <div id="wrap" align='center'>
  <table id='tblExport' border=1>
   <tbody>
    <tr>
	 <th colspan='12' align='center'> <?=$startDate?> ~ <?=$endDate?> 원재료 통계 </font> </td>
	</tr>
	<tr>
     <td> 거래처코드 </td>
	 <td> 거래처상호</td>
	 <td> 총중량</td>
	 <td> A+</td>
	 <td> A</td>
	 <td> B+</td>
	 <td> B</td>
	 <td> C+</td>
	 <td> C</td>
	 <td> E</td>
	 <td> 공장</td>
	 <td> 생활</td>
    </tr>
	<?for($i=0;$i<$k;$i++) { ?>
	<tr>
     <td> <?=$vender[$i]['vcod']?> </td>
	 <td> <?=$vender[$i]['vender']?></td>
	 <td> <?=number_format($vender[$i]['snet'])?></td>
	 <td> <?=number_format($vender[$i]['aplus'])?></td>
	 <td> <?=number_format($vender[$i]['a'])?></td>	 
	 <td> <?=number_format($vender[$i]['bplus'])?></td>	 
	 <td> <?=number_format($vender[$i]['b'])?></td>
	 <td> <?=number_format($vender[$i]['cplus'])?></td>
	 <td> <?=number_format($vender[$i]['c'])?></td>
	 <td> <?=number_format($vender[$i]['e'])?></td>
	 <td> <?=number_format($vender[$i]['factory'])?></td>
	 <td> <?=number_format($vender[$i]['house'])?></td>	 
    </tr>
	
	<? } ?>
	<tr>
     <td colspan='2'> 소계 </td>
	 <td> <?=number_format($gradeTotal)?></td>
	 <td> <?=number_format($gradeAplus)?></td>
	 <td> <?=number_format($gradeA)?></td>
	 <td> <?=number_format($gradeBplus)?></td>
	 <td> <?=number_format($gradeB)?></td>
	 <td> <?=number_format($gradeCplus)?></td>
	 <td> <?=number_format($gradeC)?></td>
	 <td> <?=number_format($gradeE)?></td>
	 <td> <?=number_format($gradeFactory)?></td>
	 <td> <?=number_format($gradeHouse)?></td>
    </tr>
   </tbody>
  </table>     
 </div>
 <br /><br />	  
 <center>
  <a id="btnExport" href="#" download="<?='원재료마감_'.$startDate.'_'.$endDate?>.xls"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a>
	<button type="button" class="btn" onclick="window.print()"> 인쇄 </button> 
	
 </center>
 
<script type="text/javascript">
    $(document).ready(function () {
 
        function itoStr($num)
        {
            $num < 10 ? $num = '0'+$num : $num;
            return $num.toString();
        }
         
        var btn = $('#btnExport');
        var tbl = 'tblExport';
 
        btn.on('click', function () {
            var dt = new Date();
            var year =  itoStr( dt.getFullYear() );
            var month = itoStr( dt.getMonth() + 1 );
            var day =   itoStr( dt.getDate() );
            var hour =  itoStr( dt.getHours() );
            var mins =  itoStr( dt.getMinutes() );
 
            var postfix = year + month + day + "_" + hour + mins;
            var fileName = "Daelim_"+ postfix + ".xls";
 
            var uri = $("#"+tbl).excelexportjs({
                containerid: tbl
                , datatype: 'table'
                , returnUri: true
            });
 
            $(this).attr('download', fileName).attr('href', uri).attr('target', '_blank');
        });
    });
</script>
</body> 
</html>