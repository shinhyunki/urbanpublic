<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$now = date('Ymd');
isset($_REQUEST["field"])?$field = $_REQUEST["field"]:$field='IDX_';
isset($_REQUEST["field_detail"])?$field_detail = $_REQUEST["field_detail"]:$field_detail='';

if($field=='VENDR_' && $field_detail=='전체') $field_detail = '';

isset($_REQUEST["page"])?$page = $_REQUEST["page"]:$page='1';


$list_num=10;
$start_rec=($page-1)*$list_num;

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$sql = "select count(IDX_) as dataCount from `TNOTICE` where ($field like '%$field_detail%');"; 

$result = $mysqli->query($sql);

$total_rec=$result->fetch_object()->dataCount;

$total_page=ceil($total_rec/$list_num);

$join = 1;

?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;	
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 

a {
	color: red;
	text-decoration: none;
	font-weight:bold;
}

</style>

</head>
<body>

<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <a class="active" href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="#about"> 기타 </a>
</div>


<? 
	$sql = "select * from `TNOTICE`  where ($field like '%$field_detail%') order by IDX_ desc limit $start_rec, $list_num";	
	$result = $mysqli->query($sql);	

?>
<BR>
<div style="text-align:right"> <a href='AdminNoticeNew.php'> 공지사항 신규등록 </a> </div>
<BR>
<table>
  <tr>
    <th>벤더이름<br>벤더코드</th>
    <th>공지제목</th>
	<th>공지내용</th>
	<th>노출시작</th>
	<th>노출마감</th>
	<th>1:노출<br>0:정지</th>
	<th>정보변경일</th>
  </tr>


 <? $k=0; while($row = $result->fetch_object()) { $k++; ?>
   
   <tr onclick="location.href='AdminNoticeEdit.php?idx=<?=$row->IDX_?>'" style="cursor:hand"> 
	<td> <?if($row->VCOD_==0) echo "전체<BR>0"; else echo $row->VENDR_.'<BR>'.$row->VCOD_; ?> </td> 
	<td> <?=$row->NOTICETITLE_?> </td> 
	<td> <?=nl2br($row->NOTICECONTENTS_)?> </td> 
	<td> <?=$row->STARTDATETIME_?> </td> 	
	<td> <?=$row->ENDDATETIME_?> </td>
	<td> <?=$row->STATUSFLAG_?> </td>
	<td> <?=$row->UPDATETS_?> </td> 
   </tr>
  <?}?>

  </table>
  <br>
  <center>

 <? 
	$page_num = 10;
	$total_block=ceil($total_page/$page_num);
	$block = ceil($page/$page_num);
	$first=($block-1)*$page_num+1;
	$end=($block*$page_num);

	if($total_block>1) {

	if($total_block==$block) $end=$total_page;
	if($block>1){
		echo "[<a href='AdminNotice.php?page=1&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'> 처음 </a>]";
		echo "<a href='AdminNotice.php?page=".($first-1)."&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'> ◀ </a>";
	}

	for($l=$first; $l<=$end; $l++){
		if($l==$page){
			echo "<b>  $l  </b>";
		}else{
			echo " [<a href='AdminNotice.php?page=$l&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'>$l</a>] ";
		}
	}
    
	if($block<$total_block){
		echo "[<a href='AdminNotice.php?page=".($end+1)."&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'>다음 $page_num 개 </a>]";
		echo "[<a href='AdminNotice.php?page=$total_page&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'>마지막</a>]";
	}
	}

	?>

<br>
<br>
<form name="search" method=post action='AdminNotice.php?page=1&field=<?$field?>&field_detail=<?$field_detail?>&date_start=<?$date_start?>&date_end=<?$date_end?>'>

<select name="field">
	<option value="VENDR_" selected> 벤더이름 </option>
	<option value="VCOD_"> 벤더코드 </option>
</select> 
<input type=text size=30 maxlength=30 name="field_detail" align=left>


<input type="submit" value=" Search " >
</form>

</center>

</body>
</html>
