<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}


table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {

	font-size: 100%;
    padding: 0.5em 0.5em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

td:hover {
	background-color:#f49d9d;
}

.td_code {
	text-align: right;
}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>
function goBack() {
	location.replace('AdminReserve.php?page=1');
}

function goDel(carNo) {
	var r = confirm("정말로 삭제하시겠습니까?");
	//if(r==true) alert(vcod);
	url = 'adminCarEditDel.php?carNo='+carNo;
	if(r==true) location.replace(url);	
}
</script>
</head>
<body>

<? 
	$sql = "select * from `TCARINFO` order by `CARNO_` asc";
	$result = $mysqli->query($sql);
	

?>

<h2> 공차 중량 관리 </h2>
<table>
  <tr>
    <th>차량번호</th>
    <th>공차중량</th>
	<th colspan='2'> 추가 수정 삭제</th>
  </tr>
  <tr>
    <form method='post' action='adminCarEdit_insert.php'>
	<td><input type='text' name='carNo' size='10'> </td>
	<td><input type='num' name='regCarWgt' size='10'> </td>
	<td colspan='2'><input type='submit' value='추가하기''> </td>		
	</form>
  </tr>
  <? while($row = $result->fetch_object()) { ?>
  <tr>
    <form method='post' action='adminCarEdit_ok.php'>
    <span style='text-align: right;'> 
	<td><input type='text' name='carNo' size='10' value='<?=$row->CARNO_?>' readonly> </td>
	<td><input type='num' name='regCarWgt' size='10' value='<?=number_format($row->REGCARWGT_)?>'> </td>
	<td><input type='submit' value='수정'> </td>		
	<td><input type='button' value='삭제' onclick="goDel('<?=$row->CARNO_?>')"> </td>			
	</form>
  </tr>
  <?}?>
  </table>


</body>
</html>
