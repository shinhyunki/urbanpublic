<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<? include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$startDate=$_GET["startDate"];
$endDate=$_GET["endDate"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 
$start_day = $_REQUEST["start_day"]; 
$end_year = $_REQUEST["end_year"]; 
$end_month = $_REQUEST["end_month"]; 
$end_day = $_REQUEST["end_day"]; 

$startDate = $start_year.'-'.$start_month.'-'.$start_day;
$endDate = $end_year.'-'.$end_month.'-'.$end_day;

//$startDate = '2018-01-01';
//$endDate = '2018-12-31';

$sql = "SELECT `VCOD_`, `VENDR_`, `SNET_`, `MINUS_`, `GRADE_`, `ORIENT_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료'"; 
$result = $mysqli->query($sql);

$gradeTotal = 0;
$gradeAplus = 0;
$gradeA = 0;
$gradeBplus = 0;
$gradeB = 0;
$gradeCplus = 0;
$gradeC = 0;
$gradeE = 0;

$minusTotal = 0;
$minusAplus = 0;
$minusA = 0;
$minusBplus = 0;
$minusB = 0;
$minusCplus = 0;
$minusC = 0;
$minusE = 0;

$venderAplus = '';
$venderA = '';
$venderBplus = '';
$venderB = '';
$venderCplus = '';
$venderC = '';
$venderE = '';

$house = 0;
$factory = 0;

$minusHouse = 0;
$minusFactory = 0;

$houseVender = '';
$factoryVender = '';


while($row=$result->fetch_object()) {

	$gradeTotal += $row->SNET_;
	$minusTotal += $row->MINUS_;
	$vdr = $row->VENDR;
	
	
	if($row->GRADE_ == 'A+') { 
		$gradeAplus += $row->SNET_;
		$minusAplus += $row->MINUS_;		
		if(strrpos($venderAplus,  $row->VENDR_) === false) $venderAplus .= $row->VENDR_.',';
		
	} else if($row->GRADE_ == 'A') {
		$gradeA += $row->SNET_;
		$minusA += $row->MINUS_;
		if(strrpos($venderA, $row->VENDR_) === false) $venderA .= $row->VENDR_.',';
	} else if($row->GRADE_ == 'B+') { 
		$gradeBplus += $row->SNET_;
		$minusBplus += $row->MINUS_;
		if(strrpos($venderBplus, $row->VENDR_) === false) $venderBplus .= $row->VENDR_.',';
	} else if($row->GRADE_ == 'B') { 
		$gradeB += $row->SNET_;
		$minusB += $row->MINUS_;
		if(strrpos($venderB, $row->VENDR_) === false) $venderB .= $row->VENDR_.',';
	} else if($row->GRADE_ == 'C+') { 
		$gradeCplus += $row->SNET_;
		$minusCplus += $row->MINUS_;
		if(strrpos($venderCplus, $row->VENDR_) === false) $venderCplus .= $row->VENDR_.',';
	} else if($row->GRADE_ == 'C') { 
		$gradeC += $row->SNET_;
		$minusC += $row->MINUS_;
		if(strrpos($venderC, $row->VENDR_) === false) $venderC .= $row->VENDR_.',';
	} else if($row->GRADE_ == 'E') { 
		$gradeE += $row->SNET_;
		$minusE += $row->MINUS_;
		if(strrpos($venderE, $row->VENDR_) === false) $venderE .= $row->VENDR_.',';
	}


	if($row->ORIENT_ == '생활') {
		$house += $row->SNET_;
		$minusHouse += $row->MINUS_;
		if(strrpos($houseVender, $row->VENDR_)=== false) $houseVender .= $row->VENDR_.',';		
	} else if($row->ORIENT_ == '공장') { 
		$factory += $row->SNET_;
		$minusFactory += $row->MINUS_;
		if(strrpos($factoryVender, $row->VENDR_)=== false) $factoryVender .= $row->VENDR_.',';
	}
}


?>

<!DOCTYPE html>
<html lang="ko">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script src="script/jquery-latest.min.js"></script>
    <script type="text/javascript" src="script/jquery.battatech.excelexport.js"></script>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }
        table{
            width: 1050px;
            text-align: center;
            border: 1px solid black;
			font-size:12px;
        }
		th {
			font-size:15px;
		}
		.btn {
		   background-color: hotpink;
		    color: white;
		    padding: 10px 10px;
		    border: none;
		    cursor: pointer;
		    width: 20%;
		    opacity: 0.9;
			margin : auto;			
		}
    </style>
	<script>
		function goURL(URL) {
			var url = URL+'.php';
			location.replace(url);
		}
	</script>
</head>

<body>

 <div id="wrap" align='center'>
  <table id='tblExport' border=1>
   <tbody>
    <tr>
	 <th colspan='7' align='center'> <?=$startDate?> ~ <?=$endDate?> 원재료 통계 </font> </td>
	</tr>
	<tr>
     <td width='60' bgcolor='DarkSeaGreen'> 품질등급 </td>
	 <td width='60' bgcolor='DarkSeaGreen'> 품질(%) </td>
	 <td width='60' bgcolor='DarkSeaGreen'> 인수량 </td>
	 <td width='60' bgcolor='DarkSeaGreen'> 감량 </td>
	 <td width='60' bgcolor='DarkSeaGreen'> 품질비중 </td>
	 <td width='60' bgcolor='DarkSeaGreen'> 목표</td>
	 <td bgcolor='DarkSeaGreen'> 거래처 </td>
    </tr>	
	<tr>
	 <td width='60'> A+ </td>
	 <td> <?=number_format(100*$minusAplus/($gradeAplus+$minusAplus), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeAplus)?> </td>
	 <td> <?=number_format($minusAplus)?> </td>
	 <td> <?=number_format(100*$gradeAplus/$gradeTotal)?> </td>
	 <td rowspan='2'> </td>
	 <td> <?=$venderAplus?> </td>
	</tr>
	<tr>
	 <td> A </td>
	 <td> <?=number_format(100*$minusA/($gradeA+$minusA), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeA)?> </td>
	 <td> <?=number_format($minusA)?> </td>
	 <td> <?=number_format(100*$gradeA/$gradeTotal)?> </td>
	 <td> <?=$venderA?> </td>
	</tr>
	<tr>
	 <td> B+ </td>
	 <td> <?=number_format(100*$minusBplus/($gradeBplus+$minusBplus), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeBplus)?> </td>
	 <td> <?=number_format($minusBplus)?> </td>
	 <td> <?=number_format(100*$gradeBplus/$gradeTotal)?> </td>
	 <td rowspan='2'> </td>
	 <td> <?=$venderBplus?> </td>
	</tr>
	<tr>
	 <td> B </td>
	 <td> <?=number_format(100*$minusB/($gradeB+$minusB), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeB)?> </td>
	 <td> <?=number_format($minusB)?> </td>
	 <td> <?=number_format(100*$gradeB/$gradeTotal)?> </td>
	 <td> <?=$venderB?> </td>
	</tr>
	<tr>
	 <td> C+ </td>
	 <td> <?=number_format(100*$minusCplus/($gradeCplus+$minusCplus), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeCplus)?> </td>
	 <td> <?=number_format($minusCplus)?> </td>
	 <td> <?=number_format(100*$gradeCplus/$gradeTotal)?> </td>
	 <td rowspan='2'> </td>
	 <td> <?=$venderCplus?> </td>
	</tr>
	<tr>
	 <td> C </td>
	 <td> <?=number_format(100*$minusC/($gradeC+$minusC), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeC)?> </td>
	 <td> <?=number_format($minusC)?> </td>
	 <td> <?=number_format(100*$gradeC/$gradeTotal)?> </td>
	 <td> <?=$venderC?> </td>
	</tr>
	<tr>
	 <td> 벌크 </td>
	 <td> <?=number_format(100*$minusE/($gradeE+$minusE), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeE)?> </td>
	 <td> <?=number_format($minusE)?> </td>
	 <td> <?=number_format(100*$gradeE/$gradeTotal)?> </td>
	 <td rowspan='2'> </td>
	 <td> <?=$venderE?> </td>
	</tr>
	<tr>
	 <td> 소계 </td>
	 <td> <?=number_format(100*$minusTotal/($gradeTotal+$minusTotal), 2, '.', '')?> </td>
	 <td> <?=number_format($gradeTotal)?> </td>
	 <td> <?=number_format($minusTotal)?> </td>
	 <td> <?=number_format(100)?> </td>
	 <td> </td>
	</tr>
	<tr>
	 <td bgcolor='yellow'> 원료구분 </td>
	 <td bgcolor='yellow'> 인수량 </td>
	 <td bgcolor='yellow'> 감량 </td>
	 <td bgcolor='yellow'> 입고비율(%) </td>
	 <td bgcolor='yellow'> 감량률(%) </td>
	 <td bgcolor='yellow'> 목표 </td>
	 <td bgcolor='yellow'> 거래처 </td>
	<tr>
	 <td> 생활 </td>
	 <td> <?=number_format($house)?></td>
	 <td> <?=number_format($minusHouse)?></td>
	 <td> <?=number_format((100*$house/$gradeTotal), 2, '.', '')?></td>
	 <td> <?=number_format((100*$minusHouse/($house+$minusHouse)), 2, '.', '')?></td>
	 <td> </td>
	 <td> <?=$houseVender?></td>
	</tr>
	<tr>
	 <td> 공장 </td>
	 <td> <?=number_format($factory)?></td>
	 <td> <?=number_format($minusFactory)?></td>
	 <td> <?=number_format((100*$factory/$gradeTotal), 2, '.', '')?></td>
	 <td> <?=number_format((100*$minusFactory/($factory+$minusFactory)), 2, '.', '')?></td>
	 <td> </td>
	 <td> <?=$factoryVender?></td>
	</tr>
	<tr>
	 <td> 소계 </td>
	 <td> <?=number_format($factory+$house)?></td>
	 <td> <?=number_format($minusHouse+$minusFactory)?></td>
	 <td> <?=number_format(100)?></td>
	 <td> <?=number_format((100*($minusHouse+$minusFactory)/($factory+$house+$minusHouse+$minusFactory)), 2, '.', '')?></td>
	 <td> </td>
	 <td> </td>
	</tr>



   </tbody>
  </table>     
 </div>
 <br /><br />	  
 <center>
  <a id="btnExport" href="#" download="<?='원재료마감_'.$startDate.'_'.$endDate?>.xls"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a>
	<button type="button" class="btn" onclick="window.print()"> 인쇄 </button> 
	
 </center>
 
<script type="text/javascript">
    $(document).ready(function () {
 
        function itoStr($num)
        {
            $num < 10 ? $num = '0'+$num : $num;
            return $num.toString();
        }
         
        var btn = $('#btnExport');
        var tbl = 'tblExport';
 
        btn.on('click', function () {
            var dt = new Date();
            var year =  itoStr( dt.getFullYear() );
            var month = itoStr( dt.getMonth() + 1 );
            var day =   itoStr( dt.getDate() );
            var hour =  itoStr( dt.getHours() );
            var mins =  itoStr( dt.getMinutes() );
 
            var postfix = year + month + day + "_" + hour + mins;
            var fileName = "Daelim_"+ postfix + ".xls";
 
            var uri = $("#"+tbl).excelexportjs({
                containerid: tbl
                , datatype: 'table'
                , returnUri: true
            });
 
            $(this).attr('download', fileName).attr('href', uri).attr('target', '_blank');
        });
    });
</script>
</body> 
</html>