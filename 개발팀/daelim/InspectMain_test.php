<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('image/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 16px;
}

#myTable th, #myTable td {
  text-align: center;
  padding: 15px 2px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
</head>




<body>

<h2><a href="InspectMain.php"> 검수차량 조회</a></h2>

<input type="number" id="myInput" oninput="myFunction()" placeholder="차량번호 검색" title="Type in a name">


<?
include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$today = DATE("Y-m-d");

$sql = "select IDX_, DATE_, WNO_, CARNO_, VENDR_, ITEM_, STATUS_ from TDATA where DATE_ = '$today' and STATUS_='1' AND `WGUBN_` = '원재료' order by IDX_ asc"; 
$result = $mysqli->query($sql);

$sql2 = "select IDX_, DATE_, WNO_, CARNO_, VENDR_, ITEM_, STATUS_ from TDATA where  DATE_ = '$today' and STATUS_='2' AND `WGUBN_` = '원재료' order by IDX_ asc"; 
$result2 = $mysqli->query($sql2);

$sql3 = "select IDX_, DATE_, WNO_, CARNO_, VENDR_, ITEM_, STATUS_ from TDATA where DATE_ = '2019-04-26' and  STATUS_>'2' AND `WGUBN_` = '원재료' order by IDX_ asc"; 
$result3 = $mysqli->query($sql3);


/*while($row = $result->fetch_object()){ 
	if($row->STATUS_ == 2) echo "<tr><td><a href=InspectConfirm.php?dateValue=".$row->DATE_."&wno=".$row->WNO_."><font color='red'><b>".$row->WNO_.' : '.$row->VENDR_.' : '.$row->ITEM_.' : '.$row->CARNO_."</b></font></a></td></tr>";
	else echo "<tr><td><a href=InspectConfirm.php?dateValue=".$row->DATE_."&wno=".$row->WNO_.">".$row->WNO_.' : '.$row->VENDR_.' : '.$row->ITEM_.' : '.$row->CARNO_."</a></td></tr>";
}*/
?>
<table id='myTable'>
  <tr class="header">
    <th> 순번 </th>
	<th> 거래처 </th>
	<th> 품목 </th>
	<th> 차량번호 </th>
  </tr>
<? while($row = $result->fetch_object()){ ?>
  <tr onclick="location.href='InspectConfirm.php?idx=<?=$row->IDX_?>'" style="cursor:hand"> 
    <td><?=$row->WNO_?></td>
	<td><?=$row->VENDR_?></td>
	<td><?=$row->ITEM_?></td>
	<td><?if($row->STATUS_==2) echo "<font color='red'><b>".$row->CARNO_."</b></font>"; else echo "<font color='black'><b>".$row->CARNO_."</b></font>"?></td>
  </tr>
<? } ?>
<? while($row2 = $result2->fetch_object()){ ?>
  <tr onclick="location.href='InspectConfirm.php?idx=<?=$row2->IDX_?>'" style="cursor:hand"> 
    <td><?=$row2->WNO_?></td>
	<td><?=$row2->VENDR_?></td>
	<td><?=$row2->ITEM_?></td>
	<td><?if($row2->STATUS_==2) echo "<font color='red'><b>".$row2->CARNO_."</b></font>"; else echo "<font color='black'><b>".$row2->CARNO_."</b></font>"?></td>
  </tr>
<? } ?>

<? while($row3 = $result3->fetch_object()){ ?>
  <tr onclick="location.href='InspectConfirm.php?idx=<?=$row3->IDX_?>'" style="cursor:hand"> 
    <td><?=$row3->WNO_?></td>
	<td><?=$row3->VENDR_?></td>
	<td><?=$row3->ITEM_?></td>
	<td><?if($row3->STATUS_>2) echo "<font color='blue'><b>".$row3->CARNO_."</b></font>"; else echo "<font color='black'><b>".$row3->CARNO_."</b></font>"?></td>
  </tr>
<? } ?>
</table>

<script>
/*function myFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}*/
function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>

</body>
</html>
