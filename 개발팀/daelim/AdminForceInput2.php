<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 


$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");



$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_`, `USERNAME_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$row=$resultUser->fetch_object();

$userStatus = $row->USERSTATUS_;
$userName = $row->USERNAME_;

$dateY = date("Y");
$dateM = date("m");
$dateD = date("d");


//if($userStatus!=4 or $userStatus!=5) echo "<script language='javascript'> alert('권한이 없습니다. 다시 로그인하세요.'); location.replace('Login.php'); </script>";
?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 95%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}

input[type=number]{
    width: 70px;
} 
</style>

<script>

</script>
</head>
<body>

<h2> 수동입력 모드(입력자 : <?=$userStatus;?> )</h2>
<br>
<form  name="adminMainEdit" method="post" action="adminForceInput2_ok.php">

<table>
  <tr>
	<th>계량날짜</th>
	<th>구분</th>
	<th>차번</th>
	<th>거래처</th>
	<th>품목</th>
	<th>총중량</th>
	<th>공차중량</th>
	<th>실중량<br>(자동)</th>	
  </tr>
   <tr>
    <td>
		<select name='dateY'>
			<option value='<?=$dateY-1?>'>  <?=$dateY-1?>년 </option>
			<option value='<?=$dateY?>' selected>  <?=$dateY?>년 </option>
			<option value='<?=$dateY+1?>'>  <?=$dateY+1?>년 </option>
			<option value='<?=$dateY+2?>'>  <?=$dateY+2?>년 </option>
			<option value='<?=$dateY+3?>'>  <?=$dateY+3?>년 </option>
		</select>
		<select name='dateM'>
			<option value='1' <?if($dateM==1) echo 'selected' ?>> 1월 </option>
			<option value='2' <?if($dateM==2) echo 'selected' ?>> 2월 </option>
			<option value='3' <?if($dateM==3) echo 'selected' ?>> 3월 </option>
			<option value='4' <?if($dateM==4) echo 'selected' ?>> 4월 </option>
			<option value='5' <?if($dateM==5) echo 'selected' ?>> 5월 </option>
			<option value='6' <?if($dateM==6) echo 'selected' ?>> 6월 </option>
			<option value='7' <?if($dateM==7) echo 'selected' ?>> 7월 </option>
			<option value='8' <?if($dateM==8) echo 'selected' ?>> 8월 </option>
			<option value='9' <?if($dateM==9) echo 'selected' ?>> 9월 </option>
			<option value='10' <?if($dateM==10) echo 'selected' ?>> 10월 </option>
			<option value='11' <?if($dateM==11) echo 'selected' ?>> 11월 </option>
			<option value='12' <?if($dateM==12) echo 'selected' ?>> 12월 </option>
		</select>
		<select name='dateD'>
			<?for($i=1; $i<=31; $i++) {
				echo "<option value='".$i."'";
				if($dateD==$i) echo " selected";
				echo "> ".$i."일 </option>";
			}
			?>
		</select>
	</td>

    <td> <select name='wgubn'>			
			<option value="부재료">  부재료 </option>
			<option value="폐합성수지"> 폐합성수지 </option>
			<option value="소각폐기물">  소각폐기물 </option>
			<option value="제품판매"> 제품판매 </option>
			<option value="기타">  기타 </option>			
		</select>
	</td> 	
	<td> <input type='text' name='carNo' size='8'> </td> 
	<td> 
		<select name='venderCode'>
			<?						
			$sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_!='원재료' order by VENDR_ ASC";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->VCOD_?>%<?=$rowVender->VENDR_?>' <?if($rowVender->VCOD_==$row->VCOD_) echo 'selected'?>> <?=$rowVender->VENDR_?> : <?=$rowVender->VCOD_?></option>
			<?}?>
		</select>
	</td> 
	<td> 
		<select name='itemCode'>
			<?
			$sqlVender = "select ITEM_, ICOD_ from `TICOD` where WGUBN_!='원재료' order by ICOD_ ASC";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->ICOD_?>%<?=$rowVender->ITEM_?>' <?if($rowVender->ICOD_==$row->ICOD_) echo 'selected'?>> <?=$rowVender->ITEM_?> : <?=$rowVender->ICOD_?></option>
			<?}?>
		</select>
	</td>
	<td> <input type='number' name='gross' id='gross' size='3'> </td>
	<td> <input type='number' name='car' id='car' size='1'> </td>
	<td> <input type='number' name='net' id='net' size='1' readonly> </td> 	
   </tr>
  </table>
<input type='hidden' name='userName' value='<?=$userName?>'>
  <center>
  <br><br>
 <button type="submit" class="btn"> 삽입 </button>
  </center>
 <br /><br />
 </form>
</body>
</html>


<script>
document.getElementById("gross").addEventListener("keyup", myFunction);
document.getElementById("car").addEventListener("keyup", myFunction);

var gross=0, car=0, net=0;

function myFunction() {	
	gross = document.getElementById("gross").value;
	car = document.getElementById("car").value;
	net = Math.abs(gross-car);
	document.getElementById("net").value = net;

	
	/*var net = document.getElementById("net").value;
	var perc1 = document.getElementById("perc1").value;
	var perc2 = document.getElementById("perc2").value;
	net = net*1;perc1 = perc1*1;perc2 = perc2*1;
	var minus = Math.floor(net*(perc1+perc2)*0.001)*10;
	var snet = net - minus;
	var unit = document.getElementById("unit").value;
    document.getElementById("minus").value = minus;
	document.getElementById("snet").value = snet;
	var hap = unit*snet;		
	document.getElementById("hap").value = hap;*/
}

</script>