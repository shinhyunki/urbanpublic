<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 


$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");


$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_`, `USERNAME_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$row=$resultUser->fetch_object();

$userStatus = $row->USERSTATUS_;
$userName = $row->USERNAME_;


if($userStatus!=9) echo "<script language='javascript'> alert('권한이 없습니다. 다시 로그인하세요.'); location.replace('Login.php'); </script>";

//echo $userStatus;

?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 95%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}

input[type=number]{
    width: 70px;
} 
</style>

<script>

</script>
</head>
<body>

<h2> 수동입력 모드(원재료) </h2>
<br>
<form  name="adminMainEdit" method="post" action="adminForceInput_ok.php">
<input type='hidden' name='postValue' value=<?=$postValue?>>
<table>
  <tr>
	<th>차번</th>
	<th>거래처</th>
	<th>품목</th>
	<th>총중량</th>
	<th>공차중량</th>
	<th>실중량<br>(자동)</th>
	<th>검수자</th>
	<th>감률1</th>
	<th>감률2</th>
	<th>감률<br>(자동)</th>
	<th>이물질<br>(자동)</th>
	<th>수분<br>(자동)</th>
	<th>감량<br>(자동)</th>
	<th>인수량<br>(자동)</th>	
	<th>구분</th>
	<th>등급</th>	
  </tr>

   <tr> 
	<td> <input type='text' name='carNo' size='8'> </td> 
	<td> 
		<select name='venderCode'>
			<?			
			if($userStatus==9) $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
			else $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_!='원재료' order by VENDR_ ASC";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->VCOD_?>%<?=$rowVender->VENDR_?>' <?if($rowVender->VCOD_==$row->VCOD_) echo 'selected'?>> <?=$rowVender->VENDR_?> : <?=$rowVender->VCOD_?></option>
			<?}?>
		</select>
	</td> 
	<td> 
		<select name='itemCode'>
			<?
			$sqlVender = "select ITEM_, ICOD_ from `TICOD` where WGUBN_='원재료' order by ICOD_ ASC";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->ICOD_?>%<?=$rowVender->ITEM_?>' <?if($rowVender->ICOD_==$row->ICOD_) echo 'selected'?>> <?=$rowVender->ITEM_?> : <?=$rowVender->ICOD_?></option>
			<?}?>
		</select>
	</td>
	<td> <input type='number' name='gross' id='gross' size='3'> </td>
	<td> <input type='number' name='car' id='car' size='1'> </td>
	<td> <input type='number' name='net' id='net' size='1' readonly> </td> 
	<td> <input type='number' name='inspectman' id='inspectman' size='1'> </td> 
	<td> <input type='number' name='perc1' id='perc1' size='1'></td>
	<td> <input type='number' name='perc2' id='perc2' size='1'></td>
	<td> <input type='number' name='perc' id='perc' size='1' readonly></td>
	<td> <input type='number' name='minus1' id='minus1' size='3' readonly></td>
	<td> <input type='number' name='minus2' id='minus2' size='3' readonly></td>
	<td> <input type='number' name='minus' id='minus' size='3' readonly></td>
	<td> <input type='number' name='snet' id='snet' size='3' readonly> </td> 
	<td> <select name='orient'>
			<option value="공장"> 공장 </option>
			<option value="생활"> 생활 </option>
		</select>
	</td>
	<td> <select name='grade'>
			<option value="A+"> A+ </option>
			<option value="A">  A </option>
			<option value="B+"> B+ </option>
			<option value="B" selected>  B </option>
			<option value="C+"> C+ </option>
			<option value="C">  C </option>
			<option value="E">  E </option>
		</select>
	</td> 	
   </tr>
  </table>
  <input type='hidden' name='userName' value='<?=$userName?>'>
  <center>
  <br><br>
 <button type="submit" class="btn"> 삽입 </button>
  </center>
 <br /><br />
 </form>
</body>
</html>


<script>
document.getElementById("gross").addEventListener("keyup", myFunction);
document.getElementById("car").addEventListener("keyup", myFunction);

document.getElementById("perc1").addEventListener("keyup", myFunction2);
document.getElementById("perc2").addEventListener("keyup", myFunction2);
document.getElementById("unit").addEventListener("keyup", myFunction2);
document.getElementById("fareunit").addEventListener("keyup", myFunction2);

var gross=0, car=0, net=0;

function myFunction() {	
	gross = document.getElementById("gross").value;
	car = document.getElementById("car").value;
	net = gross-car;
	document.getElementById("net").value = net;

	
	/*var net = document.getElementById("net").value;
	var perc1 = document.getElementById("perc1").value;
	var perc2 = document.getElementById("perc2").value;
	net = net*1;perc1 = perc1*1;perc2 = perc2*1;
	var minus = Math.floor(net*(perc1+perc2)*0.001)*10;
	var snet = net - minus;
	var unit = document.getElementById("unit").value;
    document.getElementById("minus").value = minus;
	document.getElementById("snet").value = snet;
	var hap = unit*snet;		
	document.getElementById("hap").value = hap;*/
}

function myFunction2() {	
	//var net = document.getElementById("net").value;
	var perc1 = parseInt(document.getElementById("perc1").value);
	var perc2 = parseInt(document.getElementById("perc2").value);
	var minus1 = 0;
	var perc = perc1+perc2;
	var minus1 = Math.floor(net*perc1*0.001)*10;
	var minus2 = Math.floor(net*perc2*0.001)*10;
	var minus = minus1+minus2;
	var snet = net-minus;

	document.getElementById("perc").value = perc;
	document.getElementById("minus1").value = minus1;
	document.getElementById("minus2").value = minus2;
	document.getElementById("minus").value = minus;
	document.getElementById("snet").value = snet;
	/*var unit = document.getElementById("unit").value;
	var fareunit = document.getElementById("fareunit").value;
	var snet = document.getElementById("snet").value;
	unit = unit*1;snet = snet*1;fareunit = fareunit*1
	var hap = (unit+fareunit)*snet;	
    document.getElementById("hap").value = hap;*/
}
</script>