<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

if($userStatus<3) echo "<script language='javascript'> alert('사용권한이 없습니다.'); location.replace('Login.php'); </script>";
else if($userStatus<9) $userAdmin = 1;
else $userAdmin = 9;

$now = date('Ymd');

$start_year = isset($_REQUEST["start_year"]); 
if($start_year=='') $start_year = date("Y");
else $start_year = $_REQUEST["start_year"];

$start_month = isset($_REQUEST["start_month"]); 
if($start_month=='') $start_month = date("m");
else $start_month = $_REQUEST["start_month"];

$start_day = isset($_REQUEST["start_day"]); 
if($start_day=='') $start_day = date("d");
else $start_day = $_REQUEST["start_day"];

$end_year = isset($_REQUEST["end_year"]); 
if($end_year=='') $end_year = date("Y");
else $end_year = $_REQUEST["end_year"];

$end_month = isset($_REQUEST["end_month"]); 
if($end_month=='') $end_month = date("m");
else $end_month = $_REQUEST["end_month"];

$end_day = isset($_REQUEST["end_day"]); 
if($end_day=='') $end_day = date("d");
else $end_day = $_REQUEST["end_day"];

$date_start = $start_year."-".$start_month."-".$start_day;
$date_end = $end_year."-".$end_month."-".$end_day;


isset($_REQUEST["vender"]) ? $vender = $_REQUEST["vender"]:$vender=''; 
if(($vender=='') || ($vender==0)) $vender = '';
else $vender = $_REQUEST["vender"];

isset($_REQUEST["item"]) ? $item = $_REQUEST["item"]:$item =''; 
if(($item=='') || ($item==0)) $item = '';
else $item = $_REQUEST["item"];

$status = isset($_REQUEST["status"]); 
if($status=='') $status = 0;
else $status = $_REQUEST["status"];

$carNo = isset($_REQUEST["carNo"]); 
if($carNo=='') $carNo = '';
else $carNo = $_REQUEST["carNo"];

if(!$_REQUEST["page"])$page=1;
else $page = $_REQUEST["page"];

$list_num=150;
$start_rec=($page-1)*$list_num;

if($userAdmin==9) $sql = "select count(IDX_) as dataCount from `TRESERVE` where WGUBN_='원재료' and VCOD_ like '%$vender%' and ICOD_ like '%$item%' and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end'";
else $sql = "select count(IDX_) as dataCount from `TRESERVE` where VCOD_ like '%$vender%' and ICOD_ like '%$item%' and ICOD_ != 411 and ICOD_ != 412 and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end'";

$result = $mysqli->query($sql);

$total_rec=$result->fetch_object()->dataCount;

$total_page=ceil($total_rec/$list_num);

$join = 1;

$postValue='?start_year='.$start_year.'&start_month='.$start_month.'&start_day='.$start_day.'&end_year='.$end_year.'&end_month='.$end_month.'&end_day='.$end_day.'&vender='.$vender.'&item='.$item.'&status='.$status.'&carNo='.$carNo.'&page='.$page;

?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 
</style>

</head>
<body>

<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a class="active" href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <?if($userAdmin==9) { ?> 
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="#"> 기타 </a>
  <? } else { ?>
  <a href="AdminETC2.php"> 기타 </a>
  <?}?>
</div>

<? 
	if($userAdmin==9) $sql = "select * from `TRESERVE` where WGUBN_='원재료' and VCOD_ like '%$vender%' and ICOD_ like '%$item%' and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end' order by IDX_ desc limit $start_rec, $list_num";
	else $sql = "select * from `TRESERVE` where VCOD_ like '%$vender%' and ICOD_ like '%$item%' and ICOD_ != 411 and ICOD_ != 412 and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end' order by IDX_ desc limit $start_rec, $list_num";
	$result = $mysqli->query($sql);

	if($vender=='') $venderView = '전체';
	else $venderView = $vender;
	if($item=='') $itemView = '전체';
	else $itemView = $item;
	if($status=='') $statusView = '예약';
	else if($status==0) $statusView = '예약';
	else $statusView = "계량";
	if($carNo=='') $carNoView = '전체';
	else $carNoView = $carNo;
		
	echo "<br /><div align=right> 검색조건 (거래처:".$venderView.") (품목:".$itemView.") (상태:".$statusView.") (차번:".$carNoView.") 기간:(".$date_start."~".$date_end.")</div>";

?>

<table>
  <tr>
    <th>예약날짜</th>
    <th>차량번호</th>
    <th>벤더</th>
	<th>벤더코드</th>
	<th>품목</th>
	<th>품목코드</th>
	<th>입출고</th>
	<th>상태</th>
	<th>업데이트시간</th>
  </tr>


 <? $k=0; while($row = $result->fetch_object()) { $k++; ?>
   <tr onclick="location.href='AdminReserveEdit.php<?=$postValue?>&idx=<?=$row->IDX_?>'" style="cursor:hand"> 
    <td> <?=$row->DATE_?> </td> 
	<td> <?=$row->CARNO_?> </td> 
	<td> <?=$row->VENDR_?> </td> 
	<td> <?=$row->VCOD_?> </td> 
	<td> <?=$row->ITEM_?> </td>
	<td> <?=$row->ICOD_?> </td> 
	<td> <?if($row->INOUT_==1) echo "입고"; else echo "출고";?> </td> 
	<td> <?if($row->STATUS_==0) echo "예약"; else echo "계량";?> </td>
	<td> <?=$row->UPDATETS_?> </td> 
   </tr> 
   <? } ?>
  </table>
  <br >
  <center>

 <? 
	$page_num = 10;
	$total_block=ceil($total_page/$page_num);
	$block = ceil($page/$page_num);
	$first=($block-1)*$page_num+1;
	$end=($block*$page_num);

	if($total_block==$block) $end=$total_page;
	if($block>1){
		echo "[<a href='AdminReserve.phppage=1&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end'> 처음 </a>]";
		echo "<a href='AdminReserve.php?page=".($first-1)."&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end'> ◀ </a>";
	}

	for($l=$first; $l<=$end; $l++){
		if($l==$page){
			echo "<b>  $l  </b>";
		}else{
			echo " [<a href='AdminReserve.php?page=$l&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end'>$l</a>] ";
		}
	}
    
	if($block<$total_block){
		echo "[<a href='AdminReserve.php?page=".($end+1)."&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end'>다음 $page_num 개 </a>]";
		echo "[<a href='AdminReserve.php?page=$total_page&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end'>마지막</a>]";
	}

	?>

<br>
<br>
<form name="search" method=post action='AdminReserve.php?page=1'>
검색기간 : 
<select name="start_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
<br><br>
<?
if($userAdmin==9) $sql = "select VCOD_, VENDR_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC"; 
else $sql = "select VCOD_, VENDR_ from `TVCOD` where WGUBN_!='원재료' order by VENDR_ ASC"; 
$result = $mysqli->query($sql);
?>
<select name="vender">
	<option value="0" selected> - 모든 거래처 - </option>;
	<? while($row=$result->fetch_object()){
		echo "<option value=".$row->VCOD_.">".$row->VENDR_."-".$row->VCOD_."</option>";
	}?>
</select>

<?
$sql = "select ICOD_, ITEM_ from `TICOD`"; 
$result = $mysqli->query($sql);
?>
<select name="item">
	<option value="0" selected> - 모든 품목 - </option>;
	<? while($row=$result->fetch_object()){
		echo "<option value=".$row->ICOD_.">".$row->ITEM_."-".$row->ICOD_."</option>";
	}?>
</select> 

<select name="status">
	<option value="0" selected> - 예약 - </option>
	<option value="1"> - 계량 - </option>
</select>

<br> 
<br>
차량번호 : <input type=text size=15 maxlength=30 name="carNo" align="left">

<input type="submit" value="검색" >
</form>

</center>

</body>
</html>
