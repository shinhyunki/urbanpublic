<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$now = date('Ymd');

$start_year = $_REQUEST["start_year"]; 
if($start_year=='') $start_year = date("Y");
else $start_year = $_REQUEST["start_year"];

$start_month = $_REQUEST["start_month"]; 
if($start_month=='') $start_month = date("m");
else $start_month = $_REQUEST["start_month"];

$start_day = $_REQUEST["start_day"]; 
if($start_day=='') $start_day = date("d");
else $start_day = $_REQUEST["start_day"];

$end_year = $_REQUEST["end_year"]; 
if($end_year=='') $end_year = date("Y");
else $end_year = $_REQUEST["end_year"];

$end_month = $_REQUEST["end_month"]; 
if($end_month=='') $end_month = date("m");
else $end_month = $_REQUEST["end_month"];

$end_day = $_REQUEST["end_day"]; 
if($end_day=='') $end_day = date("d");
else $end_day = $_REQUEST["end_day"];

echo $date_start."<br>";

$date_start = $start_year."-".$start_month."-".$start_day;
$date_end = $end_year."-".$end_month."-".$end_day;


$vender = $_REQUEST["vender"]; 
if(($vender=='') || ($vender==0)) $vender = '';
else $vender = $_REQUEST["vender"];

if(!$_REQUEST["page"])$page=1;
else $page = $_REQUEST["page"];

$list_num=10;
$start_rec=($page-1)*$list_num;

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$sql = "select count(distinct VENDR_, VCOD_, TRANSDATE_) as dataCount from `TDATA` where VCOD_ like '%$vender%' and TRANSDATE_>='$date_start' and TRANSDATE_<='$date_end' and STATUS_=5"; 
$result = $mysqli->query($sql);
$total_rec=$result->fetch_object()->dataCount;
$total_page=ceil($total_rec/$list_num);
$join = 1;

?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;
}

tr:hover {
	background-color:#f49d9d;
} 

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}
</style>
<script>
  function popup(){
	var myWindow = window.open("", "거래명세서", "width=650,height=1000");

}


</script>

</head>
<body>
<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a class="active" href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="#about"> 기타 </a>
</div>
<BR/><BR/>

<table>
 <tr>
   <th> 명세서 발행일 </th>
   <th> 거래처 </th>
   <th> 거래처코드 </th>
   
 </tr>

 <? 
 $sql = "select distinct VENDR_, VCOD_, TRANSDATE_ from `TDATA` where VCOD_ like '%$vender%' and TRANSDATE_>='$date_start' and TRANSDATE_<='$date_end' and STATUS_=5 order by VENDR_ asc limit $start_rec, $list_num "; 
 echo $sql;
 $result = $mysqli->query($sql);
	 while($row = $result->fetch_object()) { $k++; ?>
     <tr onclick=
	 'window.open("AdminTransReciept.php?venderCode=<?=$row->VCOD_?>&transDate=<?=$row->TRANSDATE_?>", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=200,width=700,height=1000");'  style="cursor:hand"> 
	  <td> <?=$row->TRANSDATE_?> </td>
      <td> <?=$row->VENDR_?> </td>
	  <td> <?=$row->VCOD_?> </td>	  
	</tr>
<? } ?>
</table>  
</form>

<br>
<form name="search" method=post action='AdminTras.php?page=1'>
검색기간 : 
<select name="start_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
<br><br>
<?
//$sql = "select VCOD_, VENDR_ from `TVCOD`"; 
$sql = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
$result = $mysqli->query($sql);
?>
<select name="vender">
	<option value="0" selected> - 모든 거래처 - </option>;
	<? while($row=$result->fetch_object()){
		echo "<option value=".$row->VCOD_.">".$row->VENDR_."-".$row->VCOD_."</option>";
	}?>
</select>

<input type="submit" value="검색" >
</form>
<br><br>

<br >
<center>

 <? 
	$page_num = 10;
	$total_block=ceil($total_page/$page_num);
	$block = ceil($page/$page_num);
	$first=($block-1)*$page_num+1;
	$end=($block*$page_num);

	if($total_block==$block) $end=$total_page;
	if($block>1){
		echo "[<a href='AdminTras.php?page=1&vender=$vender&date_start=$date_start&date_end=$date_end'> 처음 </a>]";
		echo "<a href='AdminTras.php?page=".($first-1)."&vender=$vender&date_start=$date_start&date_end=$date_end'> ◀ </a>";
	}

	for($l=$first; $l<=$end; $l++){
		if($l==$page){
			echo "<b>  $l  </b>";
		}else{
			echo " [<a href='AdminTras.php?page=$l&vender=$vender&date_start=$date_start&date_end=$date_end'>$l</a>] ";
		}
	}
    
	if($block<$total_block){
		echo "[<a href='AdminTras.php?page=".($end+1)."&vender=$vender&date_start=$date_start&date_end=$date_end'>다음 $page_num 개 </a>]";
		echo "[<a href='AdminTras.php?page=$total_page&vender=$vender&date_start=$date_start&date_end=$date_end'>마지막</a>]";
	}

	?>

<br>
</center>

</body>
</html>
