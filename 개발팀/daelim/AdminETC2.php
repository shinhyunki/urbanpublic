 <? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

if($userStatus<3) echo "<script language='javascript'> alert('사용권한이 없습니다.'); location.replace('Login.php'); </script>";
else if($userStatus<9) $userAdmin = 1;
else $userAdmin = 9;

?>


<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;
}

tr:hover {
	background-color:#f49d9d;
} 

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}
</style>
<script>
function forceInput2() {
	var url = 'AdminForceInput2.php';
	var popupVender = window.open(url, "계량데이터 수동 입력", "width=1500,height=500");
}

function popupVender() {
	var url = 'AdminVcodEdit.php';
	var popupVender = window.open(url, "거래처 코드 관리", "width=1600,height=1000");
}

function popupItem() {
	var url = 'AdminIcodEdit.php';
	var popupVender = window.open(url, "품목 코드 관리", "width=1000,height=1000");
}


</script>

</head>
<body>
<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <a class="active" href="AdminETC2.php"> 기타 </a>
</div>
<BR/><BR/>

<h3> 계량데이터 수동 입력 : <input type='button' value='수동 입력' onclick='forceInput2();'> </h3>
<br>
<h3> 거래처 추가, 삭제 : <input type='button' value='관리창 열기' onclick='popupVender();'> </h3>
<br>
<h3> 품목 추가, 삭제 : <input type='button' value='관리창 열기' onclick='popupItem();'> </h3>
<br>


</body>
</html>