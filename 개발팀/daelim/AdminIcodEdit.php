<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}


table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {

	font-size: 100%;
    padding: 0.5em 0.5em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

td:hover {
	background-color:#f49d9d;
}

.td_code {
	text-align: right;
}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>
function goBack() {
	location.replace('AdminReserve.php?page=1');
}

function goDel(icod) {
	var r = confirm("정말로 삭제하시겠습니까?");
	//if(r==true) alert(vcod);
	url = 'adminIcodEditDel.php?itemCode='+icod;
	if(r==true) location.replace(url);	
}
</script>
</head>
<body>

<? 
	$sql = "select * from `TICOD` order by `ICOD_` asc";
	$result = $mysqli->query($sql);
	

?>

<h2> 품목 코드 관리 </h2>
<table>
  <tr>
    <th>코드</th>
    <th>구분</th>
    <th>품명</th>
	<th>규격</th>
	<th colspan='2'> 추가 수정 삭제</th>
  </tr>
  <tr>
    <form method='post' action='AdminIcodEditList.php'>
    <td colspan='4' align='right' name='td_code'> 구분 : 
	 <select name='wGubn'>
	    <option value='원재료' "selected"> 원재료 </option>
		<option value='부재료' > 부재료 </option>
		<option value='폐합성수지'> 폐합성수지 </option>
		<option value='소각폐기물'> 소각폐기물 </option>
		<option value='제품판매'> 제품판매 </option>
		<option value='기타'> 기타 </option>
	  </select>
	품명 입력 : <input type='text' name='itemName' size='20'> </td>	
	<td colspan='2'><input type='submit' value='추가하기''> </td>		
	</form>
  </tr>
  <? while($row = $result->fetch_object()) { ?>
  <tr>
    <form method='post' action='adminIcodEdit_ok.php'>
    <span style='text-align: right;'> <td><input type='text' name='itemCode' size='5' value='<?=$row->ICOD_?>' readonly> </td></span>
	<td>
	  <select name='wGubn'>
	    <option value='원재료' <?if($row->WGUBN_=='원재료') echo "selected"?>> 원재료 </option>
		<option value='부재료' <?if($row->WGUBN_=='부재료') echo "selected"?>> 부재료 </option>
		<option value='폐합성수지' <?if($row->WGUBN_=='폐합성수지') echo "selected"?>> 폐합성수지 </option>
		<option value='소각폐기물' <?if($row->WGUBN_=='소각폐기물') echo "selected"?>> 소각폐기물 </option>
		<option value='제품판매' <?if($row->WGUBN_=='제품판매') echo "selected"?>> 제품판매 </option>
		<option value='기타' <?if($row->WGUBN_=='기타') echo "selected"?>> 기타 </option>
	  </select>
	</td>	
	<td><input type='text' name='itemName' size='15' value='<?=$row->ITEM_?>'> </td>
	<td><input type='text' name='itemSpec' size='50' value='<?=$row->SPEC_?>'> </td>
	<td><input type='submit' value='수정'> </td>		
	<td><input type='button' value='삭제' onclick="goDel('<?=$row->ICOD_?>')"> </td>			
	</form>
  </tr>
  <?}?>
  </table>


</body>
</html>
