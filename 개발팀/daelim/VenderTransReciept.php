<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<? include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$venderCode=$_GET["venderCode"];
$transDate=$_GET["transDate"];

$sqlH = "SELECT `HAP_` FROM `TDATA` WHERE `TRANSDATE_`='$transDate' and `STATUS_`=5 and `VCOD_`='$venderCode' order by `IDX_` asc"; 

$resultH = $mysqli->query($sqlH);

$habgae = 0;

while($rowH=$resultH->fetch_object()) { 
	$habgae += $rowH->HAP_;
	
}


$sql = "SELECT DATE_, CARNO_, ITEM_, SNET_, UNIT_, HAP_ FROM `TDATA` WHERE `TRANSDATE_`='$transDate' and `STATUS_`=5 and `VCOD_`='$venderCode' order by `IDX_` asc"; 
$result = $mysqli->query($sql);

$hap = 0;

$sqlMP = "SELECT * FROM `TVCOD` WHERE `VCOD_`='$venderCode'";
$resultMP = $mysqli->query($sqlMP);
$rowMP = $resultMP->fetch_object();

?>

<!DOCTYPE html>
<html lang="ko">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script src="script/jquery-latest.min.js"></script>
    <script type="text/javascript" src="script/jquery.battatech.excelexport.js"></script>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }
        table{
            width: 600px;
            text-align: center;
            border: 1px solid black;
			font-size:12px;
        }
		.btn {
		   background-color: hotpink;
		    color: white;
		    padding: 10px 10px;
		    border: none;
		    cursor: pointer;
		    width: 20%;
		    opacity: 0.9;
			margin : auto;			
		}
    </style>
	<script>
		function goURL(URL) {
			var url = URL+'.php';
			location.replace(url);
		}
	</script>
</head>
<body>
 <div id="wrap" align='center'>
  <table id='tblExport' border=1>
   <tbody>
    <tr>
	 <td colspan='10' align='center'><font size='6'> 거래명세표 </font> </td>
	</tr>
	<tr>
     <td> 일자 </td>
     <td colspan='4'> <?=$transDate?> </td>
     <td> 등록번호 </td>
     <td colspan='4' align='center'> <?=substr($rowMP->REGINO_,0,3)."-".substr($rowMP->REGINO_,3,2)."-".substr($rowMP->REGINO_,5,5)?></td>
    </tr>
    <tr>
     <td rowspan='2' colspan ='5' align='center'> <font size='5'>대림제지 귀하</font> </td>
     <td> 상호 </td>
     <td colspan='2'> <?=$rowMP->VENDR_; ?> </td>
     <td> 성명 </td>
     <td> <?=$rowMP->NAME_?> </td>  
    </tr>
	<tr>
     <td> 사업장<br>소재지 </td>
     <td colspan='4'> <?=$rowMP->ADDR_?> </td>
    </tr>
    <tr>
     <td colspan='5'> 아래와 같이 계산합니다. </td>
     <td> 업태 </td>
     <td colspan='2'> <?=$rowMP->JONGMOK_?> </td>
     <td> 종목 </td>
     <td> <?=$rowMP->UPTAE_?> </td>  
    </tr>
    <tr>
     <td> 합계 <br> 금액 </td>
     <td colspan='9' align='center'><font size='5'> &#92; <?=number_format($habgae*1.1)?></font></td>
    </tr>
    <tr>
     <td>년월</td>
     <td>일</td>
     <td>품목</td>
     <td>차량번호</td>
     <td>규격</td>
     <td>수량</td>
     <td>단가</td>
     <td colspan='2'>공급가액</td>
     <td>세액</td>
    </tr>
	<? while($row=$result->fetch_object()) { ?>
	<tr>
     <td><?=str_replace('-','.',substr($row->DATE_, 0, 7))?></td>
     <td><?=substr($row->DATE_,-2,2)?></td>
     <td><?=$row->ITEM_?></td>
     <td><?=$row->CARNO_?></td>
     <td></td>
     <td align='right'><?=number_format($row->SNET_)?></td>
     <td align='right'><?=number_format($row->UNIT_)?></td>
     <td colspan='2' align='right'><? echo number_format($row->HAP_); $hap += $row->HAP_;?></td>
     <td align='right'><?=number_format($row->HAP_*0.1)?></td>
    </tr>
	<?}?>
	<tr>
     <td colspan='7'>소계</td>     
     <td colspan='2' align='right'><?=number_format($hap)?></td>
     <td align='right'><?=number_format($hap*0.1)?></td>
    </tr>
	<tr>
     <td colspan='7'>부가가치세 포함 합계</td>     
     <td colspan='2' align='right'><?=number_format($hap*1.1)?></td>
     <td align='right'></td>
    </tr>
   </tbody>
  </table>     
 </div>
 <br /><br />	  
 <center>
 <button type="button" class="btn" style="background-color: #555556;" onclick="goURL('VenderTrans')"> 뒤로 </button>
  <a id="btnExport" href="#" download="<?='대림제지_'.$rowMP->SCMNM1.'_'.$transDate.'.xls'?>"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a>
	<button type="button" class="btn" onclick="window.print()"> 인쇄 </button> 
 </center>
 
<script type="text/javascript">
    $(document).ready(function () {
 
        function itoStr($num)
        {
            $num < 10 ? $num = '0'+$num : $num;
            return $num.toString();
        }
         
        var btn = $('#btnExport');
        var tbl = 'tblExport';
 
        btn.on('click', function () {
            var dt = new Date();
            var year =  itoStr( dt.getFullYear() );
            var month = itoStr( dt.getMonth() + 1 );
            var day =   itoStr( dt.getDate() );
            var hour =  itoStr( dt.getHours() );
            var mins =  itoStr( dt.getMinutes() );
 
            var postfix = year + month + day + "_" + hour + mins;
            var fileName = "Daelim_"+ postfix + ".xls";
 
            var uri = $("#"+tbl).excelexportjs({
                containerid: tbl
                , datatype: 'table'
                , returnUri: true
            });
 
            $(this).attr('download', fileName).attr('href', uri).attr('target', '_blank');
        });
    });
</script>
</body>
</html>