/* <meta charset=utf-8>
<a id="btnExport" href="#" download="<?='대림제지_'.$rowMP->SCMNM1.'_'.$transDate.'.sql'?>"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a><br /> */
<?php

include 'db_access.php';

backup_tables('localhost',$db_id,$db_pw,$db_name);

function backup_tables($db_host,$db_id,$db_pw,$db_name,$tables = '*') {
  $mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
  $mysqli->query("SET NAMES 'utf8'");
  
  if($tables == '*')  {
    $tables = array();
    $result = $mysqli->query('show TABLES');

    while($row = $result->fetch_array()) {
      $tables[] = $row[0];	  
    }
  } else {
    $tables = is_array($tables) ? $tables : explode(',',$tables);
  }


  foreach($tables as $table) {
    $result = $mysqli->query('SELECT * FROM '.$table);
    $num_fields = $mysqli->field_count;
    $return.= 'DROP TABLE '.$table.';';
	$row2 = $mysqli->query('SHOW CREATE TABLE '.$table)->fetch_row();
    $return.= "\n\n".$row2[1].";\n\n";

		for ($i = 0; $i < $num_fields; $i++) {
      while($row = $result->fetch_row()) {
		  
        $return.= 'INSERT INTO '.$table.' VALUES(';
        for($j=0; $j<$num_fields; $j++) {
			
          $row[$j] = addslashes($row[$j]);		  
		  
          $row[$j] = preg_replace_callback('\n','\\n',$row[$j]);		  	  
		  
          if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
          if ($j<($num_fields-1)) { $return.= ','; }
		  
        }
        $return.= ");\n";
      }
    } 
    $return.="\n\n\n";
	//echo nl2br($return);	
	echo $return;	
  }
  $handle = fopen('db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
  fwrite($handle,$return);
  fclose($handle);
  
}


?>

