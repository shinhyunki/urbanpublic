<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding: 10px 3px;
}

table td a {
	width:100%;
	display:block;
	height:100%;
}

table td a:hover{
　  background-color: #f49d9d;
} 

tr:nth-child(even) {
    background-color: #f2f2f2
}


.btn {
    background-color: hotpink;
    color: white;
    padding: 10px 20px;
    border: none;
    cursor: pointer;
    width: 40%;
    opacity: 0.9;
	margin : auto;
	font-size:18px;
}

.btn:hover {
    opacity: 1;
}

 input, select{
	font-size:16px;
	width:80%;
 }

 .input-container {
    display: -ms-flexbox; /* IE10 */
    display: flex;    
}

.input-field {    
    outline: none;
	/*font-size:20px;*/
	width:30%;
}

.input-field:focus {
    border: 2px solid dodgerblue;
}

.radio {
width:10%;
}
.button {
width:150px;
height:40px;
margin-left: 10px;
}
</style>
<script>
var moisturePer=0; var moistureSum=0;var foreignPer=0; var foreignSum=0; var totalPer=0; var totalSum=0; 

	function thou(num) {	
		return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	function moisture(division) {
		if(division=='plus') moisturePer++;
		else moisturePer--;
		if(moisturePer<0) { 
			alert("감률은 음수일수 없습니다");
			moisturePer = 0;
			return false;
		}
		moistureSum = (grossWeight - regWeight)*moisturePer*0.01;		
		totalPer = moisturePer + foreignPer;
		totalSum = moistureSum + foreignSum;
		
		totalSumFix = totalSum.toFixed();
		moistureSumFix = moistureSum.toFixed();
		moistureSumFix2 = moistureSumFix/10;
		moistureSumFix2 = moistureSum = moistureSumFix2.toFixed() * 10;
		totalSumFix = totalSum = moistureSumFix2 + foreignSum;
		realSum = (grossWeight - regWeight) - totalSum;
		realSumFix = realSum.toFixed();
		document.getElementById("moisturePer").innerHTML = moisturePer;	
		document.getElementById("moistureSum").innerHTML = thou(moistureSumFix2);	
		document.getElementById("totalPer").innerHTML = totalPer;	
		document.getElementById("totalSum").innerHTML = thou(totalSumFix);	
		document.getElementById("realSum").innerHTML = thou(realSumFix);
		test.moisturePer.value = moisturePer;
		test.moistureSum.value = moistureSumFix2;
		test.totalPer.value = totalPer;
		test.totalSum.value = totalSumFix;
		test.moistureCheck.value = 1;
	}

	function foreign(division) {
		if(division=='plus') foreignPer++;
		else foreignPer--;
		if(foreignPer<0) { 
			alert("감률은 음수일수 없습니다");
			foreignPer = 0;
			return false;
		}
		foreignSum = (grossWeight - regWeight)*foreignPer*0.01;
		totalPer = moisturePer + foreignPer;
		totalSum = moistureSum + foreignSum;
		
		totalSumFix = totalSum.toFixed();
		foreignSumFix = foreignSum.toFixed();
		foreignSumFix2 = foreignSumFix/10;
		foreignSumFix2 = foreignSum = foreignSumFix2.toFixed() * 10;
		totalSumFix =  totalSum = moistureSum + foreignSumFix2;
		realSum = (grossWeight - regWeight) - totalSum;
		realSumFix = realSum.toFixed();
		document.getElementById("foreignPer").innerHTML = foreignPer;	
		document.getElementById("foreignSum").innerHTML = thou(foreignSumFix2);
		document.getElementById("totalPer").innerHTML = totalPer;	
		document.getElementById("totalSum").innerHTML = thou(totalSumFix);
		document.getElementById("realSum").innerHTML = thou(realSumFix);
		test.foreignPer.value = foreignPer;	
		test.foreignSum.value = foreignSumFix2;
		test.totalPer.value = totalPer;
		test.totalSum.value = totalSumFix;
		test.foreignCheck.value = 1;
	}

	function goBack() {
		 history.go(-1);	
	}

</script>
</head>

<body>



<?
include 'db_access.php';

$idx = $_REQUEST["idx"];

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$join_id = $_SESSION["join_id"];
$sql = "SELECT `USERNAME_`, `USERSTATUS_` FROM `TUSERINFO` WHERE USERID_ = '$join_id'"; 
$result = $mysqli->query($sql);
$user = $result->fetch_object();
$userName = $user->USERNAME_;
if($user->USERSTATUS_!=7)  echo "<script language='javascript'> alert('검수자 아이디로 다시 로그인하세요.'); location.replace('Login.php'); </script>";
if(!($userName>0 and $userName<1000))  echo "<script language='javascript'> alert('검수자 이름(번호)을 확인하세요. '); location.replace('Login.php'); </script>";

$sql = "select CARNO_, VCOD_, VENDR_, ICOD_, ITEM_, GROSS_, CAR_, REGCARWGT_, PERC_, MINUS_, PERC1_, MINUS1_, PERC2_, MINUS2_, GRADE_, ORIENT_, STATUS_ from `TDATA` where IDX_='$idx'"; 
$result = $mysqli->query($sql);
$row = $result->fetch_object();

$carNo = $row->CARNO_;
$venderCode = $row->VCOD_;
$venderName = $row->VENDR_;
$itemCode = $row->ICOD_;
$itemName = $row->ITEM_;
$grossWeight = $row->GROSS_;
$carWeight = $row->CAR_;
$regCarWeight = $row->REGCARWGT_;

if($carWeight) $regCarWeight = $carWeight;
$grade = $row->GRADE_;
$orient = $row->ORIENT_;

$minus = $row->MINUS_;
$minus1 = $row->MINUS1_;
$minus2 = $row->MINUS2_;
$minus = $minus1+$minus2;
$perc = $row->PERC_;
$perc1 = $row->PERC1_;
$perc2 = $row->PERC2_;

if($row->STATUS_==1) {
	$perc = 0;
	$perc1 = 0;
	$perc2 = 0;
	$minus = 0;
	$minus1 = 0;
	$minus2 = 0;
} 
 ?>
 <script>
  moisturePer=<?=$perc1?>; moistureSum=<?=$minus1?>; foreignPer=<?=$perc2?>; foreignSum=<?=$minus2?>; totalPer=<?=$perc?>; totalSum=<?=$minus?>; 
 </script>

 <?


echo "<h3>".$userName." : 검수차량번호 : ".$carNo."</h3>";
?> 
<FORM name="test" method="post" action="inspectConfirm_ok.php">
<table>
 <tr>
  <td> 거래처 </td>
  <td colspan='3'>
   <select name="venderName">
    <?
	  $sql2 = "SELECT VCOD_, VENDR_ FROM `TVCOD` WHERE `WGUBN_` = '원재료' order by VENDR_ asc "; 
	  $result2 = $mysqli->query($sql2);
	  while($row2=$result2->fetch_object()) { ?>
       <option value="<?=$row2->VCOD_.':'.$row2->VENDR_?>" <?if($row2->VCOD_==$venderCode) echo "selected"?>> <?=$row2->VENDR_.':'.$row2->VCOD_?> </option>
	<? } ?>
   </select>
  </td>
 </tr>
 <tr>
  <td> 품명 </td>
  <td colspan='3'>
   <select name="itemName">
    <?
	  $sql3 = "SELECT ICOD_, ITEM_ FROM `TICOD` WHERE `WGUBN_` = '원재료' order by ICOD_ asc"; 
	  $result3 = $mysqli->query($sql3);
	  while($row3=$result3->fetch_object()) { ?>
       <option value="<?=$row3->ICOD_.':'.$row3->ITEM_?>" <?if($row3->ICOD_==$itemCode) echo "selected"?>> <?=$row3->ITEM_.':'.$row3->ICOD_?> </option>
	<? } ?>
   </select>
  </td>
 </tr>
 <tr>
  <td> 총중량 </td>
  <td> <span name='gross'> <?=number_format($grossWeight)?></span>kg</td>
  <td> 공 차 </td>
  <td> <span name='car'> <?=number_format($regCarWeight)?></span>kg</td>
 </tr>
  <tr>
  <td> 실중량 </td>
  <td> <?=number_format($grossWeight-$regCarWeight-$minus)?>kg</td>
  <td> 인수량 </td>
  <td> <font color='red'><span id='realSum'> <?=number_format($grossWeight-$regCarWeight-$minus)?></span> kg<script>grossWeight="<?=$grossWeight?>";regWeight="<?=$regCarWeight?>"</script></font></td>
 </tr>
 <tr>
  <td> 감률계 </td>
  <td> <font color='blue'><span id='totalPer'> <?=$perc?></span> %</font></td>
  <td> 감량계 </td>
  <td> <font color='blue'><span id='totalSum'> <?=number_format($minus)?></span> kg</font></td>
 </tr>
 <tr>
  <td> 수분감률 </td>
  <td> <span id='moisturePer' name='moisturePer'> <?=$perc1?></span> %</td>
  <td> 수분감량 </td>
  <td> <span id='moistureSum' name='moistureSum'> <?=number_format($minus1)?></span> kg</td>
 </tr>
 <tr>
  <td> 이물질감률 </td>
  <td> <span id='foreignPer' name='foreignPer'> <?=$perc2?></span> %</td>
  <td> 이물질감량 </td>
  <td> <span id='foreignSum' name='foreignSum'> <?=number_format($minus2)?></span> kg</td>
 </tr>
 <tr>
  <td colspan='2'> <input type='button' class='button' value='수분감소' onclick="moisture('minus')"> </td>
  <td colspan='2'> <input type='button' class='button' value='수분증가' onclick="moisture('plus')"> </td>
 </tr>
 
  <tr>
  <td colspan='2'> <input type='button' class='button' value='이물질감소' onclick="foreign('minus')"> </td>
  <td colspan='2'> <input type='button' class='button' value='이물질증가' onclick="foreign('plus')"> </td>
 </tr>
 </tr>
  <tr>
  <td colspan='1'> 등급 </span></td>
  <td colspan='3'><input type='radio' class='radio' name='grade' value='A+' <?if($grade=='A+') echo 'checked';?>>A+ <input type='radio' class='radio' name='grade' value='A' <?if($grade=='A') echo 'checked';?>>A <input type='radio' class='radio' name='grade' value='B+' <?if($grade=='B+') echo 'checked';?>>B+ <input type='radio' class='radio' name='grade' value='B' <?if($grade=='B' or $grade=='') echo 'checked';?>>B <br><input type='radio' class='radio' name='grade' value='C+' <?if($grade=='C+') echo 'checked';?>>C+ <input type='radio' class='radio' name='grade' value='C' <?if($grade=='C') echo 'checked';?>>C <input type='radio' class='radio' name='grade' value='E' <?if($grade=='E') echo 'checked';?>>벌크 </td>
 </tr>
 <tr>
  <td colspan='1'> 구분 </td>
  <td colspan='3'><input type='radio' class='radio' name='orient' value='공장' <?if($orient=='공장' or $orient=='') echo 'checked';?>>공장 <input type='radio' class='radio' name='orient' value='생활' <?if($orient=='생활') echo 'checked';?>>생활 </td>
  
 </tr>
</table>


<br>
<center> <button type="button" class="btn" style="background-color: #555555" onClick="goBack();"> 취소 </button> <button type="submit" class="btn"> 등록 </button> </center>
<input type='hidden' name='moistureCheck' value='0'>
<input type='hidden' name='foreignCheck' value='0'>
<input type='hidden' name='moisturePer' value='<?=$moisturePer?>'>
<input type='hidden' name='moistureSum' value='<?=$moistureSum?>'>

<input type='hidden' name='foreignPer' value='<?=$foreignPer?>'>
<input type='hidden' name='foreignSum' value='<?=$foreignSum?>'>
<input type='hidden' name='totalPer' value='<?=$foreignPer?>'>
<input type='hidden' name='totalSum' value='<?=$foreignSum?>'>

<input type='hidden' name='idx' value='<?=$idx?>'>
<input type='hidden' name='userName' value='<?=$userName?>'>

<br>

</form> 

<br>
<table>
  <tr> 
    <th> 날짜 </th>
	<th> 차번 </th>
	<th> 총중 </th>
	<th> 공차 </th>
	<th> 실중 </th>
	<th> 감률 </th>
	<th> 인수량 </th>
  </tr>

<?
$now=date('Y-m-d');
$sql = "select DATE_, CARNO_, GROSS_, CAR_ ,NET_,SNET_, PERC_, MINUS_ from `TDATA` where VCOD_='$venderCode' and STATUS_>2 and DATE_ < '$now' order by IDX_ DESC limit 10"; 
$result = $mysqli->query($sql);
while($row = $result->fetch_object()) { ?>
  <tr>
	<td><?=substr($row->DATE_,5,5)?></td>
	<td><?=substr($row->CARNO_,-4)?></td>
	<td><?=number_format($row->GROSS_)?></td>
	<td><?=number_format($row->CAR_)?></td>
	<td><?=number_format($row->NET_)?></td>
	<td><?=$row->PERC_?></td>
	<td><?=number_format($row->SNET_)?></td>	
  </tr>
<? } ?>
</table>
<br /><br />
</body>
</html>
