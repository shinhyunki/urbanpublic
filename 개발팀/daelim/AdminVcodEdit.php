<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
$userID = $_SESSION["join_id"];
?>

<?
include 'db_access.php'; 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}


table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {

	font-size: 100%;
    padding: 0.5em 0.5em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

td:hover {
	background-color:#f49d9d;
}

.td_code {
	text-align: right;
}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>
function goBack() {
	location.replace('AdminReserve.php?page=1');
}

function goDel(vcod, wgubn) {
	var r = confirm("정말로 삭제하시겠습니까?");
	//if(r==true) alert(vcod);
	url = 'adminVcodEditDel.php?venderCode='+vcod;
	if(r==true) location.replace(url);	
}
</script>
</head>
<body>

<? 
	$sqlUser = "select USERSTATUS_ from `TUSERINFO` where USERID_ = '$userID'"; 
	$UserGrade = $mysqli->query($sqlUser)->fetch_object()->USERSTATUS_;

	if($UserGrade == 9) $sql = "select * from `TVCOD` where WGUBN_='원재료' order by `VCOD_` asc";
	else $sql = "select * from `TVCOD` where WGUBN_!='원재료' order by `WGUBN_` asc, `VENDR_` asc ";		
	$result = $mysqli->query($sql);
?>

<h2> 거래처 코드 관리 </h2>
<table>
  <tr>
    <th>아이디</th>
    <th>구분</th>
    <th>코드</th>
    <th>거래처</th>
    <th>사업자번호</th>
	<th>대표자</th>
	<th>업태</th>
	<th>종목</th>
	<th>주소</th>
	<th colspan='2'> 추가 수정 삭제</th>
  </tr>
  <tr>
    <form method='post' action='AdminVcodEditList.php'>
    <td colspan='9' align='right' name='td_code'> 
	  <select name='wGubn'>
	    <?if($UserGrade==9) { ?> <option value='원재료' selected> 원재료 </option> <?}?>
		<option value='부재료' <?if($UserGrade!=9) echo "selected"?>> 부재료 </option>
		<option value='폐합성수지'> 폐합성수지 </option>
		<option value='소각폐기물'> 소각폐기물 </option>
		<option value='제품판매'> 제품판매 </option>
		<option value='기타'> 기타 </option>
	  </select> 
	  거래처 이름 입력 : <input type='text' name='venderName' size='20'> </td>	
	<td colspan='2'><input type='submit' value='추가하기''> </td>		
	</form>
  </tr>
  <? while($row = $result->fetch_object()) { 
	  $sqlMat = "select count(IDX_) as idx from `TUSERINFO` where VCOD_ = '$row->VCOD_'"; 
	  $resultMat = $mysqli->query($sqlMat);
	  $idxCount = $resultMat->fetch_object()->idx; 
	  if($idxCount) $reg = $idxCount.'개';
	  else $reg = '<font color=red> 미등록</font>'
	  
	  ?>
  <tr>
    <form method='post' action='adminVcodEdit_ok.php'>
	<td><?=$reg?> </td>
	<span style='text-align: right;'> <td><input type='text' name='wGubn' size='2' value='<?=$row->WGUBN_?>' readonly> </td></span>
    <span style='text-align: right;'> <td><input type='text' name='venderCode' size='2' value='<?=$row->VCOD_?>' readonly> </td></span>
	<td><input type='text' name='venderName' size='10' value='<?=$row->VENDR_?>'> </td>
	<td><input type='text' name='regiNo' size='10' value='<?=$row->REGINO_?>'> </td>
	<td><input type='text' name='ceoName' size='2' value='<?=$row->NAME_?>'> </td>
	<td><input type='text' name='uptae' size='10' value='<?=$row->UPTAE_?>'> </td>
	<td><input type='text' name='jongmok' size='30' value='<?=$row->JONGMOK_?>'> </td>
	<td><input type='text' name='address' size='50' value='<?=$row->ADDR_?>'> </td>	
	<td><input type='submit' value='수정'> </td>		
	<td><input type='button' value='삭제' onclick=goDel("<?=$row->VCOD_.'@'.$row->WGUBN_?>") </td>			
	</form>
  </tr>
  <?}?>
  </table>


</body>
</html>
