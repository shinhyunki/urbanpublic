<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

if($userStatus<3) echo "<script language='javascript'> alert('사용권한이 없습니다.'); location.replace('Login.php'); </script>";
else if($userStatus<9) $userAdmin = 1;
else $userAdmin = 9;

$idx = $_REQUEST["idx"];
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;	
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}

</style>
<script>
function goBack() {
	location.replace('AdminUser.php?page=1');
}

function goDel() {
	var r = confirm("정말로 삭제하시겠습니까?");
	if(r==true) location.replace('adminUserEditDel.php?idx=<?=$idx?>');	
}
</script>

</head>
<body>


<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a class="active" href="AdminUser.php?page=1"> 사용자관리 </a>
  <?if($userAdmin==9) { ?> 
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="#"> 기타 </a>
  <? } else { ?>
  <a href="AdminETC2.php"> 기타 </a>
  <?}?>
</div>


<?  $mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
	$mysqli->query("SET NAMES 'utf8'");
	$sql = "select * from `TUSERINFO` where IDX_ = '$idx'";
	$result = $mysqli->query($sql);
	$row = $result->fetch_object();
?>
<h2> 사용자 정보 수정 삭제 </h2>
<form  name="adminUserEdi" method="post" action="adminUserEdit_ok.php">
<table>
  <tr>
    <th>벤더이름 : 벤더코드</th>    
	<th>사용자ID</th>
	<th>사용자이름</th>
	<th>이메일</th>
	<th>전화번호</th>
	<th>등급</th>
  </tr>
  <!-- <tr> <th colspan='7'> 기존 정보 </th> </tr> -->
   <tr onclick="location.href='AdminUserEdit.php?idx=<?=$row->IDX_?>'" style="cursor:hand"> 
    <td> <?=$row->VENDR_.":".$row->VCOD_?></td> 	
	<td> <?=$row->USERID_?> </td> 
	<td> <?=$row->USERNAME_?> </td> 
	<td> <?=$row->USEREMAIL_?> </td> 	
	<td> <?=$row->USERPHONE_?> </td>
	<td> <?=$row->USERSTATUS_?> </td>
   </tr>
  <!--<tr> <th colspan='7'> 수정 정보 </th> </tr>-->
  <tr>
    <td> 
		<select name='venderName'>			
			<?
			if($userStatus==9) $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
			else $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_!='원재료' order by VENDR_ ASC";			
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->VENDR_.":".$rowVender->VCOD_?>' <?if($rowVender->VENDR_==$row->VENDR_) echo 'selected'?>> <?=$rowVender->VENDR_.":".$rowVender->VCOD_?> </option>
			<?}?>
		</select>	
	</td>	

	<td> <input type='text' name='userID' value='<?=$row->USERID_?>'> </td> 
	<td> <input type='text' name='userName' value='<?=$row->USERNAME_?>'> </td> 
	<td> <input type='text' name='userEmail' value='<?=$row->USEREMAIL_?>'> </td> 
	<td> <input type='text' name='userPhone' value='<?=$row->USERPHONE_?>'> </td> 
	<td> 
		<select name='userStatus'>
			<option value='0' <?if($row->USERSTATUS_==0) echo 'selected'?>> 신청:0 </option>
			<option value='1' <?if($row->USERSTATUS_==1) echo 'selected'?>> 정지:1 </option>
			<option value='2' <?if($row->USERSTATUS_==2) echo 'selected'?>> 기사:2 </option>
			<option value='3' <?if($row->USERSTATUS_==3) echo 'selected'?>> 업체:3 </option>
			<option value='4' <?if($row->USERSTATUS_==4) echo 'selected'?>> 대림:4 </option>
			<option value='5' <?if($row->USERSTATUS_==5) echo 'selected'?>> 대림:5 </option>
			<?if($userAdmin==9) { ?> <option value='7' <?if($row->USERSTATUS_==7) echo 'selected'?>> 검수:7 </option>
			<option value='9' <?if($row->USERSTATUS_==9) echo 'selected'?>> 관리:9 </option> <?}?>
		</select>
	</td>	
  </tr>
  </table>
  <div align='right'> 등급 0:신청 1:정지 2:기사 3:업체관리자 7:검수 9:관리자 </div>
  <br>
  <br>
  <input type='hidden' name="idx" value="<?=$row->IDX_?>">
  <center>
 <button type="button" class="btnB" onClick="goBack()" > 취소 </button>
 <button type="button" class="btnR" onClick="goDel()" > 삭제 </button>
 <button type="submit" class="btn"> 수정 </button>
  </center>
 <br /><br />
 </form>
  
</body>
</html>
