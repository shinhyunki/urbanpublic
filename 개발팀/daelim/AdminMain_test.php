<meta charset='utf-8'>
<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?

include 'db_access.php'; 
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

if($userStatus<3) echo "<script language='javascript'> alert('사용권한이 없습니다.'); location.replace('Login.php'); </script>";
else if($userStatus<9) $userAdmin = 1;
else $userAdmin = 9;

$now = date('Ymd');
$date_start; $start_year;
if(!isset($_REQUEST["date_start"])) {
	$start_year = $_REQUEST["start_year"] ?? ''; 
	if($start_year=='') $start_year = date("Y");
	else $start_year = $_REQUEST["start_year"];

	$start_month = isset($_REQUEST["start_month"]); 
	if($start_month=='') $start_month = date("m");
	else $start_month = $_REQUEST["start_month"];

	$start_day = isset($_REQUEST["start_day"]); 
	if($start_day=='') $start_day = date("d");
	else $start_day = $_REQUEST["start_day"];

	$end_year = $_REQUEST["end_year"] ?? ''; 
	if($end_year=='') $end_year = date("Y");
	else $end_year = $_REQUEST["end_year"];

	$end_month = $_REQUEST["end_month"] ?? ''; 
	if($end_month=='') $end_month = date("m");
	else $end_month = $_REQUEST["end_month"];

	$end_day = $_REQUEST["end_day"] ?? ''; 
	if($end_day=='') $end_day = date("d");
	else $end_day = $_REQUEST["end_day"];

	$date_start = $start_year."-".$start_month."-".$start_day;
	$date_end = $end_year."-".$end_month."-".$end_day;
} else {
	$date_start = $_REQUEST["date_start"];
	$date_end = $_REQUEST["date_end"];
}


$vender = $_REQUEST["vender"] ?? ''; 
if(($vender=='') || ($vender==0)) $vender = '';
else $vender = $_REQUEST["vender"];

//echo $_REQUEST["item"]; 
$item = $_REQUEST["item"] ?? ''; 
if($item=='') $item = '원재료';
else if($item=='0') {
	$item = '';
	$itemGubn = '0';
}
if($item=='원재료' and $userAdmin<9) $item = '';
else if($item=='0') {
	$item = '';
	$itemGubn = '0';
}

//else $item = $_REQUEST["item"];


$status = $_REQUEST["status"] ?? ''; 
if(($status=='') || ($status==0)) $status = '';
else $status = $_REQUEST["status"];
//echo $status;


$carNo = $_REQUEST["carNo"] ?? ''; 
if($carNo=='') $carNo = '';
else $carNo = $_REQUEST["carNo"];

if(!isset($_REQUEST["page"]))$page=1;
else $page = $_REQUEST["page"];

$list_num = $_REQUEST["list_num"] ?? ''; 
if($list_num=='') $list_num = '150';
else $list_num = $_REQUEST["list_num"];

$start_rec=($page-1)*$list_num;
//$endlimit = $start_rec+$list_num;
$endlimit = $list_num;


if($userAdmin==9) $sql = "select count(IDX_) as dataCount from `TDATA` where VCOD_ like '%$vender%' and WGUBN_ like '%$item%' and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end'"; 
else $sql = "select count(IDX_) as dataCount from `TDATA` where VCOD_ like '%$vender%' and WGUBN_ like '%$item%' and WGUBN_ !='원재료' and WGUBN_ !='운임' and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end'";


$result = $mysqli->query($sql);

$total_rec=$result->fetch_object()->dataCount;

$total_page=ceil($total_rec/$list_num);

$join = 1;
$hapSnet = 0;
$hapHap = 0;


?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.js"></script>
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: right;
	font-size: 95%;
    padding: 1em 0.5em;
	border: 0.1em solid #eee;
}


th {
	 text-align: center;
}
tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select, option {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 


/* Tooltip container */
.tooltip {
  position: relative;
  display: inline-block;  
}

/* Tooltip text */
.tooltiptext {
  visibility: hidden;
  width: 200px;
  background-color: black;
  color: #fff;
  text-align: center;
  padding: 5px 0;
  border-radius: 6px;
 
  /* Position the tooltip text - see examples below! */
  position: absolute;
  z-index: 10;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}


</style>
<script>

</script>
</head>
<body>

<!-- Logo -->
<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div>

<div align='right'> <a href="Login.php"> 로그아웃(로그인 페이지 이동) </a> </div><br>

<!-- Navigation -->
<div class="topnav">
  <a class="active" href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <?if($userAdmin==9) { ?> 
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="AdminETC.php"> 기타 </a>
  <? } else { ?>
  <a href="AdminETC2.php"> 기타 </a>
  <?}?>
</div>


<? 
	if($userAdmin==9) $sql = "select * from `TDATA` where VCOD_ like '%$vender%' and WGUBN_ like '%$item%' and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end' order by DATE_ asc, WNO_ asc limit $start_rec, $endlimit";
	else $sql = "select * from `TDATA` where VCOD_ like '%$vender%' and WGUBN_ like '%$item%' and WGUBN_ !='원재료' and WGUBN_ !='운임' and STATUS_ like '%$status%' and CARNO_ like '%$carNo%' and DATE_>='$date_start' and DATE_<='$date_end' order by DATE_ asc, WNO_ asc limit $start_rec, $endlimit";
	echo $sql;
	$result = $mysqli->query($sql);
	if($vender=='') $venderView = '전체';
	else $venderView = $vender;
	if($item=='') $itemView = '전체';
	else $itemView = $item;
	if($status=='') $statusView = '전체';
	else $statusView = $status;
	if($carNo=='') $carNoView = '전체';
	else $carNoView = $carNo;
		
	echo "<br /><div align=right> 검색조건 (거래처:".$venderView.") (구분:".$itemView.") (상태:".$statusView.") (차번:".$carNoView.") 기간:(".$date_start."~".$date_end.")</div>";

	$k=0;
	
	isset($start_year) ? $start_year=$start_year : $start_year=date('Y');
	isset($start_month) ? $start_month=$start_month : $start_month=date('m');
	isset($start_day) ? $start_day=$start_day : $start_day=date('d');
	isset($end_year) ? $end_year=$end_year : $end_year=date('Y');
	isset($end_month) ? $end_month=$end_month : $end_month=date('m');
	isset($end_day) ? $end_day=$end_day : $end_day=date('d');
	$postValue='?start_year='.$start_year.'&start_month='.$start_month.'&start_day='.$start_day.'&end_year='.$end_year.'&end_month='.$end_month.'&end_day='.$end_day.'&vender='.$vender.'&item='.$item.'&status='.$status.'&carNo='.$carNo.'&page='.$page;

	//echo $postValue;

?>
<form name='trans' method='post'>
<input type='hidden' name='postValue' value=<?=$postValue?>>

<table>
  <tr>
	<th>승인 <input type='checkbox' id="dayAll" /></th>
    <th>명세 <input type='checkbox' id="tenAll" /></th>
    <th>날짜</th>
    <th>순번</th>
    <th>거래처</th>
	<th>차번</th>
	<th>품목</th>
	<th>총중량</th>
	<th>공차중량</th>
	<th>감량중량</th>
	<th>감량률</th>
	<th>인수중량</th>
	<th>단가</th>
	<th>운임</th>
	<th>금액</th>
  </tr>


 <? while($row = $result->fetch_object()) {  ?>   
   <tr align="center"> 
   <td style="text-align:center"> <input type='hidden' name='idx[<?=$k?>]' value='<?=$row->IDX_?>'> 
		<?
	 	//if($row->WGUBN_!='원재료') echo $row->WGUBN_;
		if($row->STATUS_>3) echo "완료"; 
		else if($row->STATUS_==3) echo "승인 <input type='checkBox' id='confirmDaily' name='confirmD[".$k."]' value=1>" ;
		else echo "선택불가";
		 ?> 
   </td> 
    <td style="text-align:center;">
		<?
		if($row->WGUBN_!='원재료') echo $row->WGUBN_;
		else if($row->STATUS_==4) echo "승인 <input type='checkBox' id='confirmTenDay' name='confirmT[".$k."]' value=2>";
		else if($row->STATUS_==5) echo "완료";
		else echo "선택불가";
	?>	
	</td>
    <td> <?=$row->DATE_?> </td> 
	<td> <?=$row->WNO_?> </td>
	
	<!--<td title="거래코드(<?=$row->VCOD_?>) 품목코드(<?=$row->ICOD_?>) 총중시간(<?=substr($row->GDATETIME_, 11, 8)?>) 공차시간(<?=substr($row->CDATETIME_, 11, 8)?>) 이물질(<?=$row->MINUS1_?>kg <?=$row->PERC1_?>%) 수분(<?=$row->MINUS2_?>kg <?=$row->PERC2_?>%)"> <?=$row->VENDR_?> <span class="tooltiptext">Tooltip text </span> </td> -->

	<td> <div class='tooltip'> <?=$row->VENDR_?><span class="tooltiptext"> 
		거래코드 : <?=$row->VCOD_?> <br>
		품목코드 : <?=$row->ICOD_?> <br>
		총중시간 <?=substr($row->GDATETIME_, 11, 8)?> <br>
		공차시간 <?=substr($row->CDATETIME_, 11, 8)?> <br> 
		이물질 : <?=$row->MINUS1_?>kg <?=$row->PERC1_?>% <br>
		수분 : <?=$row->MINUS2_?>kg <?=$row->PERC2_?>%	
	</span> </div></td> 

	 <td  onclick="location.href='AdminMainEdit.php<?=$postValue?>&idx=<?=$row->IDX_?>&wGubn=<?=$row->WGUBN_?>'" style="cursor:hand"> <?=$row->CARNO_?> </td> 
	
	
	<td> <?=$row->ITEM_?> </td> 
	<td> <?echo number_format($row->GROSS_); $hapGross+=$row->GROSS_?> </td> 
	<td> <?=number_format($row->CAR_)?> </td> 
	<td> <?echo number_format($row->MINUS_); $hapMinus+=$row->MINUS_?> </td>
	<td> <?=number_format($row->PERC_)?> </td>
	<td> <font color='purple'><?echo(number_format($row->SNET_)); $hapSnet+=$row->SNET_?></font> </td> 
	<td> <font color='red'> <?=$row->UNIT_?></font> </td> 
	<td> <?=$row->FAREUNIT_?> </td>
	<td> <font color='purple'><? $totalUnit = $row->UNIT_+$row->FAREUNIT_;$totalHap = $totalUnit*($row->SNET_); echo number_format($totalHap); $hapHap+=$totalHap;?></font> </td>
	<!-- <td> 
		<? if($row->STATUS_==1) echo "검수대기"; 
           else if($row->STATUS_==2) echo "공차대기";
		   else if($row->STATUS_==3) echo "<font color='green'>승인대기</font>";
		   else if($row->STATUS_==4) echo "<font color='yellow''>명세대기</font>";
		   else if($row->STATUS_==5) echo "완료";
		   ?> </td> -->
   </tr> 
   <? $k++;} ?>
   <tr align="center">
    <td><button type='submit' class="btn" value='일승인' name='일승인' formaction='adminTrasDay.php'>선택승인</button></td> 
	<td><button type='submit' class="btnR" value='역발행' name='역발행' formaction='adminTrasTen.php'>선택발행</button></td> 
	<td><!--<button type='submit' class="btn" value='일승인' name='일승인' formaction='adminTrasDay.php'>승인취소</button>--></td> 
	<td><!--<button type='submit' class="btnR" value='역발행' name='역발행' formaction='adminTrasTen.php'>명세취소</button>--></td> 
	<td> </td> 
	<td> </td>
	<td> </td> 
	<td> <? if($hapGross!=0) echo number_format($hapGross); else echo "0";?> </td>
	<td> </td>
	<td> <? if($hapMinus!=0) echo number_format($hapMinus); else echo "0";?> </td>
	<td> <? if($hapGross!=0) echo number_format(floor(10000*$hapMinus/$hapGross)/100, 2, '.', ''); else echo "0.00";?> </td>
	<td> <?=number_format($hapSnet)?> </td> 
	<td> 평단가 : <? if($hapSnet!=0) echo number_format(floor($hapHap/$hapSnet*100)/100, 2, '.', ''); else echo "0";?> </td>
	<td> 금액합계 </td> 
	<td> <?=number_format($hapHap)?> </td>
	 
   </tr> 
  </table>
  </form>
  <center>
  <br />  
 <? 

	$page_num = 10;
	$total_block=ceil($total_page/$page_num);
	$block = ceil($page/$page_num);
	$first=($block-1)*$page_num+1;
	$end=($block*$page_num);
	if(isset($itemGubn)) if($itemGubn==0) $item=0;

	if($total_block==$block) $end=$total_page;
	if($block>1){
		echo "[<a href='AdminMain.php?page=1&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end&list_num=$list_num'> 처음 </a>]";
		echo "<a href='AdminMain.php?page=".($first-1)."&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end&list_num=$list_num'> ◀ </a>";
	}

	for($l=$first; $l<=$end; $l++){
		if($l==$page){
			echo "<b>  $l  </b>";
		}else{
			echo " [<a href='AdminMain.php?page=$l&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end&list_num=$list_num'>$l</a>] ";
		}
	}
    
	if($block<$total_block){
		echo "[<a href='AdminMain.php?page=".($end+1)."&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end&list_num=$list_num'>다음 $page_num 개 </a>]";
		echo "[<a href='AdminMain.php?page=$total_page&vender=$vender&item=$item&status=$status&carNo=$carNo&date_start=$date_start&date_end=$date_end&list_num=$list_num'>마지막</a>]";
	}
	?>

<br>
<br>
<form name="search" method=post action='AdminMain_test.php?page=1'>
검색기간 : 
<select name="start_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
<br><br>
<?
if($userAdmin==9) $sql = "select VCOD_, VENDR_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC"; 
else $sql = "select VCOD_, VENDR_ from `TVCOD` where WGUBN_!='원재료' order by VENDR_ ASC"; 
$result = $mysqli->query($sql);
?>
<select name="vender">
	<option value="0" selected> - 모든 거래처 - </option>;
	<? while($row=$result->fetch_object()){
		echo "<option value=".$row->VCOD_.">".$row->VENDR_."-".$row->VCOD_."</option>";
	}?>
</select>

<select name="item">
	<option value='0'> - 모든 품목 - </option>;
	<?if($userAdmin==9) echo "<option value='원재료' selected> 원재료 </option>" ?>
	<option value='부재료'> 부재료 </option>
	<option value='폐합성수지'> 폐합성수지 </option>
	<option value='소각폐기물'> 소각폐기물 </option>
	<option value='제품판매'> 제품판매 </option>	
	<option value='기타'> 기타 </option>	
</select> 

<select name="status">
	<option value="0" selected> - 모든 상태 - </option>;
	<option value="1"> 검수대기 </option>
	<option value="2"> 공차대기 </option>
	<option value="3"> 승인대기 </option>
	<option value="4"> 명세대기 </option>
	<option value="5"> 완료 </option>	
</select>

<select name="list_num">
	<option value="10" > 10개씩 </option>
	<option value="50" > 50개씩 </option>
	<option value="100" > 100개씩 </option>;
	<option value="150" selected> 한화면에 150개씩 </option>
	<option value="500"> 500개씩 </option>
	<option value="10000"> 10,000개씩</option>
</select>

<br> 
<br>
차량번호 : <input type=text size=15 maxlength=30 name="carNo" align="left">

<input type="submit" value="검색" >
</form>
<br><br>
</center>

</body>
</html>
<script>
$(document).ready(function(){
    //최상단 체크박스 클릭
    $("#dayAll").click(function(){		
        //클릭되었으면
        if($("#dayAll").prop("checked")){
            //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 true로 정의
            $("input[id=confirmDaily]").prop("checked",true);
            //클릭이 안되있으면
        }else{
            //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 false로 정의
            $("input[id=confirmDaily]").prop("checked",false);
        }    
	});	

	$("#tenAll").click(function(){		
        //클릭되었으면
        if($("#tenAll").prop("checked")){
            //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 true로 정의
            $("input[id=confirmTenDay]").prop("checked",true);
            //클릭이 안되있으면
        }else{
            //input태그의 name이 chk인 태그들을 찾아서 checked옵션을 false로 정의
            $("input[id=confirmTenDay]").prop("checked",false);
        }    
	});
})

</script>