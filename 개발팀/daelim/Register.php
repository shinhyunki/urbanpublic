<html> 
<head> 
<title> :: 대림제지 - 사전정보예약 :: </title>
<meta http-equiv="Content-Type" content="text/html" charset=utf-8> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add icon library -->
<link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

.input-container {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    width: 100%;
    margin-bottom: 15px;
}

.icon {
    padding: 10px;
    background: dodgerblue;
    color: white;
    min-width: 50px;
    text-align: center;
}

.input-field {
    width: 100%;
    padding: 10px;
    outline: none;
}

.input-field:focus {
    border: 2px solid dodgerblue;
}

/* Set a style for the submit button */
.btn {
    background-color: dodgerblue;
    color: white;
    padding: 10px 10px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
	position:relative;
	width:100%;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}

/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 15px;
    width: 15px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 5px;
    top: 2px;
    width: 4px;
    height: 8px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>
<script  src="script/jquery-latest.min.js"></script>

<script>

function goRegister() {
	 location.replace('Login.php')
}


function idCheck() {
	var frm  = document.registerContent;
	if(frm.userID.value == '') {
		alert("아이디는 공란일 수 없습니다.");
		frm.userID.focus();
		return;
	} else {
		 $.ajax({
			url: 'registerAjax.php',
			type: 'POST',
			data: {'userID':frm.userID.value},
			dataType: 'html',
			success: function(data){
				if(data<1) { 
					alert("사용가능한 ID입니다."); 
					frm.userPW.focus();
					idck = 1;
					return;
				} else {
					alert("사용이 불가능한 ID입니다. 다른 ID를 입력해주세요~!");
					frm.userID.focus();		
					return;
				}
			}
		});
	}
}


</script>

</head>

<body>

<form name="registerContent" method="post" action="register_ok.php" style="max-width:500px;margin:auto">

  <div> <center> <img src = "image/ci.jpg"> </center> </div> <br />
  <div class="input-container">
	<i class="fa fa-building icon"></i>
    <input class="input-field" type="text" placeholder="회사명" name="userCompany" >
  </div>
  <div class="input-container">
	<i class="fa fa-info icon"></i>
    <input class="input-field" type="text" placeholder="담당자 이름" name="userName">
  </div>
  <div class="input-container">
	<i class="fa fa-phone icon"></i>
    <input class="input-field" type="number" placeholder="전화번호" name="userPhone">
  </div>
  <div class="input-container">
	<i class="fa fa-envelope icon"></i>
    <input class="input-field" type="text" placeholder="이메일" name="userEmail">
  </div>
  <div class="input-container">
    <i class="fa fa-user icon"></i>
    <input class="input-field" type="text" placeholder="아이디:영문과 숫자 조합(한글불가)" name="userID" id="userID"> 
	  <button type="button" class="btn" onClick='idCheck();'> 아이디 중복확인 </button>
  </div>

  <div class="input-container">
    <i class="fa fa-key icon"></i>
    <input class="input-field" type="password" placeholder="비밀번호" name="userPW" id="userPW">
  </div>
  <div class="input-container">
    <i class="fa fa-key icon"></i>
    <input class="input-field" type="password" placeholder="비밀번호확인" name="userPW2">
  </div>
  <table border="0" width="100%">
  <tr><td><button type="button" class="btnR" style='background-color: silver;' onclick='goRegister();'> 취소 </button> </td>
  <td><button type="submit" class="btnR" > 사용승인 요청 </button></td></tr></table>
</form>


</body> 
</html> 
