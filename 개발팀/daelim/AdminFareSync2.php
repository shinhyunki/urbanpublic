<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<? include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$startDate=$_GET["startDate"];
$endDate=$_GET["endDate"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 

$startDate = $start_year.'-'.$start_month.'-01';
$endDate = $start_year.'-'.$start_month.'-31';


$sqlD = "SELECT DISTINCT `VCOD_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `FAREUNIT_`>0 and `WGUBN_`='원재료'"; 
$resultD = $mysqli->query($sqlD);
$k=0;
while($rowD=$resultD->fetch_object()){
	$vender[$k]['vcod'] = $rowD->VCOD_;
	$k++;	
}

$sql = "SELECT `VCOD_`, `VENDR_`, `FAREUNIT_`, `SNET_`  FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료'"; 
$result = $mysqli->query($sql);

while($row=$result->fetch_object()) { 
	for($i=0;$i<$k;$i++) {		
		if($vender[$i]['vcod'] == $row->VCOD_) {
			$vender[$i]['vender'] = $row->VENDR_;
			$vender[$i]['faretotal'] += $row->FAREUNIT_*$row->SNET_;
			$vender[$i]['snet'] += $row->SNET_;			
			//echo $vender[$i]['vcod'].':'.$vender[$i]['hap'].'<br>';
		}
	}
}



// 정렬
/*$arr_type = array(); 
$arr_file = array(); 
foreach ($vender as $idx=>$val) { 
  $arr_type[$idx] = $val['hap']; 
  $arr_file[$idx] = $val['snet']; 
} 
array_multisort($arr_type, SORT_DESC, $arr_file, SORT_DESC, $vender); 
*/
?>

<!DOCTYPE html>
<html lang="ko">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script src="script/jquery-latest.min.js"></script>
    <script type="text/javascript" src="script/jquery.battatech.excelexport.js"></script>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }
        table{
            width: 650px;
            text-align: center;
            border: 1px solid black;
			font-size:12px;
        }
		th {
			font-size:15px;
		}
		.btn {
		   background-color: hotpink;
		    color: white;
		    padding: 10px 10px;
		    border: none;
		    cursor: pointer;
		    width: 20%;
		    opacity: 0.9;
			margin : auto;			
		}
    </style>
	<script>
		function goURL(URL) {
			var url = URL+'.php';
			location.replace(url);
		}
	</script>
</head>

<body>
 <div id="wrap" align='center'>
  <form name='fare' action='adminFareSyncGo2.php' method='post'>
  <table id='tblExport' border=1>
   <tbody>
    <tr>
	 <th colspan='9' align='center'> <?=$startDate?> ~ <?=$endDate?> 운임 확인 </font> </td>
	</tr>
	<tr>
     <td> 거래처코드 </td>
	 <td> 거래처상호</td>
	 <td> 중량</td>
	 <td> 단가</td>
	 <td> 합산액</td>
    </tr>
	<?for($i=0;$i<$k;$i++) {?>
	<tr>
     <td><?=$vender[$i]['vcod']?> </td>
	 <td><?=$vender[$i]['vender']?></td>
	 <td><?=number_format($vender[$i]['snet'])?></td>
	 <td><?=number_format($vender[$i]['faretotal']/$vender[$i]['snet'])?></td>
	 <td><?=number_format(($vender[$i]['faretotal']))?></td>
    </tr>
	<input type='hidden' name='vender[]' value="<?=$vender[$i]['vcod']?>">
	<input type='hidden' name='vendername[]' value="<?=$vender[$i]['vender']?>">
	<input type='hidden' name='snet[]' value="<?=$vender[$i]['snet']?>">
	<input type='hidden' name='faretotal[]' value="<?=$vender[$i]['faretotal']?>">
	<? } ?>
   </tbody>
  </table>
  <input type='hidden' name='k' value="<?=$k?>">
  <input type='hidden' name='month' value="<?=$start_month?>">
  <br >
  <button type="submit" class="btn" style="background-color: red;"> 서버 전송 </button></a>
  </form>
 </div>
 <br /><br />	  
 <center>
  <a id="btnExport" href="#" download="<?='운임마감_'.$startDate.'_'.$endDate?>.xls"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a>
	<button type="button" class="btn" onclick="window.print()"> 인쇄 </button> 	
 </center>
 
<script type="text/javascript">
    $(document).ready(function () {
 
        function itoStr($num)
        {
            $num < 10 ? $num = '0'+$num : $num;
            return $num.toString();
        }
         
        var btn = $('#btnExport');
        var tbl = 'tblExport';
 
        btn.on('click', function () {
            var dt = new Date();
            var year =  itoStr( dt.getFullYear() );
            var month = itoStr( dt.getMonth() + 1 );
            var day =   itoStr( dt.getDate() );
            var hour =  itoStr( dt.getHours() );
            var mins =  itoStr( dt.getMinutes() );
 
            var postfix = year + month + day + "_" + hour + mins;
            var fileName = "Daelim_"+ postfix + ".xls";
 
            var uri = $("#"+tbl).excelexportjs({
                containerid: tbl
                , datatype: 'table'
                , returnUri: true
            });
 
            $(this).attr('download', fileName).attr('href', uri).attr('target', '_blank');
        });
    });
</script>
</body> 
</html>