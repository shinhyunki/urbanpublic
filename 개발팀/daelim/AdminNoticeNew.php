<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 
$idx = $_REQUEST["idx"];
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
  font-size:1em;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;	
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select, textarea {
	font-size:1em;

}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}

</style>
<script>
function goBack() {
	location.replace('AdminNotice.php?page=1');
}

function goDel() {
	var r = confirm("정말로 삭제하시겠습니까?");
	if(r==true) location.replace('adminNoticeEditDel.php?idx=<?=$idx?>');	
}
</script>

</head>
<body>


<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <a class="active" href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="#about"> 기타 </a>
</div>


<?  $mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
	$mysqli->query("SET NAMES 'utf8'");
	$sql = "select * from `TNOTICE` where IDX_ = '$idx'";
	$result = $mysqli->query($sql);
	$row = $result->fetch_object();
	$now = date('Y-m-d H:i:s');
	$nextWeek =  date('Y-m-d', strtotime('+7 days'))." 23:59:59";
?>
<h2> 공지사항 신규 등록 </h2>
<form  name="adminUserEdi" method="post" action="adminNoticeNew_ok.php">
<table>
  <tr>
    <th>벤더이름 : 코드</th>
	<td> 
		<select name='venderCode'>
			<option value='0' selected> 전체 공지 : 0 </option>
			<?
			$sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->VCOD_?>'> 
			 	<?=$rowVender->VENDR_." : ".$rowVender->VCOD_?> </option>
			<?}?>
		</select>	
	</td>
  </tr>
  <tr>
    <th>공지제목</th>
	<td> <input type='text' name='noticeTitle' size='55'> </td> 
  </tr>
  <tr>
	<th>공지내용</th>
	<td> <textarea name='noticeContents' rows='10' cols='50'  style="font-size:16px"> </textarea> </td> 
  </tr>
	<th>노출시작</th>
	<td> <input type='text' name='startTime' placeholder="<?=$now?>" value="<?=$now?>"> 현재시간이 기본값 </td> 
  </tr>
	<th>노출마감</th>
	<td> <input type='text' name='endTime' placeholder="<?=$nextWeek?>" value="<?=$nextWeek?>"> 일주일 후 자정이 기본값</td> 
  </tr>
	<th>1:노출 / 0:정지</th>
	<td> 
		<select name='statusFlag'>
			<option value='0'> 정지:0 </option>
			<option value='1' selected> 노출:1 </option>
		</select>
	</td>
  </tr>
  </table>
  <br>
  <br>
  <input type='hidden' name="idx" value="<?=$row->IDX_?>">
  <center>
 <button type="button" class="btnB" onClick="goBack()" > 취소 </button>
 <button type="submit" class="btn"> 등록 </button>
  </center>
 <br /><br />
 </form>
  
</body>
</html>
