<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}


table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {

	font-size: 100%;
    padding: 0.5em 0.5em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

td:hover {
	background-color:#f49d9d;
}

.td_code {
	text-align: right;
}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>
function goBack() {
	location.replace('AdminReserve.php?page=1');
}

function goDel(carNo) {
	var r = confirm("정말로 삭제하시겠습니까?");
	//if(r==true) alert(vcod);
	url = 'adminFareCarEditDel.php?carNo='+carNo;
	if(r==true) location.replace(url);	
}

function goDel2(idx) {
	var r = confirm("정말로 삭제하시겠습니까?");
	//if(r==true) alert(vcod);
	url = 'adminFareUnitEditDel.php?idx='+idx;
	if(r==true) location.replace(url);	
}

function goView(venderCode) {
	//if(r==true) alert(vcod);
	url = 'adminFareUnitDetailView.php?venderCode='+venderCode;
	location.replace(url);	
}
</script>
</head>
<body>

<?  $venderCode=$_GET["venderCode"];
	$sql = "select * from TFAREUNIT where VCOD_='$venderCode' order by `DATE_` desc";
	$result = $mysqli->query($sql);
?>

<h2> 업체별 운임 확인 </h2>
<table>
  <tr>
    <th> 거래처명 : 거래처코드</th>
	<th> 운임단가</th>
	<th> 적용일자</th>
	<th colspan='2'> 수정 삭제</th>
  </tr>
   <? while($row4 = $result->fetch_object()) {  ?>
  <tr>
  	<form method='post' action='adminFareUnitEdit_ok.php'>
	<input type='hidden' name='idx' value='<?=$row4->IDX_?>'>
  	<td> <?=$row4->VENDR_.":".$row4->VCOD_?> </td>
	<td><input type='number' name='fareUnit' value="<?=$row4->FAREUNIT_?>"> </td>
	<td><input type='text' name='fareDate' size='6' value="<?=$row4->DATE_?>"> </td>	
	<td><input type='submit' value='수정하기'> </td>
	<td><input type='button' value='삭제하기' onclick="goDel2('<?=$row4->IDX_?>')"> </td>			
	</form>
  </tr>
  <?}?>
  </table>
  <br> <center>  <input type='button' value='뒤로가기' onclick='history.back(-1)'> </center>

</body>
</html>
