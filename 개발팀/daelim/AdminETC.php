 <? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$now = date('Ymd');

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");


$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

if($userStatus<9) echo "<script language='javascript'> alert('사용권한이 없습니다.'); location.replace('Login.php'); </script>";

?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;
}

tr:hover {
	background-color:#f49d9d;
} 

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}
</style>
<script>
  function popupStatistics(){
	var myWindow = window.open("AdminStatistics.php", "거래명세서", "width=1700,height=1000");
}

function forceInput() {
	var url = 'AdminForceInput.php';
	var popupVender = window.open(url, "계량데이터 수동 입력", "width=1920,height=500");
}

function inspectList() {
    var start_year = document.getElementById('start_year_ins').value;
	var start_month = document.getElementById('start_month_ins').value;
	var start_day = document.getElementById('start_day_ins').value;
    var end_year = document.getElementById('end_year_ins').value;
	var end_month = document.getElementById('end_month_ins').value;
	var end_day = document.getElementById('end_day_ins').value;
	
	var url = 'AdminInspectView.php?start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day+'&end_year='+end_year+'&end_month='+end_month+'&end_day='+end_day;
	
    var myWindow = window.open(url, "검수내역", "width=1000,height=1000,scrollbars=1");
}

function popupMagam() {
    var start_year = document.getElementById('start_year').value;
	var start_month = document.getElementById('start_month').value;
	var start_day = document.getElementById('start_day').value;
    var end_year = document.getElementById('end_year').value;
	var end_month = document.getElementById('end_month').value;
	var end_day = document.getElementById('end_day').value;
	
	var url = 'AdminMagam.php?start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day+'&end_year='+end_year+'&end_month='+end_month+'&end_day='+end_day;
	
    var myWindow = window.open(url, "마감", "width=800,height=1000");
}

function popupMinusView() {
    var start_year = document.getElementById('start_year_minus').value;
	var start_month = document.getElementById('start_month_minus').value;
	var start_day = document.getElementById('start_day_minus').value;
    var end_year = document.getElementById('end_year_minus').value;
	var end_month = document.getElementById('end_month_minus').value;
	var end_day = document.getElementById('end_day_minus').value;
	
	var url = 'AdminMinusView.php?start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day+'&end_year='+end_year+'&end_month='+end_month+'&end_day='+end_day;
	
    var myWindow = window.open(url, "통계2", "width=800,height=1000,,scrollbars=1");
}

function popupFareSync() {
    var start_year = document.getElementById('start_yearfu').value;
	var start_month = document.getElementById('start_monthfu').value;
	var start_day = document.getElementById('start_dayfu').value;
    var end_year = document.getElementById('end_yearfu').value;
	var end_month = document.getElementById('end_monthfu').value;
	var end_day = document.getElementById('end_dayfu').value;
	
	var url = 'AdminFareSync.php?start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day+'&end_year='+end_year+'&end_month='+end_month+'&end_day='+end_day;
	
    var myWindow = window.open(url, "싱크1", "width=800,height=1000");
}

function popupFareSync2() {
    var start_year = document.getElementById('yearfu').value;
	var start_month = document.getElementById('monthfu').value;
	
	var url = 'AdminFareSync2.php?start_year='+start_year+'&start_month='+start_month;
	
    var myWindow = window.open(url, "싱크2", "width=800,height=1000");
}


function popupGrade() {
    var start_year = document.getElementById('start_year_g').value;
	var start_month = document.getElementById('start_month_g').value;
	var start_day = document.getElementById('start_day_g').value;
    var end_year = document.getElementById('end_year_g').value;
	var end_month = document.getElementById('end_month_g').value;
	var end_day = document.getElementById('end_day_g').value;
	
	var url = 'AdminGrade.php?start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day+'&end_year='+end_year+'&end_month='+end_month+'&end_day='+end_day;
	
    var myWindow = window.open(url, "등급1", "width=800,height=1000");
}

function popupGrade2() {
    var start_year = document.getElementById('start_year_g2').value;
	var start_month = document.getElementById('start_month_g2').value;
	var start_day = document.getElementById('start_day_g2').value;
    var end_year = document.getElementById('end_year_g2').value;
	var end_month = document.getElementById('end_month_g2').value;
	var end_day = document.getElementById('end_day_g2').value;
	
	var url = 'AdminGrade2.php?start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day+'&end_year='+end_year+'&end_month='+end_month+'&end_day='+end_day;
	
    var myWindow = window.open(url, "등급2", "width=1200,height=1000");
}

function popupFareList() {
    var start_year = document.getElementById('start_year_f').value;
	var start_month = document.getElementById('start_month_f').value;
	var start_day = document.getElementById('start_day_f').value;
    var end_year = document.getElementById('end_year_f').value;
	var end_month = document.getElementById('end_month_f').value;
	var end_day = document.getElementById('end_day_f').value;
	
	var url = 'AdminFareList.php?start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day+'&end_year='+end_year+'&end_month='+end_month+'&end_day='+end_day;
	
    var myWindow = window.open(url, "기간별 운임 조회", "width=800,height=1000");
}

function popupVender() {
	var url = 'AdminVcodEdit.php';
	var popupVender = window.open(url, "거래처 코드 관리", "width=1600,height=1000");
}

function popupItem() {
	var url = 'AdminIcodEdit.php';
	var popupVender = window.open(url, "품목 코드 관리", "width=1000,height=1000");
}

function popupCar() {
	var url = 'AdminCarEdit.php';
	var popupVender = window.open(url, "공차 중량 관리", "width=600,height=1000");
}

function popupFare() {
	var url = 'AdminFareEdit.php';
	var popupVender = window.open(url, "운임 관리", "width=800,height=1000");
}

function goUnitAdjust() {
 var r = confirm("정말로 변경하시겠습니까?");
 var start_year = document.getElementById('start_year2').value;
 var start_month = document.getElementById('start_month2').value;
 var start_day = document.getElementById('start_day2').value;
 var venderCode = document.getElementById('venderCode').value;
 var url = 'AdminMagamUnit.php?venderCode='+venderCode+'&start_year='+start_year+'&start_month='+start_month+'&start_day='+start_day;
 
 if(r==true) location.replace(url);   
}


function popupTrans4company() {
 var r = confirm("정말로 변경하시겠습니까?");
 var url = 'popupTrans4company.php'; 
 if(r==true) location.replace(url);   
}
</script>

</head>
<body>
<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a class="active" href="AdminETC.php"> 기타 </a>
</div>
<BR/><BR/>

<h3> 계량 현황 보기 : <input type='button' value='현황창 열기' onclick='popupStatistics();'> </h3>
<br>

<h3> 검수내역 보기 : <select name="start_year" id="start_year_ins">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_month_ins">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_day_ins">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_year_ins">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_month_ins">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_day_ins">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='button' value='검수 내역 보기' onclick='inspectList()'></h3><br>

 <h3> 거래처별 원재료 통계 : <select name="start_year" id="start_year_g">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_month_g">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_day_g">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_year_g">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_month_g">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_day_g">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='button' value='원재료 통계 확인' onclick='popupGrade()'></h3><br>


<h3> 품질/원료별 원재료 통계 : <select name="start_year" id="start_year_g2">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_month_g2">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_day_g2">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_year_g2">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_month_g2">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_day_g2">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='button' value='원재료 통계 확인' onclick='popupGrade2()'></h3><br>

 <h3> 거래처별 감량/인수/단가 통계 : 
<select name="start_year" id="start_year_minus">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_month_minus">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_day_minus">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_year_minus">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_month_minus">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_day_minus">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='button' value='원재료 통계 보기' onclick='popupMinusView()'></h3><br>

<h3> 계량데이터 수동 입력 : <input type='button' value='수동 입력' onclick='forceInput();'> </h3>
<br>

<h3> 거래처 추가, 삭제 : <input type='button' value='관리창 열기' onclick='popupVender();'> </h3>
<br>
<h3> 품목 추가, 삭제 : <input type='button' value='관리창 열기' onclick='popupItem();'> </h3>
<br>
<h3> 공차 추가, 삭제: <input type='button' value='관리창 열기' onclick='popupCar();'> </h3>
<br>
<h3> 운임 관리 : <input type='button' value='관리창 열기' onclick='popupFare();'> </h3>
<br>

<h3> 운임 확인 및 확정 : <select name="start_year" id="start_year_f">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_month_f">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_day_f">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_year_f">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_month_f">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_day_f">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='button' value='운임 리스트 보기' onclick='popupFareList()'></h3><br>

<h3> 운임 마감 : 
<select name="start_year" id="start_yearfu">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_monthfu">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_dayfu">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_yearfu">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_monthfu">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_dayfu">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='button' value='운임 마감 확인' onclick='popupFareSync()'></h3><br>


<? 
	$sql7 = "SELECT DATE_ FROM TDATA WHERE ICOD_='420' order by DATE_ desc limit 1;"; 
	$result7 = $mysqli->query($sql7);
	$lastDate =$result7->fetch_object()->DATE_;	
?>

<h3> 운임 서버 전송 : 
<select name="start_year" id="yearfu">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="monthfu">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>

분 <input type='button' value='운임 전송값 확인' onclick='popupFareSync2()'></h3>
* 매월초 전월분을 확인 후 전송해야 합니다. (마지막 전송일 : <?=$lastDate ?> )
<br><br>


<h3> 동주, 썬컴퍼니, 대림제지골판지사업, 동진판지 명세서 병합(<?=date('Y').'년 '.date('m')?>월 명세서) <input type='button' value='4개 업체 월마감' onclick='popupTrans4company()'></h3>


<br><br>


<h3> 원재료 마감 : 
<select name="start_year" id="start_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='button' value='원재료 마감 확인' onclick='popupMagam()'></h3><br>

<BR><FONT COLOR='RED' SIZE='5'><B>----- 단가수정 후 이전 데이터로 복원할 수 없습니다. ----- </B></FONT><BR><BR><br> 
<? 
	$sql = "SELECT DATE_ FROM TUNIT order by DATE_ desc;"; 
	$result = $mysqli->query($sql);
	$maxDate=$result->fetch_object()->DATE_;
?>

<form name='unitPrice' action='adminUnitPrice.php' method='post'>
<h3> 사내서버에서 계량서버로 단가 전송 : <?=$maxDate?>
 포함, 이후 적용 <input type='submit' value='단가 전송하기''> </h3>
 <input type='hidden' name='maxDate' value="<?=$maxDate?>">
</form>


<br>
<h3> 단가 일괄 수정 : 
<select name="start_year2" id="start_year2">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date('Y')==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month2" id="start_month2">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date('m')==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day2" id="start_day2">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date('d')==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select> 포함, 이후  


<select name='venderCode' id='venderCode'>
	<option value='-1' selected> --- 거래처 선택 --- </option>
	<option value='0'> --- 모든 거래처 --- </option>
	<?
	//$sqlVender = "select VENDR_,VCOD_ from `TVCOD`";
	$sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
	$resultVender = $mysqli->query($sqlVender);
	while($rowVender = $resultVender->fetch_object()) { ?>
		<option value='<?=$rowVender->VCOD_?>'> <?=$rowVender->VENDR_.':'.$rowVender->VCOD_?> </option>
	<?}?>
</select>	
 거래처에  
<input type='button' value='적용하기' onclick='goUnitAdjust();'></h3><br><br>




</body>
</html>