<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('image/searchicon.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

select{
  border-collapse: collapse; 
  border: 1px solid #ddd;
  font-size: 10px;
}

input[type=submit] {
  border-collapse: collapse; 
  border: 1px solid #ddd;
  font-size: 15px;
  font-weight:700;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 16px;
}

 #viewdata {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 13px;
}

#viewdata th, #viewdata td  {
  text-align: center;
  padding: 5px 2px;
}

#viewdata tr, td, th {
  border: 1px solid #ddd;
}

#myTable th, #myTable td  {
  text-align: center;
  padding: 15px 2px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
</head>




<body>

<?

$dateY=$_REQUEST["dateY"];
$dateM=$_REQUEST["dateM"];
$dateD=$_REQUEST["dateD"];

if($dateY=='') {
	$dateY = date("Y");
	$dateM = date("m");
	$dateD = date("d");
}

$dateSel = $dateY.'-'.$dateM.'-'.$dateD;


include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$abchook = 0;
$tkpan = 0;
$abchookminus = 0;
$tkpanminus = 0;
$total = 0;
$minus = 0;
$gamryul = 0;

$sqlview = "select ICOD_, SNET_, MINUS_ from TDATA where DATE_ = '$dateSel' and STATUS_>'2' AND `WGUBN_` = '원재료'"; 
$resultview = $mysqli->query($sqlview);
	
while($rowview=$resultview->fetch_object()) {
	if($rowview->ICOD_=='410' or $rowview->ICOD_=='411' or $rowview->ICOD_=='414') {
		$abchook += (int)$rowview->SNET_;
		$abchookminus += (int)$rowview->MINUS_;

	} else if($rowview->ICOD_=='412') {
		$tkpan += (int)$rowview->SNET_; 
		$tkpanminus += (int)$rowview->MINUS_; 
	}
	$minus += (int)$rowview->MINUS_; 
}	

$total = $abchook+$tkpan;
$gamryul = 100*$minus/($total+$minus)


?>

<h2><a href="InspectMain.php"> 검수차량 조회</a></h2>

<form action=inspectMain.php method='post'>
<table id='viewdata'>
 <tr>
   <th> 날짜 </th>
   <th> 압축 </th>
   <th> 특판 </th>
   <th> 합계 </th>
   <th> 압축감량<BR>특판감량 </th>
   <th> 감률계 </th>   
 </tr>
 <tr>
   <td rowspan='2'> <select name='dateY'>
			<option value='<?=$dateY-1?>'>  <?=$dateY-1?>년 </option>
			<option value='<?=$dateY?>' selected>  <?=$dateY?>년 </option>
			<option value='<?=$dateY+1?>'>  <?=$dateY+1?>년 </option>
			<option value='<?=$dateY+2?>'>  <?=$dateY+2?>년 </option>
			<option value='<?=$dateY+3?>'>  <?=$dateY+3?>년 </option>
		</select><br><br>
		<select name='dateM'>
			<option value='1' <?if($dateM==1) echo 'selected' ?>> 1월 </option>
			<option value='2' <?if($dateM==2) echo 'selected' ?>> 2월 </option>
			<option value='3' <?if($dateM==3) echo 'selected' ?>> 3월 </option>
			<option value='4' <?if($dateM==4) echo 'selected' ?>> 4월 </option>
			<option value='5' <?if($dateM==5) echo 'selected' ?>> 5월 </option>
			<option value='6' <?if($dateM==6) echo 'selected' ?>> 6월 </option>
			<option value='7' <?if($dateM==7) echo 'selected' ?>> 7월 </option>
			<option value='8' <?if($dateM==8) echo 'selected' ?>> 8월 </option>
			<option value='9' <?if($dateM==9) echo 'selected' ?>> 9월 </option>
			<option value='10' <?if($dateM==10) echo 'selected' ?>> 10월 </option>
			<option value='11' <?if($dateM==11) echo 'selected' ?>> 11월 </option>
			<option value='12' <?if($dateM==12) echo 'selected' ?>> 12월 </option>
		</select><br><br>
		<select name='dateD'>
			<?for($i=1; $i<=31; $i++) {
				echo "<option value='".$i."'";
				if($dateD==$i) echo " selected";
				echo "> ".$i."일 </option>";
			}
			?>
		</select> 
	</td>
   <td rowspan='2'><?=number_format($abchook)?> </td>
   <td rowspan='2'><?=number_format($tkpan)?></td>
   <td rowspan='2'><?=number_format($total)?></td>
   <td><?=number_format($abchookminus)?></td>
   <td rowspan='2'><?=number_format($gamryul, 2, '.', '');?></td>
 </tr>
 <tr>
	<td><?=number_format($tkpanminus)?></td>
 </tr>
</table>
<br> <center> <input type='submit' value='조회'> </center>
</form>

<br>





<input type="number" id="myInput" oninput="myFunction()" placeholder="차량번호 검색" title="Type in a name">


<?


$today = DATE("Y-m-d");

$sql = "select IDX_, DATE_, WNO_, CARNO_, VENDR_, ITEM_, STATUS_ from TDATA where DATE_ = '$today' and STATUS_='1' AND `WGUBN_` = '원재료' order by IDX_ asc"; 
$result = $mysqli->query($sql);

$sql2 = "select IDX_, DATE_, WNO_, CARNO_, VENDR_, ITEM_, STATUS_ from TDATA where  DATE_ = '$today' and STATUS_='2' AND `WGUBN_` = '원재료' order by IDX_ asc"; 
$result2 = $mysqli->query($sql2);

$sql3 = "select IDX_, DATE_, WNO_, CARNO_, VENDR_, ITEM_, STATUS_ from TDATA where DATE_ = '$today' and  STATUS_>'2' AND `WGUBN_` = '원재료' order by IDX_ asc"; 
$result3 = $mysqli->query($sql3);


/*while($row = $result->fetch_object()){ 
	if($row->STATUS_ == 2) echo "<tr><td><a href=InspectConfirm.php?dateValue=".$row->DATE_."&wno=".$row->WNO_."><font color='red'><b>".$row->WNO_.' : '.$row->VENDR_.' : '.$row->ITEM_.' : '.$row->CARNO_."</b></font></a></td></tr>";
	else echo "<tr><td><a href=InspectConfirm.php?dateValue=".$row->DATE_."&wno=".$row->WNO_.">".$row->WNO_.' : '.$row->VENDR_.' : '.$row->ITEM_.' : '.$row->CARNO_."</a></td></tr>";
}*/
?>


<table id='myTable'>
  <tr class="header">
    <th> 순번 </th>
	<th> 거래처 </th>
	<th> 품목 </th>
	<th> 차량번호 </th>
  </tr>
<? while($row = $result->fetch_object()){ ?>
  <tr onclick="location.href='InspectConfirm.php?idx=<?=$row->IDX_?>'" style="cursor:hand"> 
    <td><?=$row->WNO_?></td>
	<td><?=$row->VENDR_?></td>
	<td><?=$row->ITEM_?></td>
	<td><?if($row->STATUS_==2) echo "<font color='red'><b>".$row->CARNO_."</b></font>"; else echo "<font color='black'><b>".$row->CARNO_."</b></font>"?></td>
  </tr>
<? } ?>
<? while($row2 = $result2->fetch_object()){ ?>
  <tr onclick="location.href='InspectConfirm.php?idx=<?=$row2->IDX_?>'" style="cursor:hand"> 
    <td><?=$row2->WNO_?></td>
	<td><?=$row2->VENDR_?></td>
	<td><?=$row2->ITEM_?></td>
	<td><?if($row2->STATUS_==2) echo "<font color='red'><b>".$row2->CARNO_."</b></font>"; else echo "<font color='black'><b>".$row2->CARNO_."</b></font>"?></td>
  </tr>
<? } ?>

<? while($row3 = $result3->fetch_object()){ ?>
  <tr onclick="location.href='InspectConfirm.php?idx=<?=$row3->IDX_?>'" style="cursor:hand"> 
    <td><?=$row3->WNO_?></td>
	<td><?=$row3->VENDR_?></td>
	<td><?=$row3->ITEM_?></td>
	<td><?if($row3->STATUS_>2) echo "<font color='blue'><b>".$row3->CARNO_."</b></font>"; else echo "<font color='black'><b>".$row3->CARNO_."</b></font>"?></td>
  </tr>
<? } ?>
</table>

<script>
/*function myFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}*/
function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>

</body>
</html>
