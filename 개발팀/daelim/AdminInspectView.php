<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
$userID = $_SESSION["join_id"];
?>

<?
include 'db_access.php'; 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}


table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {

	font-size: 100%;
    padding: 0.5em 0.5em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

td:hover {
	background-color:#f49d9d;
}

.td_code {
	text-align: right;
}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

</head>
<body>

<? 
	$start_year = $_REQUEST["start_year"]; 
	$start_month = $_REQUEST["start_month"]; 
	$start_day = $_REQUEST["start_day"]; 

	$end_year = $_REQUEST["end_year"]; 
	$end_month = $_REQUEST["end_month"]; 
	$end_day = $_REQUEST["end_day"]; 

	$date_start = $start_year."-".$start_month."-".$start_day;
	$date_end = $end_year."-".$end_month."-".$end_day;

	$today = DATE("Y-m-d");
	$sql = "select * from `TDATA` where DATE_ >= '$date_start' and DATE_ <= '$date_end' and WGUBN_='원재료' and STATUS_ > 1";
	$result = $mysqli->query($sql);
?>

<h2> <?=$date_start.' ~ '.$date_end.' 검수 내역'?> </h2>
<table>
  <tr>
    <th>거래처</th>
	<th>차번</th>
    <th>품명</th>
    <th>총중량</th>
    <th>공차중량</th>
    <th>실중량</th>
	<th>감량중량</th>
	<th>감량율</th>
	<th>등급</th>
	<th>구분(공장,생활)</th>
  </tr>
  <? while($row = $result->fetch_object()) { ?>
  <tr>
    <td><?=$row->VENDR_?></td>
	<td><?=$row->CARNO_?></td>
	<td><?=$row->ITEM_.'('.$row->ICOD_.')'?></td>
	<td><?=number_format($row->GROSS_)?></td>
	<td><?=number_format($row->CAR_)?></td>
	<td><?=number_format($row->NET_)?></td>
	<td><?=number_format($row->MINUS_)?></td>
	<td><?=$row->PERC_?></td>
	<td><?=$row->GRADE_?></td>
	<td><?=$row->ORIENT_?></td>
  </tr>  
  <? } ?>    
  </table>
</body>
</html>
