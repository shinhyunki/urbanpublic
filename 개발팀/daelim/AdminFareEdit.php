<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}


table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {

	font-size: 100%;
    padding: 0.5em 0.5em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

td:hover {
	background-color:#f49d9d;
}

.td_code {
	text-align: right;
}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>
function goBack() {
	location.replace('AdminReserve.php?page=1');
}

function goDel(carNo) {
	var r = confirm("정말로 삭제하시겠습니까?");
	//if(r==true) alert(vcod);
	url = 'adminFareCarEditDel.php?carNo='+carNo;
	if(r==true) location.replace(url);	
}

function goDel2(idx) {
	var r = confirm("정말로 삭제하시겠습니까?");
	//if(r==true) alert(vcod);
	url = 'adminFareUnitEditDel.php?idx='+idx;
	if(r==true) location.replace(url);	
}

function goView(venderCode) {
	//if(r==true) alert(vcod);
	url = 'adminFareUnitDetailView.php?venderCode='+venderCode;
	location.href=url;	
}
</script>
</head>
<body>

<? 
	$sql = "select `CARNO_` from TFARECAR order by `CARNO_` asc";
	$result = $mysqli->query($sql);

	$sql2 = "select distinct `VCOD_` from TFAREUNIT order by `VENDR_` asc";	
	$result2 = $mysqli->query($sql2);

	//$sql3 = "select `VCOD_`, `VENDR_` from TVCOD order by `VENDR_` asc";
	$sql3 = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
	$result3 = $mysqli->query($sql3);
?>

<h2> 운임차량 관리 </h2>
<table>
  <tr>
    <th>차량번호</th>
	<th>추가 삭제</th>
  </tr>
  <tr>
    <form method='post' action='adminFareCarEdit_insert.php'>
	<td><input type='text' name='carNo' size='20'> </td>
	<td><input type='submit' value='추가하기''> </td>		
	</form>
  </tr>
  <? while($row = $result->fetch_object()) { ?>
  <tr>
    <span style='text-align: right;'> 
	<td> <?=$row->CARNO_?> </td>
	<td><input type='button' value='삭제하기' onclick="goDel('<?=$row->CARNO_?>')"> </td>			
	</form>
  </tr>
  <?}?>
  </table>

<h2> 업체별 운임 관리 </h2>
<table>
  <tr>
    <th> 거래처명 : 거래처코드</th>
	<th> 최신운임단가</th>
	<th> 적용일자</th>
	<th colspan='3'> 추가 수정 삭제</th>
  </tr>
  <tr>
    <form method='post' action='adminFareUnitEdit_insert.php'>
	<td>
	  <select name='venderCode'>
	   <? while($row3 = $result3->fetch_object()) { ?>
	    <option value="<?=$row3->VCOD_?>"> <?=$row3->VENDR_.":".$row3->VCOD_?></option>
		<?}?>
	  </select>
       	
	</td>
	<td> <input type='number' name='fareUnit'> </td>
	<td> <input type='text' name='fareDate' size='6' value="<?=date('Y-m-d')?>"> </td>
	<td colspan='3'><input type='submit' value='추가하기''> </td>		
	</form>
  </tr>
  <? while($row2 = $result2->fetch_object()) { 
	  $sql4 = "select * from TFAREUNIT where VCOD_ = '$row2->VCOD_' order by `DATE_` desc";
      $result4 = $mysqli->query($sql4);
	  $row4 = $result4->fetch_object();
  ?>
  <tr>
  	<form method='post' action='adminFareUnitEdit_ok.php'>
	<input type='hidden' name='idx' value='<?=$row4->IDX_?>'>
  	<td> <?=$row4->VENDR_.":".$row4->VCOD_?> </td>
	<td><input type='number' name='fareUnit' value="<?=$row4->FAREUNIT_?>"> </td>
	<td><input type='text' name='fareDate' size='6' value="<?=$row4->DATE_?>"> </td>
	<td><input type='button' value='모든단가' onclick="goView('<?=$row4->VCOD_?>')"> </td>
	<td><input type='submit' value='수정'> </td>
	<td><input type='button' value='삭제' onclick="goDel2('<?=$row4->IDX_?>')"> </td>			
	</form>
  </tr>
  <?}?>
  </table>

<h2> 운임 일괄 변경 </h2>
<form method='post' action='adminFareUnitApplyAll.php'>
<select name="start_year" id="start_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date(Y)==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="start_month" id="start_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date(m)==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="start_day" id="start_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date(d)==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 부터 
<select name="end_year" id="end_year">
	<?for($x=2018; $x<2030; $x++) { ?> 
	<option value="<?=$x?>" <?if(date(Y)==$x) echo "selected";?>> <?=$x?>년 </option>
	<?}?>
</select>
<select name="end_month" id="end_month">
	<?for($y=1; $y<13; $y++) { ?> 
	<option value="<?=$y?>" <?if(date(m)==$y) echo "selected";?>> <?=$y?>월 </option>
	<?}?>
</select>
<select name="end_day" id="end_day">
	<?for($z=1; $z<32; $z++) { ?> 
	<option value="<?=$z?>" <?if(date(d)==$z) echo "selected";?>> <?=$z?>일 </option>
	<?}?>
</select>
 까지 
 <input type='submit' value='운임 일괄 변경'></form></h3><br>
</body>
</html>
