<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<? include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$startDate=$_GET["startDate"];
$endDate=$_GET["endDate"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 
$start_day = $_REQUEST["start_day"]; 
$end_year = $_REQUEST["end_year"]; 
$end_month = $_REQUEST["end_month"]; 
$end_day = $_REQUEST["end_day"]; 

$startDate = $start_year.'-'.$start_month.'-'.$start_day;
$endDate = $end_year.'-'.$end_month.'-'.$end_day;

//$startDate = '2018-01-01';
//$endDate = '2019-12-31';
//$venderCode = '12345';
//$hap = $venderCode.'_HAP';

$sqlV = "SELECT `VCOD_`, `VENDR_`, `NAME_` FROM `TVCOD` where `WGUBN_`='원재료' order by VENDR_ asc"; 
$resultV = $mysqli->query($sqlV);
$j=0;
while($rowV=$resultV->fetch_object()){
	$venderInfo[$j]['vcod'] = $rowV->VCOD_;
	$venderInfo[$j]['vender'] = $rowV->VENDR_;
	$venderInfo[$j]['name'] = $rowV->NAME_;
	$j++;
	//echo $rowV->VENDR_.'<br>';
}

$sqlD = "SELECT DISTINCT `VCOD_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료'"; 
$resultD = $mysqli->query($sqlD);
$k=0;
while($rowD=$resultD->fetch_object()){
	$vender[$k]['vcod'] = $rowD->VCOD_;
	$vender[$k]['hap'] = 0;
	for($l=0;$l<$j;$l++) {
		if($vender[$k]['vcod'] == $venderInfo[$l]['vcod']) {
			$vender[$k]['vender'] = $venderInfo[$l]['vender'];
			$vender[$k]['name'] = $venderInfo[$l]['name'];
			//$vender[$k]['carsum']++;
		}
	}
	//echo $k.':'.$vender[$k]['vcod'].'<br>';
	$k++;	
}

$sql = "SELECT `VCOD_`, `SNET_`, `HAP_`, `MINUS_`, `UNIT_`, `FAREUNIT_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료'"; 
$result = $mysqli->query($sql);

$totalSnet = 0;
$totalMinus = 0;
$totalHap = 0;
$totalCar = 0;

while($row=$result->fetch_object()) { 
	for($i=0;$i<$k;$i++) {		
		if($vender[$i]['vcod'] == $row->VCOD_) {
			$vender[$i]['hap'] += $row->HAP_;
			$vender[$i]['snet'] += $row->SNET_;			
			$vender[$i]['minus'] += $row->MINUS_;
			$vender[$i]['fareunit'] += $row->FAREUNIT_;
			$vender[$i]['totalhap'] += $row->SNET_*($row->FAREUNIT_+$row->UNIT_);
			$vender[$i]['carsum']++;
			$vender[$i]['avgunit'] = $row->FAREUNIT_+$row->UNIT_;
			//echo $vender[$i]['vcod'].':'.$vender[$i]['hap'].'<br>';
			$totalSnet += $row->SNET_;
			$totalMinus += $row->MINUS_;
			$totalHap += $row->SNET_*($row->FAREUNIT_+$row->UNIT_);
			$totalCar++;

		}
	}
}

for($a=0; $a<$j; $a++) {
	for($b=0; $b<$k; $b++) {
		if($vender[$b]['vcod'] == $venderInfo[$a]['vcod']) $venderInfo[$a]['vcodcheck'] = 1;
	}
}

$noInNum=0;
for($a=0; $a<$j; $a++) {
	if($venderInfo[$a]['vcodcheck']!=1 and $venderInfo[$a]['vcod']!=1) {
		$noInVender[$noInNum] = $venderInfo[$a]['vender'];
		$noInNum++;
	}
}

// 정렬
$arr_type = array(); 
$arr_file = array(); 
foreach ($vender as $idx=>$val) { 
  $arr_type[$idx] = $val['avgunit']; 
  $arr_file[$idx] = $val['snet']; 
} 
array_multisort($arr_type, SORT_ASC, $arr_file, SORT_ASC, $vender); 

?>

<!DOCTYPE html>
<html lang="ko">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script src="script/jquery-latest.min.js"></script>
    <script type="text/javascript" src="script/jquery.battatech.excelexport.js"></script>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }
        table{
            width: 650px;
            text-align: center;
            border: 1px solid black;
			font-size:12px;
        }
		th {
			font-size:15px;
		}
		.btn {
		   background-color: hotpink;
		    color: white;
		    padding: 10px 10px;
		    border: none;
		    cursor: pointer;
		    width: 20%;
		    opacity: 0.9;
			margin : auto;			
		}
    </style>
	<script>
		function goURL(URL) {
			var url = URL+'.php';
			location.replace(url);
		}
	</script>
</head>

<body>
 <div id="wrap" align='center'>
  <table id='tblExport' border=1>
   <tbody>
    <tr>
	 <th colspan='7' align='center'> <?=$startDate?> ~ <?=$endDate?> 거래처별 감량 인수 단가 조회 </th>
	</tr>
	<tr>
	 <td bgcolor='yellow'>합계 </td>
	 <td bgcolor='yellow'> <?=number_format($totalCar)?></td>
	 <td bgcolor='yellow'> <?=number_format($totalSnet+$totalMinus)?></td>
	 <td bgcolor='yellow'> <?=number_format($totalMinus)?></td>
	 <td bgcolor='yellow'> <?=number_format((floor(10000*$totalMinus/($totalSnet+$totalMinus))/100), 2, '.', '')?></td>
	 <td bgcolor='yellow'> <?=number_format($totalSnet)?></td>
	 <td bgcolor='yellow'> <?=number_format((floor(100*$totalHap/$totalSnet)/100), 2, '.', '')?></td>
    </tr>
	<tr>
	 <td bgcolor='skyblue'> 거래처</td>
	 <td bgcolor='skyblue'> 차량수</td>
	 <td bgcolor='skyblue'> 총중량</td>
	 <td bgcolor='skyblue'> 감량</td>
	 <td bgcolor='skyblue'> 감량률</td>
	 <td bgcolor='skyblue'> 인수량</td>
	 <td bgcolor='skyblue'> 단가(운임포함)</td>
    </tr>
	<?for($i=0;$i<$k;$i++) {?>
	<tr>
	 <td> <?=$vender[$i]['vender']?></td>
	 <td> <?=$vender[$i]['carsum']?></td>
	 <td> <?=number_format($vender[$i]['snet']+$vender[$i]['minus'])?></td>
	 <td> <?=number_format($vender[$i]['minus'])?></td>
	 <td> <?=number_format((floor(10000*$vender[$i]['minus']/($vender[$i]['snet']+$vender[$i]['minus']))/100), 2, '.', '')?></td>
	 <td> <?=number_format($vender[$i]['snet'])?></td>
	 <td> <?=number_format((floor(100*$vender[$i]['totalhap']/$vender[$i]['snet'])/100), 2, '.', '')?></td>	 
    </tr>	
	<? } ?>
	<tr >
	<td colspan='7' align='center' bgcolor='hotpink'> 안들어온 업체</td>
	</tr>
	<? $niN = round($noInNum/4);
	for($i=0; $i<$niN; $i++) { ?>
	<tr >
	<td colspan='1' align='center' width=162><?=$noInVender[4*$i]?></td>
	<td colspan='2' align='center' width=162><?=$noInVender[4*$i+1]?></td>
	<td colspan='2' align='center' width=162><?=$noInVender[4*$i+2]?></td>
	<td colspan='2' align='center' width=162><?=$noInVender[4*$i+3]?></td>
	</tr>
	<?}?>
   </tbody>
  </table>     
 </div>
 <br /><br />	  
 <center>
  <a id="btnExport" href="#" download="<?='원재료마감_'.$startDate.'_'.$endDate?>.xls"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a>
	<button type="button" class="btn" onclick="window.print()"> 인쇄 </button> 
	
 </center>
 
<script type="text/javascript">
    $(document).ready(function () {
 
        function itoStr($num)
        {
            $num < 10 ? $num = '0'+$num : $num;
            return $num.toString();
        }
         
        var btn = $('#btnExport');
        var tbl = 'tblExport';
 
        btn.on('click', function () {
            var dt = new Date();
            var year =  itoStr( dt.getFullYear() );
            var month = itoStr( dt.getMonth() + 1 );
            var day =   itoStr( dt.getDate() );
            var hour =  itoStr( dt.getHours() );
            var mins =  itoStr( dt.getMinutes() );
 
            var postfix = year + month + day + "_" + hour + mins;
            var fileName = "Daelim_"+ postfix + ".xls";
 
            var uri = $("#"+tbl).excelexportjs({
                containerid: tbl
                , datatype: 'table'
                , returnUri: true
            });
 
            $(this).attr('download', fileName).attr('href', uri).attr('target', '_blank');
        });
    });
</script>
</body> 
</html>
