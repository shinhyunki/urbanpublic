<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 
$idx = $_REQUEST["idx"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 
$start_day = $_REQUEST["start_day"]; 
$end_year = $_REQUEST["end_year"]; 
$end_month = $_REQUEST["end_month"]; 
$end_day = $_REQUEST["end_day"]; 
$vender = $_REQUEST["vender"]; 
$item = $_REQUEST["item"]; 
$status = $_REQUEST["status"]; 
$carNo = $_REQUEST["carNo"]; 
$page = $_REQUEST["page"];
$wGubn = $_REQUEST["wGubn"];

//echo $wGubn;

$postValue='?start_year='.$start_year.'&start_month='.$start_month.'&start_day='.$start_day.'&end_year='.$end_year.'&end_month='.$end_month.'&end_day='.$end_day.'&vender='.$vender.'&item='.$item.'&status='.$status.'&carNo='.$carNo.'&page='.$page;

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");


$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

//echo $userStatus;

?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 95%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>
function goBack() {
	history.go(-1);;
}

function goDel() {
	var sa = document.getElementById('sayou').value;
	locaValue = 'adminMainEditDel.php<?=$postValue?>&idx=<?=$idx?>&sayou='+sa;	
	var r = confirm("정말로 삭제하시겠습니까?");
	if(r==true) location.replace(locaValue);	
}
</script>
</head>
<body>

<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a class="active" href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="AdminETC.php"> 기타 </a>
</div>

<? 
	$sql = "select * from `TDATA` where IDX_ = '$idx'";
	$result = $mysqli->query($sql);
	$row = $result->fetch_object();
	

?>
<form  name="adminMainEdit" method="post" action="adminMainEdit_ok.php">
<input type='hidden' name='postValue' value=<?=$postValue?>>
<table>
  <tr>
    <th>날짜</th>
    <th>순번</th>
	<th>차번</th>
	<th>거래처</th>
	<th>품목</th>
	<th>총중량</th>
	<th>공차중량</th>
	<th>실중량</th>
	<th>감률1</th>
	<th>감률2</th>
	<th>감량</th>
	<th>인수량</th>
	<th>단가</th>
	<th>운임</th>
	<th>금액</th>
	<th>상태</th>
  </tr>

   <tr> 
    <td> <?=$row->DATE_?> </td> 
	<td> <?=$row->WNO_?> </td>
	<td> <input type='text' name='carNo' size='8' value='<?=$row->CARNO_?>'> </td> 
	<td> 
		<select name='venderCode'>
			<?
			if($userStatus==9) $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
			else $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_!='원재료' order by VENDR_ ASC";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->VCOD_?>%<?=$rowVender->VENDR_?>' <?if($rowVender->VCOD_==$row->VCOD_) echo 'selected'?>> <?=$rowVender->VENDR_?> : <?=$rowVender->VCOD_?></option>
			<?}?>
		</select>
	</td> 
	<td> 
		<select name='itemCode'>
			<?
			$sqlVender = "select ITEM_, ICOD_ from `TICOD` WHERE WGUBN_='$wGubn'";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->ICOD_?>%<?=$rowVender->ITEM_?>' <?if($rowVender->ICOD_==$row->ICOD_) echo 'selected'?>> <?=$rowVender->ITEM_?> : <?=$rowVender->ICOD_?></option>
			<?}?>
		</select>
	</td>
	<td> <?=number_format($row->GROSS_)?> </td> 
	<td> <?=number_format($row->CAR_)?> </td> 
	<td> <input type='text' name='net' id='net' size='3' value='<?=$row->NET_?>' readonly> </td> 
	<td> <input type='text' name='perc1' id='perc1' size='1' value='<?=$row->PERC1_?>'> </td>
	<td> <input type='text' name='perc2' id='perc2' size='1' value='<?=$row->PERC2_?>'> </td>
	<td> <input type='text' name='minus' id='minus' size='3' value='<?=$row->MINUS_?>' readonly></td>
	<td> <input type='text' name='snet' id='snet' size='3' value='<?=$row->SNET_?>' readonly> </td> 
	<td> <input type='text' name='unit' id='unit' size='3' value='<?=$row->UNIT_?>'> </td>
		<td> <input type='text' name='fareunit' id='fareunit' size='3' value='<?=$row->FAREUNIT_?>'> </td>
		<? $totalUnit = $row->UNIT_;$totalHap = $totalUnit*($row->SNET_); ?>
	<td> <input type='text' name='hap' id='hap' size='5' value='<?=$totalHap?>' readonly> </td>
	<td> 
		<select name='status'>
				<option value='1' <?if($row->STATUS_== 1) echo 'selected'?>> 검수대기 </option>
				<option value='2' <?if($row->STATUS_== 2) echo 'selected'?>> 2차대기 </option>
				<option value='3' <?if($row->STATUS_== 3) echo 'selected'?>> 승인대기 </option>
				<option value='4' <?if($row->STATUS_== 4) echo 'selected'?>> 명세대기 </option>
				<option value='5' <?if($row->STATUS_== 5) echo 'selected'?>> 완료 </option>
		</select>
	</td> 
   </tr>
   <tr>
    <td colspan='3'> 수정 및 삭제 사유<br> * 수정 삭제 사유는 로그파일에 기록됩니다. </td>
	<td colspan='13'> <textarea name='sayou' id='sayou' style="width:100%;border:1;overflow:visible;text-overflow:ellipsis;" rows='5'></textarea> </td>
   </tr>
  </table>
  <input type='hidden' name="idx" value="<?=$row->IDX_?>">
  <br ><br >
  <center>
 <button type="button" class="btnB" onClick="goBack()" > 취소 </button>
 <button type="button" class="btnR" onClick="goDel()" > 삭제 </button>
 <button type="submit" class="btn"> 수정 </button>
  </center>
 <br /><br />
 </form>
</body>
</html>


<script>
document.getElementById("perc1").addEventListener("keyup", myFunction);
document.getElementById("perc2").addEventListener("keyup", myFunction);
document.getElementById("unit").addEventListener("keyup", myFunction2);
document.getElementById("fareunit").addEventListener("keyup", myFunction2);

function myFunction() {	
	var net = document.getElementById("net").value;
	var perc1 = document.getElementById("perc1").value;
	var perc2 = document.getElementById("perc2").value;
	net = net*1;perc1 = perc1*1;perc2 = perc2*1;
	var minus = Math.floor(net*(perc1+perc2)*0.001)*10;
	var snet = net - minus;
	var unit = document.getElementById("unit").value;
    document.getElementById("minus").value = minus;
	document.getElementById("snet").value = snet;
	var hap = unit*snet;		
	document.getElementById("hap").value = hap;
}

function myFunction2() {	
	var unit = document.getElementById("unit").value;
	var fareunit = document.getElementById("fareunit").value;
	var snet = document.getElementById("snet").value;
	unit = unit*1;snet = snet*1;fareunit = fareunit*1
	var hap = (unit)*snet;	
    document.getElementById("hap").value = hap;
}
</script>