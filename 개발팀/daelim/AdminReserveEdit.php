<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

if($userStatus<3) echo "<script language='javascript'> alert('사용권한이 없습니다.'); location.replace('Login.php'); </script>";
else if($userStatus<9) $userAdmin = 1;
else $userAdmin = 9;

$idx = $_REQUEST["idx"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 
$start_day = $_REQUEST["start_day"]; 
$end_year = $_REQUEST["end_year"]; 
$end_month = $_REQUEST["end_month"]; 
$end_day = $_REQUEST["end_day"]; 
$vender = $_REQUEST["vender"]; 
$item = $_REQUEST["item"]; 
$status = $_REQUEST["status"]; 
$carNo = $_REQUEST["carNo"]; 
$page = $_REQUEST["page"];

$postValue='?start_year='.$start_year.'&start_month='.$start_month.'&start_day='.$start_day.'&end_year='.$end_year.'&end_month='.$end_month.'&end_day='.$end_day.'&vender='.$vender.'&item='.$item.'&status='.$status.'&carNo='.$carNo.'&page='.$page;

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;
?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;
	border: 0.1em solid #eee;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btnB {
    background-color: #929292;
    color: white;
    padding: 15px 10px;
    border: none;
    cursor: pointer;
    width: 10%;
    opacity: 0.9;
	font-size:16px;
	margin-left: auto;
    margin-right: 0;
	display: inline-block;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>
function goBack() {
	location.replace('AdminReserve.php?page=1');
}

function goDel() {
	var r = confirm("정말로 삭제하시겠습니까?");
	if(r==true) location.replace('adminReserveEditDel.php<?=$postValue?>&idx=<?=$idx?>');	
}
</script>
</head>
<body>

<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a class="active" href="AdminReserve.php?page=1"> 예약현황 </a>
  <a href="AdminUser.php?page=1"> 사용자관리 </a>
  <?if($userAdmin==9) { ?> 
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="#"> 기타 </a>
  <? } else { ?>
  <a href="AdminETC2.php"> 기타 </a>
  <?}?>
</div>

<? 
	$sql = "select * from `TRESERVE` where IDX_ = '$idx'";
	$result = $mysqli->query($sql);
	$row = $result->fetch_object();
	

?>
<form  name="adminReserveEdit" method="post" action="adminReserveEdit_ok.php">
<input type='hidden' name='postValue' value=<?=$postValue?>>
<table>
  <tr>
    <th>예약날짜</th>
    <th>차량번호</th>
    <th>벤더</th>
	<th>품목</th>
	<th>예상중량</th>
	<th>입출고</th>
	<th>상태</th>
	<th>기사명</th>
  </tr>

   <tr> 
    <td> <input type='text' name='reserveDate' size='8' value='<?=$row->DATE_?>'> </td> 
	<td> <input type='text' name='carNo' size='8' value='<?=$row->CARNO_?>'> </td> 
	<td> 
		<select name='venderCode'>
			<?
			if($userStatus==9) $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_='원재료' order by VENDR_ ASC";
			else $sqlVender = "select VENDR_, VCOD_ from `TVCOD` where WGUBN_!='원재료' order by VENDR_ ASC";	
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->VCOD_?>%<?=$rowVender->VENDR_?>' <?if($rowVender->VCOD_==$row->VCOD_) echo 'selected'?>> <?=$rowVender->VENDR_?> : <?=$rowVender->VCOD_?></option>
			<?}?>
		</select>
	</td> 
	<td> 
		<select name='itemCode'>
			<?
			$sqlVender = "select ITEM_, ICOD_ from `TICOD`";
			$resultVender = $mysqli->query($sqlVender);
			while($rowVender = $resultVender->fetch_object()) { ?>
				<option value='<?=$rowVender->ICOD_?>%<?=$rowVender->ITEM_?>' <?if($rowVender->ICOD_==$row->ICOD_) echo 'selected'?>> <?=$rowVender->ITEM_?> : <?=$rowVender->ICOD_?></option>
			<?}?>
		</select>
	</td>
	<td> <input type='text' name='expectWeight' size='8' value='<?=$row->EXPECTWEIGHT_?>'> </td> 
	<td> 
		<select name='inOut'>
				<option value='1' <?if($row->INOUT_== '1') echo 'selected'?>> 입고 </option>
				<option value='2' <?if($row->INOUT_== '2') echo 'selected'?>> 츨고 </option>
		</select>
	</td> 
	<td> 
		<select name='status'>
				<option value='0' <?if($row->STATUS_== '0') echo 'selected'?>> 예약 </option>
				<option value='1' <?if($row->STATUS_== '1') echo 'selected'?>> 계량 </option>
		</select>
	</td> 
	<td> <input type='text' name='name' size='8' value='<?=$row->NAME_?>'> </td> 	
   </tr> 
  </table>
  <input type='hidden' name="idx" value="<?=$row->IDX_?>">
  <br ><br >
  <center>
 <button type="button" class="btnB" onClick="goBack()" > 취소 </button>
 <button type="button" class="btnR" onClick="goDel()" > 삭제 </button>
 <button type="submit" class="btn"> 수정 </button>
  </center>
 <br /><br />
 </form>
</body>
</html>
