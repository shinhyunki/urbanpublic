<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    /*border: 0.2em solid red;*/
	min-width:48%;
	/*display:inline;*/
	padding:10px 10px;
}

th, td {
    text-align: right;
	font-size: 95%;
    padding: 0.8em 0.5em;
	border: 0.1em solid #eee;
}


th {
	 text-align: center;
}
tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select, option {
	font-size:1em;

}
</style>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.js"></script>
<script>

function thou(num) {	
	 return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function updateData() {
  $.ajax({        
    type : "POST",
    url : "adminAjax.php",
    dataType : "html",
    //contentType: 'application/json; charset=utf-8',
	error:function(jqXHR, textStatus, errorThrown){
            alert(textStatus + " : " + errorThrown);
            self.close();
        },
    success:function(response){

	  data = response.split('@@@');	  

	  if(data[0]>0) {
        var obj =  JSON.parse(data[1]);
		var minusTotal=0;
		var snetTotal=0;
		var perAverage=0;
	    var scaleStatusData = '<table><tr><th colspan=11> 금일 계량완료 현황 : '+data[0]+' 건</th></tr><tr><td > 순번 </td><td > 거래처 </td><td> 구분 </td><td> 품목 </td><td> 차량번호 </td><td> 총중량 </td><td> 공차중량 </td><td> 실중량 </td><td> 감량 </td><td> 감률 </td><td> 인수량 </td></tr>';
        for (var k=0;k<data[0];k++) {

		  
		  snetTotal+=parseInt(obj[k][0].SNET_);	
		  minusTotal+=parseInt(obj[k][0].MINUS_);

  		  obj[k][0].NET_ = (obj[k][0].NET_)? thou(obj[k][0].NET_):"0" ;
		  obj[k][0].GROSS_ = (obj[k][0].GROSS_)? thou(obj[k][0].GROSS_):"0" ;
		  obj[k][0].CAR_ = (obj[k][0].CAR_)? thou(obj[k][0].CAR_):"0" ;
		  obj[k][0].NET_ = (obj[k][0].NET_)? thou(obj[k][0].NET_):"0" ;
		  obj[k][0].MINUS_ = (obj[k][0].MINUS_)? thou(obj[k][0].MINUS_):"0" ;
		  obj[k][0].SNET_ = (obj[k][0].SNET_)? thou(obj[k][0].SNET_):"0" ;
	
          scaleStatusData += '<tr><td>'+obj[k][0].WNO_+'</td><td>'+obj[k][0].VENDR_+'</td><td>'+obj[k][0].WGUBN_+'</td><td>'+obj[k][0].ITEM_+'</td><td>'+obj[k][0].CARNO_+'</td><td>'+obj[k][0].GROSS_+'</td><td>'+obj[k][0].CAR_+'</td><td>'+obj[k][0].NET_+'</td><td>'+obj[k][0].MINUS_+'</td><td>'+Math.floor(obj[k][0].PERC_)+'</td><td>'+obj[k][0].SNET_+'</td></tr>';

        }	  
        scaleStatusData += '</table>';
		perAverage = Math.floor(minusTotal/(snetTotal+minusTotal)*10000)/100;
		snetTotal = (snetTotal)? thou(snetTotal):"0" ;
		scaleNow = ' [ 차량:'+data[0]+'대 감량:'+perAverage+'% 입고:'+snetTotal+'kg ]';
	    document.getElementById("scaleStatus").innerHTML = scaleStatusData;	  
		document.getElementById("scalenow").innerHTML = scaleNow;
	  } else {
	    document.getElementById("scaleStatus").innerHTML = "오늘 계량 완료된 차량이 없습니다";	
	  }

      if(data[2]>0) {
        var obj2 =  JSON.parse(data[3]);
	    var scaleReserveData = '<table><tr><th colspan=5> 금일 예약대기 현황 : '+data[2]+' 건</th></tr><tr><td > 거래처 </td><td> 구분 </td><td> 품목 </td><td> 차량번호 </td></tr>';
	  
	    for (var l=0;l<data[2];l++) {
          obj2[l][0].EXPECTWEIGHT_ = (obj2[l][0].EXPECTWEIGHT_)? thou(obj2[l][0].EXPECTWEIGHT_):"0" ;
          scaleReserveData += '<tr><td>'+obj2[l][0].VENDR_+'</td><td>'+obj2[l][0].WGUBN_+'</td><td>'+obj2[l][0].ITEM_+'</td><td>'+obj2[l][0].CARNO_+'</td></tr>';
	    }	  
	    scaleReserveData += '</table>';	
	    document.getElementById("scaleReserve").innerHTML = scaleReserveData;	
	  } else {
	    document.getElementById("scaleReserve").innerHTML = "오늘 예약된 차량이 없습니다";	
	  }
	  
      if(data[4]>0) {
	    var obj3 =  JSON.parse(data[5]);
	    var inspectData = '<table><tr><th colspan=8> 검수대기 현황 : '+data[4]+' 건</th></tr><tr><td > 순번 </td><td > 거래처 </td><td> 구분 </td><td> 품목 </td><td> 차량번호 </td><td> 총중량 </td><td> 예상공차 </td><td> 예상실중 </td><td> 입차시간 </td></tr>';

	    for (var m=0;m<data[4];m++) {
          var expWeight = Math.abs(obj3[m][0].REGCARWGT_-obj3[m][0].GROSS_);
		  if(!obj3[m][0].REGCARWGT_) obj3[m][0].REGCARWGT_='미등록';
		  if(obj3[m][0].REGCARWGT_==0) obj3[m][0].REGCARWGT_='미등록';
		  
		  obj3[m][0].GROSS_ = thou(obj3[m][0].GROSS_);
		  obj3[m][0].REGCARWGT_ = thou(obj3[m][0].REGCARWGT_);
		  expWeight = thou(expWeight);
		  
          inspectData += '<tr><td>'+obj3[m][0].WNO_+'</td><td>'+obj3[m][0].VENDR_+'</td><td>'+obj3[m][0].WGUBN_+'</td><td>'+obj3[m][0].ITEM_+'</td><td>'+obj3[m][0].CARNO_+'</td><td>'+obj3[m][0].GROSS_+'</td><td>'+obj3[m][0].REGCARWGT_+'</td><td>'+expWeight+'</td><td>'+obj3[m][0].GDATETIME_.substr(11,8)+'</td></tr>';
	    } 
	    inspectData += '</table>';	
	    document.getElementById("inspect").innerHTML = inspectData;
	  } else {
	    document.getElementById("inspect").innerHTML = "현재 검수 대기중인 차량이 없습니다.";
      }
	  if(data[6]>0) {
	    var obj4 =  JSON.parse(data[7]);
	    var outData = '<table><tr><th colspan=12> 공차대기 현황 : '+data[6]+' 건</th></tr><tr><td > 순번 </td><td > 거래처 </td><td> 구분 </td><td> 품목 </td><td> 차량번호 </td><td> 총중량 </td><td> 예상공차 </td><td> 감률  </td><td> 감량 </td><td> 예상인수 </td><td> 입차시간 </td></tr>';
	    for (var n=0;n<data[6];n++) {
          var expWeight = Math.abs(obj4[n][0].REGCARWGT_-obj4[n][0].GROSS_);
		  expWeight = expWeight - obj4[n][0].MINUS_;
		  if(!obj4[n][0].REGCARWGT_) obj4[n][0].REGCARWGT_='미등록';
		  if(obj4[n][0].REGCARWGT_==0) obj4[n][0].REGCARWGT_='미등록';
		  obj4[n][0].GROSS_ = (obj4[n][0].GROSS_)? thou(obj4[n][0].GROSS_):"0" ;
		  obj4[n][0].REGCARWGT_ = (obj4[n][0].REGCARWGT_)? thou(obj4[n][0].REGCARWGT_):"0" ;
		  obj4[n][0].MINUS_ = (obj4[n][0].MINUS_)? thou(obj4[n][0].MINUS_):"0" ;
		  expWeight = expWeight ? thou(expWeight):"0" ;
		  var perC = parseInt(obj4[n][0].PERC1_)+parseInt(obj4[n][0].PERC2_);
          outData += '<tr><td>'+obj4[n][0].WNO_+'</td><td>'+obj4[n][0].VENDR_+'</td><td>'+obj4[n][0].WGUBN_+'</td><td>'+obj4[n][0].ITEM_+'</td><td>'+obj4[n][0].CARNO_+'</td><td>'+obj4[n][0].GROSS_+'</td><td>'+obj4[n][0].REGCARWGT_+'</td><td>'+perC+'</td><td>'+obj4[n][0].MINUS_+'</td><td>'+expWeight+'</td><td>'+obj4[n][0].GDATETIME_.substr(11,8)+'</td></tr>';
	    } 
	    outData += '</table>';	
	    document.getElementById("out").innerHTML = outData;
	  } else {
	    document.getElementById("out").innerHTML = "현재 공차 대기중인 차량이 없습니다.";
      }
	  document.getElementById("now").innerHTML = data[8];	  
    }               
  })
  setTimeout("updateData()", 15000); 
}
</script>
</head>
<body>
<script> updateData();	</script>
<center> <h2> 계량 현황 : <span id='now'></span> 기준  <font color='blue'> <span id='scalenow'> </span></font></h2> </center>
<span style="display:inline-block; min-width:37%; height:480px; border:1px solid #ddd; overflow:auto;" id="scaleReserve"> </span>
<span style="display:inline-block; min-width:61%; height:480px; border:1px solid #ddd; align:left; overflow:auto;" id="scaleStatus"> 
</span><br><br>
<span style="display:inline-block; min-width:49%; height:400px; border:1px solid #ddd; overflow:auto;" id="inspect"> </span>
<span style="display:inline-block; min-width:49%; height:400px; border:1px solid #ddd; align:left; overflow:auto;" id="out">
</span>







