<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<?
include 'db_access.php'; 
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$userID = $_SESSION["join_id"];
$sqlUser = "SELECT `USERSTATUS_` FROM `tuserinfo` WHERE `USERID_`='$userID'";
$resultUser = $mysqli->query($sqlUser);
$userStatus=$resultUser->fetch_object()->USERSTATUS_;

if($userStatus<3) echo "<script language='javascript'> alert('사용권한이 없습니다.'); location.replace('Login.php'); </script>";
else if($userStatus<9) $userAdmin = 1;
else $userAdmin = 9;

$now = date('Ymd');

isset($_REQUEST["field"]) ? $field = $_REQUEST["field"]:$field='';
isset($_REQUEST["field_detail"]) ? $field_detail = $_REQUEST["field_detail"]:$field_detail='';
if($field=='') $field = 'IDX_';
else{
	$field = $_REQUEST["field"];
	$field_detail = $_REQUEST["field_detail"];
}

isset($_REQUEST["page"]) ? $page=$_REQUEST["page"]:$page=1;


$list_num=10;
$start_rec=($page-1)*$list_num;

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

if($userAdmin==9) $sql = "select count(IDX_) as dataCount from tuserinfo WHERE ($field like '%$field_detail%') and (VCOD_ IN (SELECT VCOD_ FROM tvcod WHERE tvcod.WGUBN_='원재료') or VCOD_ IN (000001, ''))"; 
else $sql = "select count(IDX_) as dataCount from tuserinfo WHERE ($field like '%$field_detail%') and (VCOD_ IN (SELECT VCOD_ FROM tvcod WHERE tvcod.WGUBN_<>'원재료') or VCOD_ IN (000001, ''))"; 

$result = $mysqli->query($sql);

$total_rec=$result->fetch_object()->dataCount;
// 개발자 아이디 레코드
$total_rec = $total_rec - 1;

$total_page=ceil($total_rec/$list_num);

$join = 1;


?>




<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 1em 1.5em;
  text-decoration: none;
  font-size: 1em;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 0.2em solid #ddd;
	max-width:100%;
}

th, td {
    text-align: left;
	font-size: 100%;
    padding: 1em;	
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

input, select {
	font-size:1em;

}

tr:hover {
	background-color:#f49d9d;
} 


</style>

</head>
<body>

<div> <center> <a href="AdminMain.php?page=1"> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </a> </center> </div><br />

<div class="topnav">
  <a href="AdminMain.php?page=1"> 계량현황 </a>
  <a href="AdminReserve.php?page=1"> 예약현황 </a>
  <a class="active" href="AdminUser.php?page=1"> 사용자관리 </a>
  <?if($userAdmin==9) { ?> 
  <a href="AdminNotice.php?page=1"> 공지사항 </a>
  <a href="AdminTras.php?page=1"> 거래내역 </a>
  <a href="#"> 기타 </a>
  <? } else { ?>
  <a href="AdminETC2.php"> 기타 </a>
  <?}?>
</div>


<? 
	if($userAdmin==9) $sql = "select * from tuserinfo WHERE ($field like '%$field_detail%') and (VCOD_ IN (SELECT VCOD_ FROM tvcod WHERE tvcod.WGUBN_='원재료') or VCOD_ IN (000001, '')) ORDER by VENDR_ limit $start_rec, $list_num";
	else $sql = "select * from tuserinfo WHERE ($field like '%$field_detail%') and (VCOD_ IN (SELECT VCOD_ FROM tvcod WHERE tvcod.WGUBN_<>'원재료') or VCOD_ IN (000001, '')) ORDER by VENDR_ limit $start_rec, $list_num";
	$result = $mysqli->query($sql);
	if($userAdmin==9) echo "원재료 및 관리자";
	else echo "부재료, 폐합성수지, 소각폐기물, 제품 및 관리자";

?>
<BR><BR>
<table>
  <tr>
    <th>벤더이름</th>
    <th>벤더코드</th>
	<th>사용자ID</th>
	<th>사용자이름</th>
	<th>이메일</th>
	<th>전화번호</th>
	<th>등급</th>
	<th>정보변경일</th>
  </tr>


 <? $k=0; while($row = $result->fetch_object()) { if($row->IDX_!=19) { $k++;?>
   
   <tr onclick="location.href='AdminUserEdit.php?idx=<?=$row->IDX_?>'" style="cursor:hand"> 
    <td> <?=$row->VENDR_?></td> 
	<td> <?=$row->VCOD_?> </td> 
	<td> <?=$row->USERID_?> </td> 
	<td> <?=$row->USERNAME_?> </td> 
	<td> <?=$row->USEREMAIL_?> </td> 	
	<td> <?=$row->USERPHONE_?> </td>
	<td> <?=$row->USERSTATUS_?> </td>
	<td> <?=$row->UPDATETS_?> </td> 
   </tr>
 <?}}?>

  </table>
  <div align='right'> 등급 0:신청 1:정지 2:기사 3:업체관리자 4/5:대림제지 7:검수자 9:관리자 </div>
  <br>
  <center>

 <? 
	$page_num = 10;
	$total_block=ceil($total_page/$page_num);
	$block = ceil($page/$page_num);
	$first=($block-1)*$page_num+1;
	$end=($block*$page_num);

	if($total_block==$block) $end=$total_page;
	if($block>1){
		echo "[<a href='AdminUser.php?page=1&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'> 처음 </a>]";
		echo "<a href='AdminUser.php?page=".($first-1)."&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'> ◀ </a>";
	}

	for($l=$first; $l<=$end; $l++){
		if($l==$page){
			echo "<b>  $l  </b>";
		}else{
			echo " [<a href='AdminUser.php?page=$l&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'>$l</a>] ";
		}
	}
    
	if($block<$total_block){
		echo "[<a href='AdminUser.php?page=".($end+1)."&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'>다음 $page_num 개 </a>]";
		echo "[<a href='AdminUser.php?page=$total_page&field=$field&field_detail=$field_detail&date_start=$date_start&date_end=$date_end'>마지막</a>]";
	}

	?>

<br>
<br>
<form name="search" method=post action='AdminUser.php?page=1&field=<?$field?>&field_detail=<?$field_detail?>&date_start=<?$date_start?>&date_end=<?$date_end?>'>

<select name="field">
	<option value="VENDR_" selected> 벤더이름 </option>
	<option value="VCOD_"> 벤더코드 </option>
	<option value="USERID_"> 사용자ID </option>
	<option value="USERNAME_"> 사용자이름 </option>
	<option value="USERSTATUS_"> 등급 </option>
</select> 
<input type=text size=30 maxlength=30 name="field_detail" align=left>


<input type="submit" value=" Search " >
</form>

</center>

</body>
</html>
