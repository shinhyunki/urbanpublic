<? session_start(); ?>
<? if($_SESSION["join_id"]) $join_id = $_SESSION["join_id"];
else echo "<script language='javascript'> alert('로그인 정보를 알 수 없습니다. 다시 로그인 하세요.'); location.replace('Login.php'); </script>";

$idx=$_GET["idx"];

include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$sql = "SELECT * FROM `TRESERVE` WHERE IDX_ = '$idx'"; 
$result = $mysqli->query($sql);
$row = $result->fetch_object();

$carNum = $row->CARNO_;
$carDriver = $row->NAME_;
$venderCode = $row->VCOD_;
$itemCode = $row->ICOD_;
$venderName = $row->VENDR_;
$itemName = $row->ITEM_;
$expectWeight = $row->EXPECTWEIGHT_;
$wgubn = $row->WGUBN_;
$inOut = $row->INOUT_;
$destinationTime = $row->DATE_;
$destinationTime = substr($destinationTime, 0, 16); 

$sql3 = "SELECT USERSTATUS_ FROM `TUSERINFO` WHERE USERID_ = '$join_id'";
$result3 = $mysqli->query($sql3);
$userState = $result3->fetch_object()->USERSTATUS_;

?>


<html> 
<head> 
<title> :: 대림제지 - 사전정보예약 :: </title>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Add icon library -->
<link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

.input-container {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    width: 100%;
    margin-bottom: 5px;
}

.icon {
    padding: 10px;
    background: hotpink;
    color: white;
    min-width: 100px;
    text-align: center;
}

.input-field {
    width: 100%;
    padding: 10px;
    outline: none;
}

.input-field:focus {
    border: 2px solid hotpink;
}

/* Set a style for the submit button */
.btn {
    background-color: hotpink;
    color: white;
    padding: 10px 10px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
	position:relative;
	width:100%;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}

/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 15px;
    width: 15px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 5px;
    top: 2px;
    width: 4px;
    height: 8px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}

</style>

<style>
/*the container must be positioned relative:*/
.custom-select {
  position: relative;
  font-family: Arial;
  margin-bottom: 8px;
}
.custom-select select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: hotpink;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 30px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
  padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
  user-select: none;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: hotpink;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}

#myInput {
  background-image: url('image/searchicon.png');
  background-position: 350px 5px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 14px;
  padding: 10px 10px 5px 10px;
  border: 1px solid #ddd;
  margin-bottom: 0px;
}

#myUL {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myUL li a{
  border: 1px solid #ddd;
  margin-top: -1px; /* Prevent double borders */
  background-color: #f6f6f6;
  padding: 5px 5px 5px 5px;
  text-decoration: none;
  font-size: 14px;
  color: black;
  display: block;
  margin-bottom: 5px;
}


#myUL li a:hover:not(.header) {
  background-color: #eee;
}

</style>

<script  src="http://code.jquery.com/jquery-latest.min.js"></script>

<script>

function goRegister() {
	 alert("취소 되었습니다.");
	 location.replace('VenderView.php');
}

function goRegister2() {	 
	 location.replace('VenderView.php');
}

function itemInput(a) {
	 document.getElementById("myInput").value=a;
	 document.getElementById("myUL").value="";
	 document.getElementById("itemList").innerHTML = "";
}

function radioClick(wgubn) {
  document.getElementById("myInput").value="";
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("itemList").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "wGubn.php?wGubn="+wgubn, true);
  xhttp.send();   
}

function goURL(URL) {
	var idx = <?=$idx?>;
	var url = URL+'.php?idx='+idx;
}

function goDel(URL) {
	if(confirm("정말로 삭제하시겠습니까?")) {
	var idx = <?=$idx?>;
	var url = URL+'.php?idx='+idx;
	location.replace(url); }
}
</script>

</head>

<body>


<form  name="registerContent" method="post" action="venderReserveEditOK.php" style="max-width:500px;margin:auto">

<h2 align='center'> 예약 상세내역 </h2>
<?if($userState>2){?>
<p> <i> 수정사항을 입력하고, 수정버튼을 누르면 수정이 완료됩니다. <br> 품목수정시 원래내용을 삭제하고, 코드나 품명으로 검색된 내용을 반드시 선택해야 합니다.</i></p>
<?}?>

  <!--  <div class="custom-select" style="max-width:500px;" name="destinationLocal">
    <select>
	    <option value="0"> 도착지 : 본사 </option>
		<option value="1"> 제1공장 </option>
	    <option value="2"> 제2공장 </option>
		<option value="3"> 마라도 </option>
	</select> -->

<!-- <i class="fa fa-university icon"></i> -->
 </div>
  <div class="input-container"> 
	<i class="fa fa-building icon"> 사명</i>
    <input class="input-field" type="text" placeholder="회사명 : 로그인정보에서 자동으로 불러옴(입력불가)" name="venderName" value="<?=$venderName?>">
  </div>
  <div class="input-container">
	<i class="fa fa-info icon"> 벤더</i> 
    <input class="input-field" type="text" placeholder="벤더구분 : 로그인정보에서 자동으로 불러옴(입력불가)" name="venderCode" value="<?=$venderCode?>">
  </div>
 
  <!--<div class="input-container">
	<i class="fa fa-user-circle-o icon"> 기사</i>
    <input class="input-field" type="text" placeholder="기사명 : 필수입력사항 아님" name="carDriver"	value="<?=$carDriver?>" >
  </div>

  <div class="input-container">
    <i class="fa fa-balance-scale icon"> 중량</i>
    <input class="input-field" type="number" placeholder="예상중량(KG) : 필수입력사항 아님" name="expectWeight" value="<?=$expectWeight?>">
  </div>-->
 
  <div class="input-container">
    <i class="fa fa-clock-o icon"> 도착</i>
    <input class="input-field" type="text" placeholder="도착예정일시" name="destinationTime" value="<?=$destinationTime?>">
  </div>

  <div class="input-container">
	<i class="fa fa-truck icon"> 차번</i>
    <input class="input-field" type="text" placeholder="12가3456 처럼 풀번호 입력" name="carNo" 
	value="<?=$carNum?>">
  </div>
  <div class="input-container" >
	<i class="fa fa-question icon" style="width:100%"> 분류 선택</i> </div>
	<table width="100%">
	 <tr>
	  <td width='50%'><input type="radio" value="원재료" name="wgubn" onclick="radioClick('원재료')" <?if($wgubn=='원재료') echo 'checked'?>> 원재료 </td>
	  <td width='50%'><input type="radio" value="부재료" name="wgubn" onclick="radioClick('부재료')" <?if($wgubn=='부재료') echo 'checked'?>> 부재료 </td>
	 </tr>
	 <tr>
	  <td><input type="radio" value="폐합성수지" name="wgubn" onclick="radioClick('폐합성수지')" <?if($wgubn=='폐합성수지') echo 'checked'?>> 폐합성수지 </td>
	  <td><input type="radio" value="소각폐기물" name="wgubn" onclick="radioClick('소각폐기물')" <?if($wgubn=='소각폐기물') echo 'checked'?>> 소각폐기물  </td>
	 </tr>
	 <tr>
	  <td><input type="radio" value="제품판매" name="wgubn" onclick="radioClick('제품판매')" <?if($wgubn=='제품판매') echo 'checked'?>> 제품판매 </td>
	  <td><input type="radio" value="기타입고" name="wgubn" onclick="radioClick('기타')" <?if($wgubn=='기타' and $inOut==1) echo 'checked'?>> 기타입고 </td>
     </tr>
	 <tr> 	  
	  <td colspan="2"> <input type="radio" value="기타출고" name="wgubn" onclick="radioClick('기타')" <?if($wgubn=='기타' and $inOut==2) echo 'checked'?>> 기타출고 </td>
	 </tr>
	</table>
    <br>
    
  <div class="input-container">
	<i class="fa fa-question icon"> 품목</i>
	<input type="text" id="myInput" oninput="myFunction()" placeholder="품목" title="Type in a name" name="itemName" value="<?=$itemName.'(code:'.$itemCode.')'?>">	
  </div>
	<ul id="myUL">
	<? 
	$sql = "SELECT * FROM `TICOD` where (WGUBN_='$wgubn'  AND ICOD_ != 414 AND ICOD_ != 410) or WGUBN_=''"; 
	$result = $mysqli->query($sql);
	echo '<div id="itemList">';
	while($row = $result->fetch_object()){
		?> <li><a href='#' onclick=itemInput("<?=$row->ITEM_.'(code:'.$row->ICOD_?>)");return false;><?=$row->ITEM_.'(code:'.$row->ICOD_?>)</a></li>
	<?}?>
	</ul>
  <br />

  <input type='hidden' name=idx value="<?=$idx?>">
  <?if($userState>2){?>
  <table border=0 width=100%>
  <tr><td><button type="button" class="btnR" style='background-color: silver;' onClick="goRegister()"> 취소 </button> </td>
  <td><button type="button" class="btnR" style='background-color:#555555;' onclick="goDel('venderReserveEditDel');"> 삭제 </button></td>
  <td><button type="submit" class="btnR" > 수정 </button></td></tr></table> <?}?>
  <?if($userState==2){?><button type="button" class="btnR" style='background-color: silver;' onClick="goRegister2()"> 목록화면 </button> <?}?>
</form>


 <script>
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script>

<script>
function myFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
</script>

</body> 
</html> 
