<? session_start(); ?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding: 16px;
}

tr:nth-child(odd) {
    background-color: #f2f2f2
}

.btn {
    background-color: dodgerblue;
    color: white;
    padding: 10px 10px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.btnR {
    background-color: hotpink;
    color: white;
    padding: 15px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
	position:relative;
	width:100%;
}

.btn:hover, .btnR:hover {
    opacity: 1;
}
</style>

<script>

function goURL(URL) {
	var url = URL+'.php';
	location.href = url;
}

</script>

</head>
<body>
 <div> <center> <img src = "image/ci.jpg" width="80%" style="max-width:383px;"> </center> </div> 
<? 

if($_SESSION["join_id"]) $join_id = $_SESSION["join_id"];
else echo "<script language='javascript'> alert('로그인 정보를 알 수 없습니다. 다시 로그인 하세요.'); location.replace('Login.php'); </script>";

include 'db_access.php';

$now = date("Y-m-d H:i:s"); 

$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$sqlVC = "SELECT `VCOD_`, `VENDR_` FROM `TUSERINFO` WHERE USERID_='$join_id'";
$resultVC = $mysqli->query($sqlVC);
$rowVC = $resultVC->fetch_object();
$venderName = $rowVC->VENDR_;
$venderCode = $rowVC->VCOD_;

$sqlW = "SELECT `WGUBN_` FROM `TVCOD` WHERE VCOD_='$venderCode'";
$resultW = $mysqli->query($sqlW);
$flagW=0;
while($rowW=$resultW->fetch_object()){
	if(($rowW->WGUBN_)=='원재료') $flagW=1;	
}
if($flagW==1) $wGubn='원재료';

$sql = "SELECT `NOTICETITLE_`, `NOTICECONTENTS_` FROM `TNOTICE` WHERE ((VCOD_=0 or VCOD_='$venderCode') and (STARTDATETIME_<'$now' and ENDDATETIME_>'$now'))"; 
$result = $mysqli->query($sql);


$i = 0;

?>
<h3> 공지사항 </h3>
<table>
<?
if($wGubn=='원재료') {
	while($row = $result->fetch_object()) { $i++; ?>
	<tr><th> <?=nl2br($row->NOTICETITLE_)?> </th></tr>
	<tr><td> <?=nl2br($row->NOTICECONTENTS_)?> </td></tr>

<? }
}

 
if(($i==0) or $wGubn!='원재료') echo "<tr><th> 공지사항이 없습니다. </th></tr>";
?>
</table>
<br />
<table border="0" width="80%" style='border: 0px'>
  <tr style="padding: 5px; background-color: #ffffff"><td style="padding: 5px;"> <button type="button" class="btnR" style='background-color: silver;' onclick="goURL('VenderTrans')"> 거래내역 </button> </td><td style="padding: 0px;"> 
  <button type="button" class="btnR" style='background-color: hotpink;padding: 16px;' onclick="goURL('VenderView')"> 예약조회 </button></td><td style="padding: 5px;">
  <button type="button" class="btnR" style='background-color: dodgerblue;' onclick="goURL('VenderReserve')"> 예약신청 </button></td></tr><table>
</body>
</html>
<?
$mysqli->close();
?>