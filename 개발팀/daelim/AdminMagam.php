<? session_start(); 
if(!$_SESSION["join_id"]) echo "<script language='javascript'> alert('로그인 시간이 만료되었습니다. 다시 로그인해주세요.'); location.replace('Login.php'); </script>";
?>

<? include 'db_access.php';
$mysqli = new mysqli($db_host, $db_id, $db_pw, $db_name, $db_port);
$mysqli->query("SET NAMES 'utf8'");

$startDate=$_GET["startDate"];
$endDate=$_GET["endDate"];

$start_year = $_REQUEST["start_year"]; 
$start_month = $_REQUEST["start_month"]; 
$start_day = $_REQUEST["start_day"]; 
$end_year = $_REQUEST["end_year"]; 
$end_month = $_REQUEST["end_month"]; 
$end_day = $_REQUEST["end_day"]; 

$startDate = $start_year.'-'.$start_month.'-'.$start_day;
$endDate = $end_year.'-'.$end_month.'-'.$end_day;

//$startDate = '2018-01-01';
//$endDate = '2018-12-31';
$venderCode = '12345';
$hap = $venderCode.'_HAP';

$sqlV = "SELECT `VCOD_`, `VENDR_`, `NAME_` FROM `TVCOD`"; 
$resultV = $mysqli->query($sqlV);
$j=0;
while($rowV=$resultV->fetch_object()){
	$venderInfo[$j]['vcod'] = $rowV->VCOD_;
	$venderInfo[$j]['vender'] = $rowV->VENDR_;
	$venderInfo[$j]['name'] = $rowV->NAME_;
	$j++;
}

$sqlD = "SELECT DISTINCT `VCOD_` FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료'"; 
$resultD = $mysqli->query($sqlD);
$k=0;
while($rowD=$resultD->fetch_object()){
	$vender[$k]['vcod'] = $rowD->VCOD_;
	$vender[$k]['hap'] = 0;
	for($l=0;$l<$j;$l++) {
		if($vender[$k]['vcod'] == $venderInfo[$l]['vcod']) {
			$vender[$k]['vender'] = $venderInfo[$l]['vender'];
			$vender[$k]['name'] = $venderInfo[$l]['name'];
		}
	}
	//echo $k.':'.$vender[$k]['vcod'].'<br>';
	$k++;	
}

$sql = "SELECT `VCOD_`, `SNET_`, `HAP_`  FROM `TDATA` WHERE `DATE_`>='$startDate' and `DATE_`<='$endDate' and `STATUS_`>2 and `WGUBN_`='원재료'"; 
$result = $mysqli->query($sql);

while($row=$result->fetch_object()) { 
	for($i=0;$i<$k;$i++) {		
		if($vender[$i]['vcod'] == $row->VCOD_) {
			$vender[$i]['hap'] += $row->HAP_;
			$vender[$i]['snet'] += $row->SNET_;			
			//echo $vender[$i]['vcod'].':'.$vender[$i]['hap'].'<br>';
			//if($row->VCOD_ == '09003') echo $row->HAP_.":".(135*$row->SNET_)."<br>";
		}
	}
}



// 정렬
$arr_type = array(); 
$arr_file = array(); 
foreach ($vender as $idx=>$val) { 
  $arr_type[$idx] = $val['snet']; 
  $arr_file[$idx] = $val['hap']; 
} 
array_multisort($arr_type, SORT_DESC, $arr_file, SORT_DESC, $vender); 

?>

<!DOCTYPE html>
<html lang="ko">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script src="script/jquery-latest.min.js"></script>
    <script type="text/javascript" src="script/jquery.battatech.excelexport.js"></script>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }
        table{
            width: 650px;
            text-align: center;
            border: 1px solid black;
			font-size:12px;
        }
		th {
			font-size:15px;
		}
		.btn {
		   background-color: hotpink;
		    color: white;
		    padding: 10px 10px;
		    border: none;
		    cursor: pointer;
		    width: 20%;
		    opacity: 0.9;
			margin : auto;			
		}
    </style>
	<script>
		function goURL(URL) {
			var url = URL+'.php';
			location.replace(url);
		}
	</script>
</head>

<body>
 <div id="wrap" align='center'>
  <table id='tblExport' border=1>
   <tbody>
    <tr>
	 <th colspan='9' align='center'> <?=$startDate?> ~ <?=$endDate?> 원재료 마감 </font> </td>
	</tr>
	<tr>
     <td> 거래처코드 </td>
	 <td> 거래처상호</td>
	 <td> 중량</td>
	 <td> 단가</td>
	 <td> 공급가액</td>
	 <td> 세액</td>
	 <td> 합계</td>
	 <td> 성명</td>
	 <td width='150'> 비고</td>
    </tr>
	<?for($i=0;$i<$k;$i++) {?>
	<tr>
     <td> <?=$vender[$i]['vcod']?> </td>
	 <td> <?=$vender[$i]['vender']?></td>
	 <td> <?=number_format($vender[$i]['snet'])?></td>
	 <td> <?=number_format(($vender[$i]['hap']/$vender[$i]['snet']))?></td>
	 <td> <?=number_format($vender[$i]['hap'])?></td>
	 <td> <?=number_format($vender[$i]['hap']*0.1)?></td>
	 <td> <?=number_format($vender[$i]['hap']*1.1)?></td>
	 <td> <?=$vender[$i]['name']?></td>
	 <td> </td>
    </tr>
	<? } ?>
   </tbody>
  </table>     
 </div>
 <br /><br />	  
 <center>
  <a id="btnExport" href="#" download="<?='원재료마감_'.$startDate.'_'.$endDate?>.xls"> 	
	<button type="button" class="btn" style="background-color: dodgerblue;"> 저장 </button></a>
	<button type="button" class="btn" onclick="window.print()"> 인쇄 </button> 
	
 </center>
 
<script type="text/javascript">
    $(document).ready(function () {
 
        function itoStr($num)
        {
            $num < 10 ? $num = '0'+$num : $num;
            return $num.toString();
        }
         
        var btn = $('#btnExport');
        var tbl = 'tblExport';
 
        btn.on('click', function () {
            var dt = new Date();
            var year =  itoStr( dt.getFullYear() );
            var month = itoStr( dt.getMonth() + 1 );
            var day =   itoStr( dt.getDate() );
            var hour =  itoStr( dt.getHours() );
            var mins =  itoStr( dt.getMinutes() );
 
            var postfix = year + month + day + "_" + hour + mins;
            var fileName = "Daelim_"+ postfix + ".xls";
 
            var uri = $("#"+tbl).excelexportjs({
                containerid: tbl
                , datatype: 'table'
                , returnUri: true
            });
 
            $(this).attr('download', fileName).attr('href', uri).attr('target', '_blank');
        });
    });
</script>
</body> 
</html>