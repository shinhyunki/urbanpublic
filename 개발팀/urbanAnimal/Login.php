<html> 
<head> 
<title> :: UrbanAnimal  :: </title>
<meta http-equiv="Content-Type" content="text/html" charset=utf-8> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="https://static.nid.naver.com/js/naverLogin_implicit-1.0.3.js" charset="utf-8"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Add icon library -->
<link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

.input-container {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    width: 100%;
    margin-bottom: 15px;
}

.icon {
    padding: 10px;
    background: dodgerblue;
    color: white;
    min-width: 50px;
    text-align: center;
}

.input-field {
    width: 100%;
    padding: 10px;
    outline: none;
}

.input-field:focus {
    border: 2px solid dodgerblue;
}

/* Set a style for the submit button */
.btn {
    background-color: 1344ff;
    color: white;
    padding: 15px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
}

.btnR {
    background-color: 1344ff;
    color: white;
    padding: 15px 20px;
    border: none;
    cursor: pointer;
    width: 100%;
    opacity: 0.9;
	position:relative;
	left:50%;
	transform: translateX(-50%);

}

.btn:hover, .btnR:hover {
    opacity: 1;
}

/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 15px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 15px;
    width: 15px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 5px;
    top: 2px;
    width: 4px;
    height: 8px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
</style>

<script>

function goRegister() {
	 location.href = 'Register.php'
}

</script>

</head>

<body>
<?php
	$join_id = $_COOKIE['join_id'] ?? '';
    $join_pw = $_COOKIE['join_pw'] ?? ''; 
?>

<form  name="loginform" method="post" action="login_ok.php" style="max-width:500px;margin:auto">

  <div> <center> <img src = "image/ani.jpg" width="80%" style="max-width:383px;"> </center> </div> <br />
  <div class="input-container">
    <i class="fa fa-user icon"></i>
    <input class="input-field" type="text" placeholder="사용자이름" name="join_id" value="<?=$join_id?>">
  </div>

  <div class="input-container">
    <i class="fa fa-key icon"></i>
    <input class="input-field" type="password" placeholder="Password" name="join_pw" value="<?=$join_pw?>">
  </div>

 <button type="submit" class="btn"> 로그인 </button><br /><br />
 <label class="container"> 아이디, 패스워드 저장
  <input type="checkbox" checked="checked" name='idSave' value='1'> 
  <span class="checkmark"> </span>
 </label>
<!-- 네이버아이디로로그인 버튼 노출 영역 -->
<div id="naver_id_login"></div>
  <!-- //네이버아이디로로그인 버튼 노출 영역 -->
  <script type="text/javascript">
  	var naver_id_login = new naver_id_login("_Jo1lHk77wNpPQf48S1x", "http://localhost/urbanAnimal/callback.php");
  	var state = naver_id_login.getUniqState();
  	naver_id_login.setButton("white", 2,40);
  	// naver_id_login.setDomain("main.php");
  	naver_id_login.setState(state);
  	// naver_id_login.setPopup();
  	naver_id_login.init_naver_id_login();
  </script>
</form> 
<br/>
 <button type="button" class="btnR" onClick="goRegister()" style="max-width:500px;margin:auto"> 사용신청 </button>
</body> 
</html> 
 
  

