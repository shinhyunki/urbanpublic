

<style id="SITE_FOOTER-mesh-styles">

#SITE_FOOTERinlineContent {
    height: auto;
    width: 100%;
    position: relative;
    bgcolor : "black"
} 
 


#SITE_FOOTERinlineContent-gridWrapper {
    pointer-events: none;
}

#SITE_FOOTERinlineContent-gridContainer {
    position: static;
    display: grid;
    height: auto;
    width: 100%;
    min-height: auto;
    grid-template-rows: min-content min-content 1fr;
    grid-template-columns: 100%;
}

#comp-jqopdf7n {
    position: relative;
    margin: 54px 0px 24px calc((100% - 980px) * 0.5);
    left: 60px;
    grid-area: 1 / 1 / 2 / 2;
    justify-self: start;
    align-self: start;
}

#comp-jqokdc5y {
    position: relative;
    margin: 0px 0px 12px calc((100% - 980px) * 0.5);
    left: 60px;
    grid-area: 2 / 1 / 3 / 2;
    justify-self: start;
    align-self: start;
}

#comp-jqoug4a7 {
    position: relative;
    margin: 51px 0px 13px calc((100% - 980px) * 0.5);
    left: 801px;
    grid-area: 2 / 1 / 3 / 2;
    justify-self: start;
    align-self: start;
}

#comp-jqoug4a8 {
    position: relative;
    margin: 50px 0px 12px calc((100% - 980px) * 0.5);
    left: 840px;
    grid-area: 2 / 1 / 3 / 2;
    justify-self: start;
    align-self: start;
}

#comp-jqoug4a9 {
    position: relative;
    margin: 50px 0px 12px calc((100% - 980px) * 0.5);
    left: 890px;
    grid-area: 2 / 1 / 3 / 2;
    justify-self: start;
    align-self: start;
}

#comp-jsmsfp7l {
    position: relative;
    margin: 0px 0px 24px calc((100% - 980px) * 0.5);
    left: 60px;
    grid-area: 3 / 1 / 4 / 2;
    justify-self: start;
    align-self: start;
}

#SITE_FOOTERcenteredContent {
    position: relative;
}

#SITE_FOOTERinlineContent-gridContainer > * {
    pointer-events: auto;
}

#SITE_FOOTERinlineContent-gridContainer > [id$="-rotated-wrapper"] {
    pointer-events: none;
}

#SITE_FOOTERinlineContent-gridContainer > [id$="-rotated-wrapper"] > * {
    pointer-events: auto;
}
</style>
<div>
 
<a href="http://localhost/urbanAnimal/map.php" target="_self">지도보러가기</a> 
 
</div>
<div id="SITE_FOOTERinlineContent" class="style-jopjw44ginlineContent">

<br/>
<div id="SITE_FOOTERinlineContent-gridWrapper" data-mesh-internal="true"><div id="SITE_FOOTERinlineContent-gridContainer" data-mesh-internal="true"><div style="width: 94px; height: 22px;" title="" data-is-responsive="false" data-display-mode="fill" data-content-padding-horizontal="0" data-content-padding-vertical="0" data-exact-height="23" class="ca1" id="comp-jqopdf7n"><div style="width: 94px; height: 22px;" id="comp-jqopdf7nlink" class="ca1link"><div style="width: 94px; height: 23px; position: relative; top: 0px; left: 0px;" data-has-bg-scroll-effect="" data-style="" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-jqopdf8d&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;masterPage&quot;,&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;2.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;&quot;,&quot;uri&quot;:&quot;85c2f3_e7f041cd9623486bba8bd1842e4c27a3~mv2.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:188,&quot;height&quot;:46,&quot;alt&quot;:&quot;footer_img_logo.png&quot;,&quot;name&quot;:&quot;footer_img_logo.png&quot;},&quot;displayMode&quot;:&quot;fill&quot;}" class="ca1img" id="comp-jqopdf7nimg">
<img id="comp-jqopdf7nimgimage" style="object-position: 50% 50%; width: 94px; height: 23px; object-fit: cover;" alt="footer_img_logo.png" data-type="image" itemprop="image" src="">
</div></div></div>
<div data-packed="true" style="width: 573px; pointer-events: none;" class="txtNew" id="comp-jqokdc5y"><p class="font_8" style="font-size:13px; line-height:1.6em;">
<span style="color:#8D95A0;">
<span style="font-size:13px;">
<span style="font-family:wfont_85c2f3_636d2fd6be9d49d5a00d68ebcfd119a7,wf_636d2fd6be9d49d5a00d68ebc,orig_notokrdemilight;">
(주) UrbanAnimal &nbsp;| &nbsp;사업자등록번호 : 000 - 00 - 00000 &nbsp;| &nbsp;대표 : 박미희 &nbsp;| &nbsp;개인정보관리책임자 : 배성준<br>
여의도 진실에 방 2층&nbsp;UrbanAnimal<br>
Tel : 00.1111.1234 월-금 (10:00 - 19:00 ) Email : <object height="0"><a class="auto-generated-link" data-auto-recognition="true" data-content="hello@petdoc.co.kr" href="mailto:hello@petdoc.co.kr" data-type="mail">hello@petdoc.co.kr</a></object></span></span></span></p>

<p class="font_8" style="font-size:13px; line-height:1.6em;">
<span style="color:#8D95A0;">
    <span style="font-size:13px;">
        <span style="font-family:wfont_85c2f3_636d2fd6be9d49d5a00d68ebcfd119a7,wf_636d2fd6be9d49d5a00d68ebc,orig_notokrdemilight;">© 2019 UrbanAnimal Inc. All Rights Reserved.</span></span></span></p></div><div style="width: 13px; height: 28px;" title="" data-is-responsive="false" data-display-mode="fill" data-content-padding-horizontal="0" data-content-padding-vertical="0" data-exact-height="28" class="ca1" id="comp-jqoug4a7">
            <a style="cursor: pointer; width: 13px; height: 28px;" href="https://www.facebook.com/도시의 아이들" target="_blank" data-content="https://www.facebook.com/petdoc1" data-type="external" id="comp-jqoug4a7link" class="ca1link">
             <div style="width: 13px; height: 28px; position: relative; top: 0px; left: 0px;" data-has-bg-scroll-effect="" data-style="" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-jqoug4bq&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;masterPage&quot;,&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;&quot;,&quot;uri&quot;:&quot;85c2f3_0546340b59864edd9a080b0c00f5538d~mv2.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:26,&quot;height&quot;:56,&quot;alt&quot;:&quot;footer_btn_facebook.png&quot;,&quot;name&quot;:&quot;footer_btn_facebook.png&quot;,&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-jqoujxx5&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;masterPage&quot;,&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;https://www.facebook.com/petdoc1&quot;,&quot;target&quot;:&quot;_blank&quot;}},&quot;displayMode&quot;:&quot;fill&quot;}" class="ca1img" id="comp-jqoug4a7img"><img id="comp-jqoug4a7imgimage" style="object-position: 50% 50%; width: 13px; height: 28px; object-fit: cover;" alt="footer_btn_facebook.png" data-type="image" itemprop="image" src="https://static.wixstatic.com/media/85c2f3_0546340b59864edd9a080b0c00f5538d~mv2.png/v1/fill/w_16,h_35,al_c,q_80,usm_0.66_1.00_0.01/footer_btn_facebook.webp"></div></a></div><div style="width: 30px; height: 30px;" title="" data-is-responsive="false" data-display-mode="fill" data-content-padding-horizontal="0" data-content-padding-vertical="0" data-exact-height="30" class="ca1" id="comp-jqoug4a8"><a style="cursor: pointer; width: 30px; height: 30px;" href="https://instagram.com/hello_petdoc" target="_blank" data-content="https://instagram.com/hello_petdoc" data-type="external" id="comp-jqoug4a8link" class="ca1link"><div style="width: 30px; height: 30px; position: relative; top: 0px; left: 0px;" data-has-bg-scroll-effect="" data-style="" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-jqoug4do&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;masterPage&quot;,&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;&quot;,&quot;uri&quot;:&quot;85c2f3_0ea4b7ceba334085aedec18a7b21295b~mv2.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:60,&quot;height&quot;:60,&quot;alt&quot;:&quot;footer_btn_inata.png&quot;,&quot;name&quot;:&quot;footer_btn_inata.png&quot;,&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-jqoujfb5&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;masterPage&quot;,&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;https://instagram.com/hello_petdoc&quot;,&quot;target&quot;:&quot;_blank&quot;}},&quot;displayMode&quot;:&quot;fill&quot;}" class="ca1img" id="comp-jqoug4a8img"><img id="comp-jqoug4a8imgimage" style="object-position: 50% 50%; width: 30px; height: 30px; object-fit: cover;" alt="footer_btn_inata.png" data-type="image" itemprop="image" src="https://static.wixstatic.com/media/85c2f3_0ea4b7ceba334085aedec18a7b21295b~mv2.png/v1/fill/w_38,h_38,al_c,q_80,usm_0.66_1.00_0.01/footer_btn_inata.webp"></div></a></div><div style="width: 30px; height: 30px;" title="" data-is-responsive="false" data-display-mode="fill" data-content-padding-horizontal="0" data-content-padding-vertical="0" data-exact-height="30" class="ca1" id="comp-jqoug4a9"><a style="cursor: pointer; width: 30px; height: 30px;" href="http://blog.naver.com/petdoc1" target="_blank" data-content="http://blog.naver.com/petdoc1" data-type="external" id="comp-jqoug4a9link" class="ca1link"><div style="width: 30px; height: 30px; position: relative; top: 0px; left: 0px;" data-has-bg-scroll-effect="" data-style="" data-image-info="{&quot;imageData&quot;:{&quot;type&quot;:&quot;Image&quot;,&quot;id&quot;:&quot;dataItem-jqoug4f2&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;masterPage&quot;,&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;title&quot;:&quot;&quot;,&quot;uri&quot;:&quot;85c2f3_c6f25bb2a1ad41f98ff7b7d0eee1fe15~mv2.png&quot;,&quot;description&quot;:&quot;&quot;,&quot;width&quot;:60,&quot;height&quot;:60,&quot;alt&quot;:&quot;footer_btn_blog.png&quot;,&quot;name&quot;:&quot;footer_btn_blog.png&quot;,&quot;link&quot;:{&quot;type&quot;:&quot;ExternalLink&quot;,&quot;id&quot;:&quot;dataItem-jqouj8kt&quot;,&quot;metaData&quot;:{&quot;pageId&quot;:&quot;masterPage&quot;,&quot;isPreset&quot;:false,&quot;schemaVersion&quot;:&quot;1.0&quot;,&quot;isHidden&quot;:false},&quot;url&quot;:&quot;http://blog.naver.com/petdoc1&quot;,&quot;target&quot;:&quot;_blank&quot;}},&quot;displayMode&quot;:&quot;fill&quot;}" class="ca1img" id="comp-jqoug4a9img"><img id="comp-jqoug4a9imgimage" style="object-position: 50% 50%; width: 30px; height: 30px; object-fit: cover;" alt="footer_btn_blog.png" data-type="image" itemprop="image" src="https://static.wixstatic.com/media/85c2f3_c6f25bb2a1ad41f98ff7b7d0eee1fe15~mv2.png/v1/fill/w_38,h_38,al_c,q_80,usm_0.66_1.00_0.01/footer_btn_blog.webp"></div></a></div><div data-packed="true" style="width: 55px; pointer-events: none;" class="txtNew" id="comp-jsmsfp7l"><p class="font_8" style="font-size:13px; line-height:1.6em;">
             <a href="" target="_self">
        <span style="color:#8D95A0;"><span style="font-size:13px;">
        <span style="font-family:wfont_85c2f3_636d2fd6be9d49d5a00d68ebcfd119a7,wf_636d2fd6be9d49d5a00d68ebc,orig_notokrdemilight;">공고</span>
    </span>
</span>
</a>
</p>
</div>
</div>
</div>
</div>
